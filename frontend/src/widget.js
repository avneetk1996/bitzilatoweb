// -*- rjsx -*-
import React from 'react';
import ReactDOM from 'react-dom';
import { MuiThemeProvider, createGenerateClassName } from '@material-ui/core/styles';
import { I18nextProvider } from 'react-i18next';
import { createWidgetStore, loadMainRefs } from './redux';
import { Provider } from 'react-redux';
import JssProvider from 'react-jss/lib/JssProvider';
import createTheme from './theme';
import LanguageDetector from 'i18next-browser-languagedetector';
import { WIDGET_BASE_URL, WIDGET_P2P_URL } from './config';
import AdvertsFilter from './app/exchange/AdvertsFilter';
import { createI18n } from './localization';
import { ensureFilter } from './app/common/advert-filter-helpers';

const i18n = createI18n(LanguageDetector);

window.Bitzlato = window.Bitzlato || {};

window.Bitzlato.p2pSearchWidget = async function(elementId) {
    const store = createWidgetStore({
        backendUrl: WIDGET_BASE_URL
    });

    await store.dispatch(loadMainRefs());

    // Create a theme instance.
    const theme = createTheme();

    // Create a new class name generator.
    const generateClassName = createGenerateClassName({
        seed: 'bzwdgt'
    });

    const applyFilter = filter => {
        const sp = new URLSearchParams();
        Object.entries(filter).forEach(([key, value]) => {
            if (Array.isArray(value)) {
                value.forEach(i => sp.append(key, i));
            } else {
                sp.append(key, value);
            }
        });

        window.open(`${WIDGET_P2P_URL}?${sp.toString()}`, '_blank');
    };

    const filter = (
        <Provider store={store}>
            <I18nextProvider i18n={i18n}>
                <JssProvider generateClassName={generateClassName}>
                    <MuiThemeProvider theme={theme}>
                        <AdvertsFilter
                            filterParams={ensureFilter({ isOwnerVerificated: true })}
                            filterAdverts={applyFilter}
                            widgetMode={true}
                        />
                    </MuiThemeProvider>
                </JssProvider>
            </I18nextProvider>
        </Provider>
    );

    ReactDOM.render(filter, document.getElementById(elementId));
};

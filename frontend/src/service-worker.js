import { createI18n } from './localization/';
import { notificationInfo } from './app/common/notifications';
import { P2P_URL } from './config';
import Remarkable from 'remarkable';
import moment from 'moment-timezone';

const md = new Remarkable();

self.addEventListener('notificationclick', function(event) {
    event.notification.close();

    const url = event.notification.data.clickAction;

    event.waitUntil(
        self.clients
            .claim()
            .then(function() {
                return self.clients.matchAll({ type: 'window', includeUncontrolled: true });
            })
            .then(function(clients) {
                if (clients.length > 0 && 'navigate' in clients[0]) {
                    clients[0].focus();
                    //clients[0].navigate(url);
                } else {
                    self.clients.openWindow(url);
                }
            })
    );
});

self.addEventListener('push', function(event) {
    const { userId, lang, name, data } = event.data.json().data;

    const i18n = createI18n(null, moment);
    i18n.changeLanguage(lang);
    i18n.setDefaultNamespace('notifications');

    const info = notificationInfo(
        {
            name,
            data: JSON.parse(data)
        },
        {
            userId,
            t: (...args) => i18n.t(...args)
        }
    );

    if (info && info.text) {
        let body = md.render(info.text).replace(/<(.|\n)*?>/g, '');

        if (info.alert) {
            body += '\n\n';
            body += info.alert;
        }

        event.waitUntil(
            self.registration.showNotification('Bitzlato P2P', {
                body,
                data: {
                    clickAction: info.link || P2P_URL
                },
                requireInteraction: true
            })
        );
    }
});

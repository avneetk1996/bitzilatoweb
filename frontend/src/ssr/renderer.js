// -*- rjsx -*-
import _ from 'lodash';
import serialize from 'serialize-javascript';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { SheetsRegistry } from 'jss';
import JssProvider from 'react-jss/lib/JssProvider';
import { MuiThemeProvider, createGenerateClassName } from '@material-ui/core/styles';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import { I18nextProvider } from 'react-i18next';
import { Helmet } from 'react-helmet';
import { renderStylesToString } from 'emotion-server';
import MobileDetect from 'mobile-detect';

import { P2P_URL, STATIC_URL, BUILD_MARK, EXTRA_PAGE_HEADER, EXTRA_PAGE_BODY } from '../config';
import createTheme from '../theme';
import { createSsrStore } from '../redux';
import { setTimezone } from '../redux/actions';
import App from '../app';

import { loadMainRefs, loadProfile, loadNotifications, showAlert, loadLastDeal } from '../redux';
import { IWProvider } from '../app/hocs/withInitialWidth';

function renderFullPage(html, css, preloadedState) {
    const helmet = Helmet.renderStatic();

    const page = `<!doctype html>
    <html>
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="theme-color" content="#000000">

        ${EXTRA_PAGE_HEADER || ''}

        ${helmet.title.toString()}
        <link rel="manifest" href="${STATIC_URL}/manifest.json">
        <link rel="shortcut icon" href="${STATIC_URL}/favicon.ico">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
      </head>

      <body>
        ${EXTRA_PAGE_BODY || ''}

        <script>
          window.__PRELOADED_STATE__ = ${serialize(preloadedState)}
        </script>
        <div id="root">${html}</div>
        <style id="jss-server-side">${css}</style>
        <script async src="${STATIC_URL}/app.bundle.${BUILD_MARK}.js"></script>
      </body>
    </html>`;

    return page;

    // return minify(page, {
    //     collapseWhitespace: true,
    //     conservativeCollapse: true,
    //     minifyCSS: true
    // });
}

function makeHandleRender({ backendUrl }, fn) {
    return async (req, res, next) => {
        try {
            // Create a sheetsRegistry instance.
            const sheetsRegistry = new SheetsRegistry();

            // Create a sheetsManager instance.
            const sheetsManager = new Map();

            // Create a theme instance.
            const theme = createTheme();

            // Create a new class name generator.
            const generateClassName = createGenerateClassName();

            // Create a new Redux store instance
            const store = createSsrStore({
                initialState: {
                    auth: req.user,
                    backendUrl,
                    cookies: req.cookies
                },
                logout: () => {
                    req.logout();
                    res.redirect(`${P2P_URL}/`);
                }
            });

            store.dispatch(setTimezone('Europe/Moscow'));

            if (_.has(req, 'redux.alert')) {
                store.dispatch(showAlert('alert', req.redux.alert));
            }

            await Promise.all([
                store.dispatch(loadMainRefs()),
                store.dispatch(loadLastDeal()),
                req.user && store.dispatch(loadProfile()),
                req.user && store.dispatch(loadNotifications())
            ]);

            if (fn) {
                await fn(store, req);
            }

            // const lang = _.get(store.getState(), 'profile.settings.lang');
            // if (lang && lang !== req.i18n.language) {
            //     req.i18n.changeLanguage(lang);
            // }
            req.i18n.changeLanguage(_.get(store.getState(), 'profile.settings.lang', 'ru'));

            const md = new MobileDetect(req.headers['user-agent']);
            const initialWidth = md.mobile() ? 'xs' : 'lg';

            // Render the component to a string.
            const html = renderStylesToString(
                ReactDOMServer.renderToString(
                    <IWProvider value={initialWidth}>
                        <Provider store={store}>
                            <I18nextProvider i18n={req.i18n}>
                                <StaticRouter location={`${req.baseUrl}${req.url}`} context={{}}>
                                    <JssProvider registry={sheetsRegistry} generateClassName={generateClassName}>
                                        <MuiThemeProvider theme={theme} sheetsManager={sheetsManager}>
                                            <App />
                                        </MuiThemeProvider>
                                    </JssProvider>
                                </StaticRouter>
                            </I18nextProvider>
                        </Provider>
                    </IWProvider>
                )
            );

            // Grab the CSS from our sheetsRegistry.
            const css = sheetsRegistry.toString();

            // Grab the initial state from our Redux store
            const preloadedState = _.omit(store.getState(), ['backendUrl', 'cookies']);

            // Send the rendered page back to the client.
            res.send(renderFullPage(html, css, preloadedState));
        } catch (err) {
            next(err);
        }
    };
}

module.exports = {
    renderFullPage,
    makeHandleRender
};

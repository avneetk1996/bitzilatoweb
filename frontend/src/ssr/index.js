const path = require('path');
const express = require('express');
const session = require('express-session');
const RedisStore = require('connect-redis')(session);
const passport = require('passport');
const proxy = require('http-proxy-middleware');

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});

function serverRouter(backendUrl) {
    return require('./routes')({ backendUrl });
}

export function startSsrServer({ port, isDevelopment, backendUrl, redisUrl }) {
    const app = express();

    app.use(
        session({
            store: new RedisStore({
                url: redisUrl
            }),
            resave: false,
            saveUninitialized: false,
            secret: 'asfj;lupiu'
        })
    );

    app.use(passport.initialize());
    app.use(passport.session());

    if (isDevelopment) {
        app.use((req, res, next) => {
            serverRouter(backendUrl)(req, res, next);
        });

        app.use(
            '/api',
            proxy({
                target: 'http://localhost:8080'
            })
        );

        app.use(
            '/socket.io',
            proxy({
                target: 'http://localhost:6000/socket.io',
                ws: true
            })
        );

        const webpack = require('webpack');
        const webpackDevMiddleware = require('webpack-dev-middleware');
        const webpackHotMiddleware = require('webpack-hot-middleware');
        const config = require('../../webpack.dev.js');
        const compiler = webpack(config);

        app.use(
            webpackDevMiddleware(compiler, {
                noInfo: true,
                publicPath: config.output.publicPath
            })
        );

        app.use(webpackHotMiddleware(compiler));

        // Do "hot-reloading" of express stuff on the server
        // Throw away cached modules and re-require next time
        // Ensure there's no important state in there!
        const chokidar = require('chokidar');
        const watcher = chokidar.watch(path.resolve(__dirname, '..'));

        watcher.on('ready', function() {
            watcher.on('all', function() {
                Object.keys(require.cache).forEach(function(id) {
                    if (/[/\\]src[/\\]/.test(id)) delete require.cache[id];
                });
            });
        });
    } else {
        app.use(serverRouter(backendUrl));
    }

    return app.listen(port);
}

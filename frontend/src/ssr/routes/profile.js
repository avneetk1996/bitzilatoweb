const _ = require('lodash');
const express = require('express');
const contentDisposition = require('content-disposition');

const { BackendClient } = require('../../redux/actions/api');
const { loadReferralLinks, loadDealStats, loadReportList } = require('../../redux');

module.exports = (makeRenderer, { backendUrl }) => {
    const router = express.Router();

    router.get('/reports/:code', async (req, res) => {
        const backend = new BackendClient(req.user.userId, req.user.idToken, backendUrl);

        const reportType = _.find(await backend.reports(), report => report.code == req.params.code);

        if (reportType) {
            res.set('Content-Disposition', contentDisposition(`${reportType.description}.${reportType.format}`));
            (await backend.report(req.params.code, 'stream')).pipe(res);
        } else {
            res.sendStatus(404);
        }
    });

    router.get(
        '*',
        makeRenderer(async store => {
            await Promise.all([
                store.dispatch(loadDealStats()),
                store.dispatch(loadReferralLinks(_.get(store.getState(), 'profile.settings.cryptocurrency'))),
                store.dispatch(loadReportList())
            ]);
        })
    );

    return router;
};

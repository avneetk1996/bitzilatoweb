const _ = require('lodash');
const express = require('express');
const url = require('url');
const { parseFilterParams, pickFilter, ensureFilter } = require('../../app/common/advert-filter-helpers');
const {
    loadMixAdverts,
    saveLastFilter,
    loadExchangeAdvert,
    loadUserInfo,
    loadUserChat,
    loadWalletList,
    loadAllWalletsInfo,
    loadAdverts,
    loadOneAdvert,
    loadTradeStatus,
    loadTrades,
    loadOneTrade,
    loadTradeMessages,
    loadDisputeAdminMessages
} = require('../../redux');

const { P2P_URL } = require('../../config');

module.exports = makeRenderer => {
    const router = express.Router();

    function fillFilter(req, res, next) {
        if ('type' in req.query && 'currency' in req.query && 'cryptocurrency' in req.query) {
            next();
        } else {
            res.redirect(
                url.format({
                    pathname: P2P_URL,
                    query: ensureFilter(req.query)
                })
            );
        }
    }

    router.get(
        '/',
        fillFilter,
        makeRenderer(async (store, req) => {
            const location = url.parse(req.url);
            const filter = parseFilterParams(location.search);
            store.dispatch(saveLastFilter(pickFilter(_.omit(filter, ['skip', 'limit']))));
            if (filter.type && filter.currency && filter.cryptocurrency) {
                await store.dispatch(loadMixAdverts(filter));
            }
        })
    );

    router.get(
        '/users/:publicName',
        makeRenderer(async (store, req) => {
            await Promise.all([
                store.dispatch(loadUserInfo(req.params.publicName)),
                req.user && store.dispatch(loadUserChat(req.params.publicName))
            ]);
        })
    );

    router.get(
        ['/exchange/buy/:advertId', '/exchange/sell/:advertId'],
        makeRenderer(async (store, req) => {
            await store.dispatch(loadExchangeAdvert(req.params.advertId));
        })
    );

    router.get(
        '/adverts',
        makeRenderer(async store => {
            await store.dispatch(loadAdverts());
            await store.dispatch(loadTradeStatus());
        })
    );

    async function loadWalletsBaseInfo(store) {
        await store.dispatch(loadWalletList());
        await store.dispatch(loadAllWalletsInfo(_.keys(store.getState().wallets)));
    }

    router.get(
        '/adverts/create',
        makeRenderer(async store => {
            await loadWalletsBaseInfo(store);
        })
    );

    router.get(
        '/adverts/:advertId',
        makeRenderer(async (store, req) => {
            await store.dispatch(loadOneAdvert(req.params.advertId));
        })
    );

    router.get(
        '/trades/',
        makeRenderer(async store => {
            await store.dispatch(loadTrades());
        })
    );

    router.get(
        '/trades/:tradeId',
        makeRenderer(async (store, req) => {
            await Promise.all([
                store.dispatch(loadOneTrade(req.params.tradeId)),
                store.dispatch(loadTradeMessages(req.params.tradeId)),
                store.dispatch(loadDisputeAdminMessages(req.params.tradeId))
            ]);
        })
    );

    router.get('/tools', makeRenderer());

    return router;
};

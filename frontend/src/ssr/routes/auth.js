const _ = require('lodash');
const express = require('express');
const passport = require('passport');
const Auth0Strategy = require('passport-auth0');
const { auth0Config, PROFILE_URL, AUTH_URL, P2P_URL } = require('../../config');
const { BackendClient } = require('../../redux/actions/api');

module.exports = ({ backendUrl }) => {
    const router = express.Router();

    const strategy = new Auth0Strategy(auth0Config, async (accessToken, refreshToken, extraParams, profile, done) => {
        const apiClient = new BackendClient(null, null, backendUrl);

        return done(null, {
            userId: (await apiClient.whoami(extraParams.id_token)).userId,
            idToken: extraParams.id_token
        });
    });

    passport.use(strategy);

    router.get(
        '/login',
        (req, res, next) => {
            req.session.returnTo = req.get('Referrer');
            next();
        },
        passport.authenticate('auth0'),
        (req, res) => res.redirect(`${PROFILE_URL}/`)
    );

    router.get('/logout', (req, res) => {
        req.logout();
        res.redirect(`${P2P_URL}/`);
    });

    router.get(
        '/successLogin',
        passport.authenticate('auth0', { failureRedirect: `${AUTH_URL}/login` }),
        doMerge,
        function(req, res) {
            let returnTo = req.session.returnTo;
            if (!returnTo || _.endsWith(returnTo, 'successLogin') || _.endsWith(returnTo, 'login')) {
                returnTo = `${PROFILE_URL}/`;
            }

            res.redirect(returnTo);
        }
    );

    router.get(
        '/merge',
        doMerge,
        (req, res, next) => {
            req.session.telegram = req.query;
            next();
        },
        passport.authenticate('auth0'),
        doMerge
    );

    async function doMerge(req, res, next) {
        if (req.user && req.session.telegram) {
            try {
                const apiClient = new BackendClient(req.user.userId, req.user.idToken, backendUrl);
                await apiClient.mergeProfile(req.session.telegram);

                _.unset(req.session, 'telegram');

                res.redirect(`${PROFILE_URL}/telegram`);
            } catch (err) {
                res.redirect(`${PROFILE_URL}/`);
            }
        } else {
            next();
        }
    }

    return router;
};

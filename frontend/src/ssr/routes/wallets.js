const _ = require('lodash');
const express = require('express');

const {
    loadWalletList,
    loadAllWalletsInfo,
    loadRateSourcesRef,
    loadVoucherList,
    loadTransactions,
    loadWithdrawInfo
} = require('../../redux');

function getCurrency(store) {
    return _.get(store.getState(), 'profile.settings.currency');
}

module.exports = makeRenderer => {
    const router = express.Router();

    async function loadWalletsBaseInfo(store) {
        await store.dispatch(loadWalletList());
        await store.dispatch(loadAllWalletsInfo(_.keys(store.getState().wallets)));
    }

    router.get(
        '/:cryptocurrency/rate',
        makeRenderer(async (store, req) => {
            await loadWalletsBaseInfo(store);
            await store.dispatch(loadRateSourcesRef(getCurrency(store), req.params.cryptocurrency));
        })
    );

    router.get(
        '/:cryptocurrency/withdraw',
        makeRenderer(async (store, req) => {
            await loadWalletsBaseInfo(store);
            await store.dispatch(loadWithdrawInfo(req.params.cryptocurrency));
        })
    );

    router.get(
        '/:cryptocurrency/vouchers',
        makeRenderer(async (store, req) => {
            await loadWalletsBaseInfo(store);
            await store.dispatch(loadVoucherList(req.params.cryptocurrency));
        })
    );

    router.get(
        '/:cryptocurrency/txs',
        makeRenderer(async (store, req) => {
            await loadWalletsBaseInfo(store);
            await store.dispatch(loadTransactions(req.params.cryptocurrency, 0, 5));
        })
    );

    router.get(
        '*',
        makeRenderer(async store => {
            await loadWalletsBaseInfo(store);
        })
    );

    return router;
};

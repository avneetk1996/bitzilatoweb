const _ = require('lodash');
const url = require('url');
const express = require('express');
const moment = require('moment-timezone');
const i18nMiddleware = require('i18next-express-middleware');
const cookieParser = require('cookie-parser');
const { createI18n } = require('../../localization');
const { makeHandleRender } = require('../renderer');

const { AUTH_URL, PROFILE_URL, WALLETS_URL, P2P_URL, MARKET_URL } = require('../../config');
const { BackendClient } = require('../../redux/actions/api');

module.exports = ({ backendUrl }) => {
    const makeRenderer = fn => makeHandleRender({ backendUrl }, fn);

    const router = express.Router();
    router.use(i18nMiddleware.handle(createI18n(i18nMiddleware.LanguageDetector, moment)));
    router.use(cookieParser());

    router.use(async (req, res, next) => {
        try {
            const deepLinkCode = req.query.start;

            if (deepLinkCode) {
                const apiClient = new BackendClient(
                    _.get(req, 'user.userId'),
                    _.get(req, 'user.idToken'),
                    backendUrl,
                    req.cookies
                );
                const action = await apiClient.deeplink(
                    deepLinkCode,
                    url.format({
                        protocol: req.protocol,
                        host: req.get('host'),
                        pathname: req.originalUrl
                    })
                );
                const type = action && action.action;
                const params = action && action.params;

                _.forEach(action.setCookies, cookie => res.append('Set-Cookie', cookie));

                if (type === 'showUser') {
                    res.redirect(`${P2P_URL}/users/${params.publicName}`);
                } else if (type === 'showTrade') {
                    res.redirect(`${P2P_URL}/trades/${params.tradeId}`);
                } else if (type === 'showAdvert') {
                    const advert = await apiClient.getPublicAdvert(params.advertId);
                    res.redirect(
                        `${P2P_URL}/exchange/${advert.type === 'purchase' ? 'buy' : 'sell'}/${params.advertId}`
                    );
                } else if (type === 'alert') {
                    req.redux = {
                        alert: params.message
                    };
                    next();
                } else {
                    next();
                }
            } else {
                next();
            }
        } catch (err) {
            next();
        }
    });

    router.use(AUTH_URL, require('./auth')({ backendUrl }));
    router.use(P2P_URL, require('./p2p')(makeRenderer));
    router.use(WALLETS_URL, require('./wallets')(makeRenderer));
    router.use(PROFILE_URL, require('./profile')(makeRenderer, { backendUrl }));

    return router;
};

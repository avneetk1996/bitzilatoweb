const _ = require('lodash');
const program = require('commander');
require('url-search-params-polyfill');

program
    .option('--development', 'Run in development mode')
    .option('--port <port>', 'Specify port (default 3000)')
    .option('--backend [url]', 'Specify backend URL (default http://localhost:8080)')
    .option('--redis <url>', 'Specify Redis URL')
    .option('--cluster <count>', 'Number of workers for production. Not compatible with development mode.')
    .parse(process.argv);

const workerConfig = {
    port: _.parseInt(program.port || 3000),
    isDevelopment: Boolean(program.development),
    backendUrl: program.backend || 'http://localhost:8080',
    redisUrl: program.redis
};

const { startSsrServer } = require('./ssr');

if (workerConfig.isDevelopment || !program.cluster) {
    startSsrServer(workerConfig);
} else {
    const cluster = require('express-cluster');
    const clusterConfig = {
        count: _.parseInt(program.cluster)
    };

    cluster(() => startSsrServer(workerConfig), clusterConfig);
}

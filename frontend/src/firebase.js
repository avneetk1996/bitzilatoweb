import _ from 'lodash';
import firebase from 'firebase/app';
import 'firebase/messaging';
import { firebaseAppConfig, firebaseVapidKey } from './config';

import { STATIC_URL } from './config';
import { createApiClient } from './redux/actions/api';

let apiClient;

const messaging = (function() {
    try {
        firebase.initializeApp(firebaseAppConfig);

        const messaging = firebase.messaging();
        messaging.usePublicVapidKey(firebaseVapidKey);

        return messaging;
    } catch (err) {
        return null;
    }
})();

async function updatePushNotifications(store) {
    try {
        const state = store.getState();

        if (!apiClient && !_.isEmpty(state.auth)) {
            apiClient = createApiClient(() => state);

            await installPushNotifications();
        } else if (apiClient && _.isEmpty(state.auth)) {
            apiClient.removePushToken();
            apiClient = null;
        }
    } catch (err) {
        //console.log(err);
    }
}

async function installPushNotifications() {
    try {
        messaging.useServiceWorker(await navigator.serviceWorker.register(`${STATIC_URL}/service-worker.js`));

        await messaging.requestPermission();
        const token = await messaging.getToken();

        await apiClient.setPushToken(token);
        // messaging.onMessage(function(payload) {
        //     console.log('payload', payload);
        // });
    } catch (err) {
        //console.log(err);
    }
}

export async function initPushNotifications(store) {
    updatePushNotifications(store);
    store.subscribe(() => updatePushNotifications(store));
}

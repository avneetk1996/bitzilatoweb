import _ from 'lodash';
import io from 'socket.io-client';

import { NEW_USER_CHAT_MESSAGE, NEW_TRADE_CHAT_MESSAGE, NEW_TRADE_ADMIN_CHAT_MESSAGE } from './redux/constants';

import {
    loadOneTrade,
    newNotification,
    loadWalletInfo,
    loadProfile,
} from './redux/actions';

let socket = null;

function updateSocket(store) {
    if (!socket || !socket.connected) return;
}

function p2pConnect(socket, store) {
    function on(name, fn) {
        socket.on(name, async data => {
            if (fn) {
                await fn(data);
            }
            await store.dispatch(newNotification(data.notificationId, name, _.omit(data, ['notificationId'])));
        });
    }

    on('tradeStatusChanged', async ({ tradeId }) => {
        await store.dispatch(loadOneTrade(tradeId));
    });
    on('tradeWillExpire');

    on('disputeResolved', async ({ tradeId }) => {
        await store.dispatch(loadOneTrade(tradeId));
    });

    on('disputeAvailable', async ({ tradeId }) => {
        await store.dispatch(loadOneTrade(tradeId));
    });

    on('userMessage');

    on('newChatMessage', ({ from, tradeId, message, isAdmin }) => {
        if (!tradeId) {
            store.dispatch({
                type: NEW_USER_CHAT_MESSAGE,
                user: from,
                message: {
                    id: message.id,
                    type: 'in',
                    created: new Date(message.created),
                    message: message.message
                }
            });
        } else {
            store.dispatch({
                type: isAdmin ? NEW_TRADE_ADMIN_CHAT_MESSAGE : NEW_TRADE_CHAT_MESSAGE,
                id: tradeId,
                message: {
                    id: message.id,
                    type: 'in',
                    created: new Date(message.created),
                    message: message.message
                }
            });
        }
    });

    on('checkCashed');

    on('moneyReceived', async ({ cryptocurrency }) => {
        store.dispatch(loadWalletInfo(cryptocurrency.code));
    });

    on('userTradeStatusChanged', async () => {
        store.dispatch(loadProfile());
    });

    on('newReferral');
    on('dividendsReceived');
    on('stockExchangeEvent');
    on('accountsMerged');
    on('blockChainMoneyReceived');
    on('blockChainMoneySent');
    on('verificationDecision');
    on('mute');
}

function connect(store) {
    socket = io();

    socket.on('connect', () => {
        updateSocket(store);

        const { idToken } = store.getState().auth;
        if (idToken) {
            socket.emit('auth', idToken, () => {});
        }
    });
    socket.on('disconnect', () => (subsActivePair = null));

    p2pConnect(socket, store);
}

export default function(store) {
    updateSocket(store);
    store.subscribe(() => updateSocket(store));
    connect(store);
}

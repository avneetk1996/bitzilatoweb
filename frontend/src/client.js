// -*- rjsx -*-
import 'url-search-params-polyfill';
import _ from 'lodash';
import React from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment-timezone';
import JssProvider from 'react-jss/lib/JssProvider';
import { MuiThemeProvider, createGenerateClassName } from '@material-ui/core/styles';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { I18nextProvider } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import createHistory from 'history/createBrowserHistory';
import MobileDetect from 'mobile-detect';

import { AUTH_URL } from './config';
import { createI18n } from './localization';
import createTheme from './theme';
import App from './app';
import { createClientStore } from './redux';
import connectSocketIO from './socket';
import { initPushNotifications } from './firebase';
import { setTimezone } from './redux/actions';

import { IWProvider } from './app/hocs/withInitialWidth';

const i18n = createI18n(LanguageDetector, moment);
const history = createHistory();
history.listen(() => window.scrollTo(0, 0));
const md = new MobileDetect(window.navigator.userAgent);
const initialWidth = md.mobile() ? 'xs' : 'lg';

class Main extends React.Component {
    // Remove the server-side injected CSS.
    componentDidMount() {
        const jssStyles = document.getElementById('jss-server-side');
        if (jssStyles && jssStyles.parentNode) {
            jssStyles.parentNode.removeChild(jssStyles);
        }
    }

    render() {
        return <App />;
    }
}

function updateLanguage(store) {
    const lang = _.get(store.getState(), 'profile.settings.lang', 'ru');
    if (lang && lang !== i18n.language) {
        i18n.changeLanguage(lang);
    }
}

(async () => {
    // Grab the state from a global variable injected into the server-generated HTML
    const preloadedState = window.__PRELOADED_STATE__;

    // Allow the passed state to be garbage-collected
    delete window.__PRELOADED_STATE__;

    // Create Redux store with initial state
    const store = createClientStore({
        initialState: preloadedState,
        history,
        logout: () => window.location.replace(`${AUTH_URL}/logout`)
    });

    store.dispatch(setTimezone(moment.tz.guess()));

    // Create a theme instance.
    const theme = createTheme();

    // Create a new class name generator.
    const generateClassName = createGenerateClassName();

    updateLanguage(store);

    ReactDOM.hydrate(
        <IWProvider value={initialWidth}>
            <Provider store={store}>
                <I18nextProvider i18n={i18n}>
                    <ConnectedRouter history={history}>
                        <JssProvider generateClassName={generateClassName}>
                            <MuiThemeProvider theme={theme}>
                                <Main />
                            </MuiThemeProvider>
                        </JssProvider>
                    </ConnectedRouter>
                </I18nextProvider>
            </Provider>
        </IWProvider>,
        document.getElementById('root')
    );

    store.subscribe(() => updateLanguage(store));

    connectSocketIO(store);

    await initPushNotifications(store);
})();

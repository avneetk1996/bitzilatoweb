export default {
    title: 'Enable safe mode',
    begin: `🔒 To continue, you need to pass a safety knowledge test when working with the service.
    This operation is a one-time and does not take much time!`,

    success: 'Congratulations! Now you can create checks and disable safe mode',
    q1: ` You have started the transaction, but your counterpart offers to conduct the transaction through the service operator, in manual mode. It's so much faster and easier, and the course is better.
    Are there operators/support agents in the bot that conduct transactions?`,
    q1o1: 'Yes',
    q1o2: 'No',

    q2: `A support agent contacted you and asks you to give him the phone number and SMS code.
    Do I need to report this information?`,
    q2o1: 'Yes',
    q2o2: 'No',

    q3:
        'Transfer of funds under the transaction by check is not a violation and will not lead to loss of funds due to possible fraud?',
    q3o1: 'Right',
    q3o2: 'Cheque - is gift',
    error: 'Oops! This is a mistake that can cost you a loss of funds. Try again.',
    nextQuestion: 'Next question',
    startTest: 'Start test',
    close: 'Close'
};

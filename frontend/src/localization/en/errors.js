export default {
    create_trade_unavailable: `⚠️ Unfortunately, we are preparing the service for maintenance.

At the moment, starting a deal is not available!`,
    withdrawal_unavailable: `⚠️ At the moment, withdrawal is not available due to maintenance work.

Thank you for understanding!`,
    maintenance: `⚠️ Sorry, we are under maintenance!

Thank you for understanding! It won't be long !`,
    advert_unavailable: 'Unfortunately, this ad is unavailable right now'
};

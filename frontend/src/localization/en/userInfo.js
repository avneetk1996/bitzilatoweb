const translate = {
    'My profile': 'My profile',
    Profile: 'Profile',
    myDealsTotal: 'In the past {{time}} you made {{count}} deals on {{amount}} {{currency}} in total.',
    dealsTotal: 'In the past {{time}} user {{user}} made {{count}} deals on {{amount}} {{currency}} in total.',
    Verification: 'Verification',
    Rating: 'Rating',
    'Total count of trades': 'Total count of trades',
    'for total amount of money': 'for total amount of money',
    Feedback: 'Feedback',
    'Last online': 'Last online',
    'Block user': 'Block user',
    'Are you sure?': 'Are you sure?',
    Cancel: 'Cancel',
    'You blocked this user earlier and can unblock him now': 'You blocked this user earlier and can unblock him now',
    'Unblock user': 'Unblock user',
    'Send money directly to user': 'Send money directly to user',
    'Want to send': 'Want to send',
    Amount: 'Amount',
    Cryptocurrency: 'Cryptocurrency',
    Send: 'Send'
};

export default translate;

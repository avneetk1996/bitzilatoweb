const translate = {
    Profile: 'Profile',
    'Please, verify your account': 'Please, verify your account',
    'Please, accept licence agreement': 'Please, accept licence agreement',
    Name: 'Name',
    'Licence agreement': 'Licence agreement',
    Verified: 'Verified',
    'My profile': 'My profile',

    'My adverts': 'My adverts',
    'type of advert': 'type',
    'advert status': 'status',
    'advert currency': 'currency',
    'advert rate': 'rate',
    'Create advert': 'Create advert',
    'temp disable adverts': 'Pause all adverts',
    'adverts disabled': 'All adverts are paused now',
    'adverts active': 'All adverts are active now',
    'You can temporally pause all adverts': 'You can temporally pause all adverts',

    'Profile settings': 'Profile settings',
    Greeting: 'Greeting message',
    'Default fiat currency': 'Default fiat currency',
    'Default currency': 'Default currency',
    'Default cryptocurrency': 'Default cryptocurrency',
    Language: 'Language',
    Save: 'Save',

    'Referral program': 'Referral program',
    'List of referral links': 'List of referral links',
    Type: 'Type',
    Link: 'Link',

    'Referral description text1':
        'Invite new users and get passive profit from bot\'s fees. Make your private exchange service! 💵',
    'Referral description text2': 'Your fee from volume: 0.80%',
    'Referral description text3':
        'For example: if your affiliate user make a deal on 5 DOGE, you will get 0.04 DOGE of dividends.',
    'Referral description text4':
        'Affiliate program is perpetual; it has no limits for invitations and begins to act immediately.',
    'Referral description text5':
        'Keep in mind, for good results you should select right category of peoples who want to buy or sell DOGE',
    'Referral description text6': 'Invite users via the link below. Also you can use cheques or link on adverts',
    'Accept licence agreement': 'Accept licence agreement',

    'Telegram merge': 'Telegram merge',
    'Telegram merge description':
        'Привязав свой аккаунт к телеграму, вы обеспечите легкую интеграцию нашего сервиса с ботом-обменником',
    'Link accounts': 'Link accounts'
};

export default translate;

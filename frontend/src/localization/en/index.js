import adverts from './adverts';
import footer from './footer';
import header from './header';
import market from './market';
import profile from './profile';
import safemode from './safemode';
import snacks from './snacks';
import userInfo from './userInfo';
import wallets from './wallets';
import tools from './tools';
import errors from './errors';
import notifications from './notifications';

export default {
    adverts,
    footer,
    header,
    market,
    profile,
    safemode,
    snacks,
    userInfo,
    wallets,
    tools,
    errors,
    notifications
};

export default {
    Tools: 'Tools',
    toolsDescription: 'In this section, there is a functional for working with the service.',
    unavailableSafeModeDisable:
        'You are in safe mode! A button to disable this mode will appear after 2 — x successful transactions!',
    safeModeDescription: 'When safe mode is enabled, only trusted traders are available to you.',
    'API token': 'API token',
    APItokenDesc: 'It allows you to execute calls to our API',
    'Show API token': 'Show API token',
    'Generate new API token': 'Generate new API token',
    Indefinite: 'Indefinite',
    'Exp. date': 'Exp. date',
    close: 'Close'
};

const translate = {
    'Create advert': 'Создать объявление',
    'Adverts list': 'Adverts list',
    Login: 'Login',
    'My profile': 'My profile',
    'Profile settings': 'Profile settings',
    Referral: 'Referral',
    Licence: 'Licence',
    Dashboard: 'Dashboard',
    Logout: 'Logout',
    'My adverts': 'My adverts',

    lastDeal:
        'The deal on {{amount}} {{cryptocurrency}}  with the rate {{rate}} {{currency}} has been completed {{date, fromNow}}'
};

export default translate;

export default {
    rateTitle: 'Rate {{cryptocurrency}}',
    receiveTitle: 'Deposit {{cryptocurrency}}',
    withdrawTitle: 'Withdraw {{cryptocurrency}}',
    addWallet: 'Add wallet',
    CreateWallet: 'Create Wallet',
    createdTime: '{{time, LLL}}',
    withdrawText1: 'Для вывода средств воспользуйтесь формой ниже. Внимание, удостоверьтесь в правильности написания адреса.',
    withdrawText2: '⚠️ Network fee will be deducted from the balance of your wallet. This allows you to send exactly the required amount!',
};

export default {
    mute: 'Your ads have been removed from display for {{duration}} hours.',
    muteReason: `Your ads have been removed from display for {{duration}} hours.

Most likely, you have violated the conditions of ads. When the time limit expires, the ads will become active again.`
};

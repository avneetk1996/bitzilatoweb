const translate = {
    eventNewOrderBid: 'Order #{{data.id}} to sell {{data.amount}} {{base}} at {{data.price}} {{quote}} placed.',
    eventNewOrderAsk: 'Order #{{data.id}} to buy {{data.amount}} {{base}} at {{data.price}} {{quote}} placed.',
    orderRemovedCancelledBid:
        'Order #{{data.id}} to sell {{data.amount}} {{base}} at {{data.price}} {{quote}} is removed.',
    orderRemovedCancelledAsk:
        'Order #{{data.id}} to buy {{data.amount}} {{base}} at {{data.price}} {{quote}} is removed.',
    orderRemovedCompletedBid: 'Order #{{data.id}} to sell {{data.amount}} {{base}} at {{data.price}} {{quote}} is done',
    orderRemovedCompletedAsk: 'Order #{{data.id}} to buy {{data.amount}} {{base}} at {{data.price}} {{quote}} is done',
    orderChangedBid: 'Order #{{data.id}} to sell {{base}} changed',
    orderChangedAsk: 'Order #{{data.id}} to buy {{base}} changed',
};

export default translate;

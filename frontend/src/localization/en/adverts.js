const translate = {
    'Search adverts': 'Search adverts',
    Trader: 'Trader (last activity)',
    'Payment method': 'Payment method',
    Rate: 'Rate',
    Buy: 'Buy',
    Sell: 'Sell',
    'Rows per page': 'Rows per page',

    Filter: 'Filter',
    'I want': 'I want to...',
    'to sell': 'sell',
    'to buy': 'buy',
    for: 'for',
    Cryptocurrency: 'Cryptocurrency',
    Currency: 'Currency',
    'Select currency': 'Select currency',
    Amount: 'Amount',

    advertHeaderSell: 'Sell {{cryptocurrency}} for {{currency}} with {{paymethod}}',
    advertHeaderBuy: 'Buy {{cryptocurrency}} for {{currency}}  with {{paymethod}}',
    'Your balance': 'Your balance',
    'Bitzlato Protect': `🔒Bitzlato protects you. During the trade cryptocurrency will be holded on the trader account. After the payment trader marks deal as finished and release the funds.

**This protects both the buyer and the seller.**`,
    notAvailable: 'Unfortunately, this ad is not available at the moment!',
    no: 'No',

    confirmAdvertRateTitle: 'Attention!',
    confirmAdvertRate: 'At the moment the rate in this ad: **{{rate}} {{currency}}**\n\nDo you want to continue?'
};

export default translate;

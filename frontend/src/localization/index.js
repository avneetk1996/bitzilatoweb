// -*- rjsx -*-
import _ from 'lodash';
import i18n from 'i18next';

import en from './en';
import ru from './ru';

export function createI18n(languageDetector, moment) {
    const instance = i18n.createInstance();

    if (languageDetector) {
        instance.use(languageDetector);
    }

    function format(value, format, lng) {
        if (format === 'fromNow') {
            return moment(value)
                .locale(lng)
                .fromNow();
        } else if (_.isDate(value)) {
            return moment(value)
                .locale(lng)
                .format(format);
        } else if (format === 'duration') {
            return moment
                .duration(value)
                .locale(lng)
                .humanize();
        }

        return value;
    }

    instance.init({
        resources: {
            en,
            ru
        },
        fallbackLng: 'en',
        debug: false,

        keySeparator: false, // we use content as keys

        interpolation: {
            escapeValue: false, // not needed for react!!
            formatSeparator: ',',
            format
        }
    });

    return instance;
}

export default {
    tradeStatusChanged: 'Измененился статус сделки **#{{tradeId}}**',
    tradeExtendWaitingTime: 'Сделка #{{tradeId}}: Пользователь добавил {{time}} минут на обсуждение сделки!',
    tradeWillExpire: 'Сделка {{trade tradeId}} будет автоматически отменена {{time, fromNow}}',
    disputeSuccess: '✌ Вы победили в споре сделки {{tradeId}} и получили {{amount}} {{cryptocurrency}}.',
    disputeFail: '😞 Вы проиграли в споре сделки {{tradeId}}.',
    disputeAvailable:
        'Продавец не подтвердил получение средств в течении 40 минут,  вам доступна возможность открыть спор.',
    newTradeMessage: 'Новое сообщение в сделке #{{tradeId}}',
    newChatMessage: 'Новое сообщение от пользователя **{{publicName}}**',
    newMessage: 'Новое сообщение',
    checkCashed: '{{recipient}} обналичил чек на *{{amount}} {{cryptocurrency}}*.',
    moneyReceived:
        'Вы получили **{{cryptocurrency.amount}} {{cryptocurrency.code}}** ({{currency.amount}} {{currency.code}}) от {{donor}}!',
    tradePause:
        'Упс! Кажется Вас сейчас нет на месте и Вы пропускаете сделки. Мы приостановили показ ваших объявлений до тех пор, пока Вы не вернетесь.',
    newReferral: 'У Вас новый реферал {{publicName}}',
    dividendsReceived: 'Вы получили *{{cryptocurrency.amount}} {{cryptocurrency.code}}* вознаграждения от реферала 🤝',
    'payment-processed':
        '🚀 Отправка {{amount}} {{cryptocurrency}} на адрес {{address}} произведена, транзакция {{txid}}',
    'wallet-balance-loaded':
        '🔥 Ваш кошелек {{cryptocurrency}} пополнен на {{amount}} {{cryptocurrency}}, транзакция {{txid}}',
    'Telegram and web accounts have been merged': '✅ Телеграм-аккаунт прикреплен к вашей учетной записи.',
    'verification-confirmed': '✅ Ваша заявку на верификацию потверждена',
    'verification-rejected': '❌ Ваша заявка на верификацию отклонена',
    mute: 'Ваши объявления сняты с показа на {{duration}} ч.',
    muteReason: `Ваши объявления сняты с показа на {{duration}} ч.

Вероятнее всего, Вы нарушили условия размещения объявлений. По истечению срока ограничения объявления станут активны.`,

    'Remove All': 'Удалить всё',
    'No notifications': 'Нет уведомлений',
    notificationsCount: '{{count}} уведомлемлений'
};

export default {
    tradeListTitle: 'Активные сделки',
    Status: 'Статус',
    Paymethod: 'Способ оплаты',
    Amount: 'Размер сделки',
    Partner: 'Партнёр',
    tradeCreated: 'Новая сделка',
    confirmTrade: 'Подтверждение сделки',
    payment: 'Платёж',
    dispute: 'Спор',
    cancel: 'Сделка отменена',
    historyTime: '{{time, LLL}}',
    'Back to active trades list': 'Назад к списку активных сделок',
    confirmCancelTitle: 'Отмена сделки',
    confirmCancel: `Вы уверены в том, что хотите **отменить** сделку {{id}} на **{{currency.amount}} {{currency.code}}**? (**{{paymethod.name}}**).

⚠️  Пожалуйста, не отменяйте сделку если {{currency.amount}} {{currency.code}} уже отправлены по реквизитам продавца. Получить {{cryptocurrency.amount}} {{cryptocurrency.code}} или вернуть отправленные средства после отмены невозможно!`,
    confirmPaymentTitle: 'Подтверждение сделки',
    confirmPayment: `Вы уверены в том, что **уже получили** {{currency.amount}} {{currency.code}} от **{{partner.name}}**?

⚠️ Вернуть {{cryptocurrency.amount}} {{cryptocurrency.code}} после завершения сделки невозможно.

❕Обязательно дождитесь и проверьте фактическое получение средств на счет!`,
    Market: 'Биржа',
};

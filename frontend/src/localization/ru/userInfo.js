const translate = {
    'My profile': 'Мой профиль',
    Profile: 'Профиль',
    myDealsTotal: 'За последние {{time}} вы совершили {{count}} сделок на сумму {{amount}} {{currency}}',
    dealsTotal:
        'За последние **{{time, duration}}** пользователь совершил **{{count}}** сделок на сумму **{{amount}} {{currency}}**',
    dealsTotalTitle: 'За последние **{{time, duration}}** пользователь совершил:',
    dealsTotalInfo: '**{{count}}** сделок на сумму **{{amount}} {{currency}}**',
    Verification: 'Верификация аккаунта',
    Rating: 'Рейтинг',
    'Total count of trades': 'Общее количество сделок',
    'for total amount of money': 'на сумму',
    Feedback: 'отзывы',
    'Last online': 'Последняя активность',
    Block: 'Заблокировать',
    'Are you sure?': 'Вы уверены?',
    Cancel: 'Отмена',
    'You blocked this user earlier and can unblock him now':
        'Вы заблокировали этого пользователя ранее, и можете разблокировать его сейчас.',
    Unblock: 'Разблокировать ',
    'Send money': 'Отправить деньги',
    'Want to send': 'Хочу отправить',
    Amount: 'Сумма',
    Cryptocurrency: 'Криптовалюта',
    Send: 'Отправить',
    lastActivity: '{{lastActivity, fromNow}}',
    Required: 'Обязательно',
    'Invalid value': 'Поле заполнено неверно'
};

export default translate;

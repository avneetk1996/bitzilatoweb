export default {
    Wallets: 'Кошельки',
    'All Balance': 'Общий баланс',
    address: 'адрес',
    'Invalid address': 'Недействительный адрес',
    amount: 'сумма',
    Amount: 'Сумма',
    Comment: 'Комментарий',
    rateTitle: 'Курс {{cryptocurrency}}',
    rateText:
        'Выберите источник актуального курса для пары **{{cryptocurrency}}/{{currency}}**. Учтите, что изменения будут применены к вашим объявлениям с плавающей ценой.',
    receiveTitle: 'Внести {{cryptocurrency}}',
    withdrawTitle: 'Вывод {{cryptocurrency}}',
    Balance: 'Баланс',
    Equivalent: 'Примерно',
    Blocked: 'Заблокировано',
    addWallet: 'Добавить кошелек',
    CreateWallet: 'Создать кошелек',
    cryptocurrency: 'Криптовалюта',
    'Min. value': 'Минимальная сумма',
    'Available for withdrawal': 'Доступно для вывода',
    Fee: 'Комиссия',
    'Rows per page': 'Строк на странице',
    Created: 'Создан',
    createdTime: '{{time, LLL}}',
    Required: 'Обязательно',
    'Invalid value': 'Поле заполнено неверно',
    withdrawText1:
        'Для вывода средств воспользуйтесь формой ниже. Внимание, удостоверьтесь в правильности написания адреса.',
    withdrawText2:
        '⚠️ Комиссия сети списывается с баланса Вашего кошелька, что позволяет отправить ровно необходимую сумму!'
};

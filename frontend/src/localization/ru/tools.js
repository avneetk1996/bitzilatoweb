export default {
    Tools: 'Инструменты',
    toolsDescription: 'В данном разделе находится функционал, для работы с сервисом.',
    unavailableSafeModeDisable:
        'Вы находитесь в безопасном режиме! Кнопка для отключения данного режима появится после 2—х успешных сделок!',
    'Safe Mode': 'Безопасный режим',
    safeModeDescription: 'При включенном безопасном режиме Вам доступны заявки только доверенных трейдеров.',
    'Disable safe mode': 'Отключить безопасный режим',
    'Enable safe mode': 'Включить безопасный режим',
    'API token': 'API токен',
    APItokenDesc: 'Позволяет выполнять запросы в наш API',
    'Show API token': 'Показать API токен',
    'Generate new API token': 'Сгенерировать новый API токен',
    Indefinite: 'Не ограниченно',
    'Exp. date': 'Срок действия',
    close: 'Закрыть'
};

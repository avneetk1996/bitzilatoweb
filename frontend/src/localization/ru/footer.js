const translate = {
    Products: 'ПРОДУКТЫ',
    Company: 'КОМПАНИЯ',
    HelpAndSupport: 'ПОМОЩЬ И ПОДДЕРЖКА',
    P2PExch: 'P2P-обменник',
    Exch: 'Биржа',
    Wallet: 'Кошелек',
    Merchant: 'Платежный шлюз',
    ACompany: 'О компании',
    RefProgram: 'Реферальная программа',
    Commissions: 'Комиссии',
    Comments: 'Отзывы',
    Vacancies: 'Вакансии',
    TermsOfUse: 'Условия использования',
    Security: 'Политика безопасности',
    HowToSelfSafe: 'Как обезопасить себя',
    Forum: 'Форум',
    AccountVerification: 'Верификация аккаунта',
    SupportTelegram: 'Поддержка в Telegram',
    Support: 'Поддержка',
    Blog: 'Блог'
};

export default translate;

const translate = {
    Links: 'Ссылки',
    'Link for this advert': 'Ссылка на это объявление',
    'Search adverts': 'Покупайте и продавайте криптовалюту мгновенно.',
    Search: 'Искать',
    Trader: 'Трейдер',
    'Payment method': 'Способ оплаты',
    Rate: 'Курс',
    ratePercent: 'Процент от биржевого курса',
    rateValue: 'или указать точный курс',
    Buy: 'Купить',
    Sell: 'Продать',
    'Rows per page': 'Строк на странице',

    Filter: 'Фильтровать',
    'I want': 'Я хочу...',
    'to sell': 'продать',
    'to buy': 'купить',
    for: 'за',
    Cryptocurrency: 'Криптовалюта',
    Currency: 'Валюта',
    'Select currency': 'Выберите валюту',
    Amount: 'Сумма',

    advertHeaderSell: 'Обменять {{cryptocurrency}} на {{currency}} через {{paymethod}}',
    advertHeaderBuy: 'Обменять {{currency}} на {{cryptocurrency}} через {{paymethod}}',

    advertSellEstimate:
        'Обменять {{cryptocurrency.amount}} {{cryptocurrency.code}} на {{currency.amount}} {{currency.code}}, коммисия {{fee}} {{cryptocurrency.code}}',
    advertBuyEstimate:
        'Обменять {{currency.amount}} {{currency.code}} на {{cryptocurrency.amount}} {{cryptocurrency.code}}, коммисия {{fee}} {{cryptocurrency.code}}',

    'Start all adverts': '🌞 Начать торговлю',
    'Pause all adverts': '🌚 Остановить торговлю',
    'Create advert': '📝 Добавить объявление',

    advertListTitle: 'Здесь отображается список ваших объявлений',
    createAdvert: 'Создать объявление',
    Type: 'Тип',
    Status: 'Статус',
    selling: 'Продажа',
    purchase: 'Покупка',
    active: 'Включено',
    paused: 'Отключено',
    Edit: 'Изменить',
    Details: 'Реквизиты для перевода',
    Limits: 'Лимиты',
    Min: 'Минимум',
    Max: 'Максимум',
    'Terms of use': 'Условия торговли',
    'Unactive reason': 'Причина остановки',
    Delete: 'Удалить',
    'Delete anyway': 'Действительно удалить',
    Save: 'Сохранить',
    Cancel: 'Отмена',
    'Turn On': 'Включить',
    'Turn Off': 'Выключить',

    'Start trade': 'Начать сделку',
    'Need login': 'Для совершения сделки необходимо войти в систему',
    'Please, accept licence agreement': 'Пожалуйста, примите лицензионное соглашение',
    'Bitzlato Protect': `🔒Bitzlato защищает Вас.
На время сделки криптовалюта будет заблокирована на счету трейдера. После оплаты, трейдер отмечает сделку, как оплаченную и отпускает криптовалюту.

**Механизм блокировки криптовалюты защищает как трейдера, так и покупателя.**`,

    'Only active': 'Только активные',
    'Only verificated': 'Проверенные пользователи',

    Required: 'Обязательно',
    'Minimal amount': 'Минимальная сумма {{min}} {{coin}}',
    'Maximal amount': 'Максимальная сумма {{max}} {{coin}}',
    'Your balance': 'Ваш баланс',
    'Back to adverts list': 'Назад к списку объявлений',
    'Not enought funds to start the trade': 'Не достаточно средств для начала сделки',
    notAvailable: 'К сожалению, данное объявление недоступно на данный момент!',
    no: 'Нет',
    Suspicious: 'Подозрительный',
    Verification: 'Верификация',
    'Identity card': 'Документы',

    confirmAdvertRateTitle: 'Внимание!',
    confirmAdvertRate: 'На данный момент курс в объявлении: **{{rate}} {{currency}}**\n\nВы желаете продолжить?'
};

export default translate;

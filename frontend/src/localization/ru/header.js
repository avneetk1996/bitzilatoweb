const translate = {
    'Create advert': 'Создать объявление',
    'Adverts list': 'Список объявлений',
    Login: 'Войти',

    'My profile': 'Личный кабинет',
    'My trades': 'Сделки',
    'My wallets': 'Кошельки',
    'My adverts': 'Мои Объявления',
    Market: 'Биржа',
    'Market. Reports': 'Биржа. Отчеты',
    'Market. Orders': 'Биржа. Ордера',
    Logout: 'Выход',

    'Profile settings': 'Настройки профиля',
    Referral: 'Реферальная программа',
    Licence: 'Лицензионное соглашение',
    Dashboard: 'Дашбоард',

    lastDeal:
        'Завершена сделка на сумму {{amount}} {{cryptocurrency}} при курсе {{rate}} {{currency}} {{date, fromNow}}',

    Tools: 'Инструменты'
};

export default translate;

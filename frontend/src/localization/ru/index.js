import adverts from './adverts';
import common from './common';
import errors from './errors';
import footer from './footer';
import header from './header';
import market from './market';
import notifications from './notifications';
import profile from './profile';
import safemode from './safemode';
import snacks from './snacks';
import tools from './tools';
import trades from './trades';
import userInfo from './userInfo';
import wallets from './wallets';

export default {
    adverts,
    common,
    errors,
    footer,
    header,
    market,
    notifications,
    profile,
    safemode,
    snacks,
    trades,
    userInfo,
    wallets,
    tools
};

const translate = {
    eventNewOrderBid: 'Вы выставили ордер #{{data.id}} на покупку {{data.amount}} {{base}} за {{data.price}} {{quote}}',
    eventNewOrderAsk: 'Вы выставили ордер #{{data.id}} на продажу {{data.amount}} {{base}} за {{data.price}} {{quote}}',
    orderRemovedCancelledBid:
        'Вы отменили ордер #{{data.id}} на покупку {{data.amount}} {{base}} за {{data.price}} {{quote}}',
    orderRemovedCancelledAsk:
        'Вы отменили ордер #{{data.id}} на продажу {{data.amount}} {{base}} за {{data.price}} {{quote}}',
    orderRemovedCompletedBid:
        'Ордер #{{data.id}} на покупку {{data.amount}} {{base}} за {{data.price}} {{quote}} выполнен',
    orderRemovedCompletedAsk:
        'Ордер #{{data.id}} на продажу {{data.amount}} {{base}} за {{data.price}} {{quote}} выполнен',
    orderChangedBid: 'Ордер #{{data.id}} на покупку {{base}} изменился',
    orderChangedAsk: 'Ордер #{{data.id}} на продажу {{base}} изменился',
};

export default translate;

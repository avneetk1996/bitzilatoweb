const translate = {
    Reports: 'Отчёты',
    Profile: 'Профиль',
    'Please, verify your account': 'Пожалуйста, пройдите верификацию аккаунта',
    'Please, accept licence agreement': 'Пожалуйста, примите лицензионное соглашение',
    Name: 'Имя',
    'Licence agreement': 'Лицензионное соглашение',
    Verified: 'Верификация',
    'My profile': 'Профиль',

    'Profile settings': 'Настройки',
    Greeting: 'О себе',
    'Default fiat currency': 'Основная фиатная валюта',
    'Default currency': 'Основная валюта',
    'Default cryptocurrency': 'Основная криптовалюта',
    Language: 'Язык',
    Save: 'Сохранить',

    Referrals: 'Реферальная программа',
    'Referral program': 'Реферальная программа',
    'List of referral links': 'Список ваших реферальных ссылок',
    Type: 'Тип',
    Link: 'Линк',
    'Referral description text1':
        'Приглашайте новых пользователей и получайте пассивный доход от комиссий бота, создав свой личный обменник!  💵',
    'Referral description text2': 'Ваша комиссия от оборота: 0.80%',
    'Referral description text3':
        'Например: ваш подписчик проводит сделку на сумму 5 DOGE, а вы получаете 0.04 DOGE  дивидендов.',
    'Referral description text4':
        'Партнерская программа бессрочна, не имеет лимита приглашений и начинает действовать моментально.',
    'Referral description text5':
        'Учтите, что для достижения хороших результатов необходимо внимательно подходить к поиску целевой аудитории и привлекать только тех, кто будет покупать или продавать DOGE',
    'Referral description text6':
        'Используйте ссылку ниже для инвайта, вы также можете использовать чеки и ссылки на объявления.',
    Accept: 'Принять',

    'Disable safe mode': '🔐 Отключить безопасный режим',
    'Enable safe mode': '🔓 Включить безопасный режим',
    'Disable service fee': '🎾 Отключить комиссию',
    'Enable service fee': '⚪️ Включить комиссию',

    'Telegram merge': 'Telegram',
    'Merged account':
        'Вы уже привязали вашу учетную запись к telegram-аккаунту. Теперь вы можете совершать сделки, как в веб-версии на p2p.bitzlato.com, так и в боте!',
    Market: 'Биржа'
};

export default translate;

export default {
    'Write a message...': 'Введите сообщение',
    timeWithDate: '{{time, LLL}}',
    timeWithoutDate: '{{time, LT}}',
    Required: 'Обязательно',
    Ok: 'Ок',
    Cancel: 'Отмена'
};

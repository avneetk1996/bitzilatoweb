// -*- rjsx -*-
import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Grid } from '@material-ui/core';
import { withNamespaces } from 'react-i18next';

const History = props => {
    const { history, t } = props;

    return (
        <Grid container spacing={8}>
            {history &&
                history.length > 0 &&
                history.map((hist, key) => (
                    <Grid item xs={12} key={key}>
                        <Typography variant="body2">
                            <strong>{t('historyTime', { time: hist.date })}</strong>
                            &nbsp; &nbsp; &nbsp;
                            {hist.status === 'cancel' && 'Отменена'}
                            {hist.status === 'trade_created' && 'Создана'}
                            {hist.status === 'confirm_trade' && 'Подтверждена'}
                            {hist.status === 'payment' && 'Деньги отправлены продавцу'}
                            {hist.status === 'confirm_payment' && 'Сделка успешна'}
                            {hist.status === 'dispute' && 'Открыт спор'}
                        </Typography>
                    </Grid>
                ))}
        </Grid>
    );
};

History.propTypes = {
    t: PropTypes.func.isRequired,
    history: PropTypes.array
};

export default withNamespaces('trades')(History);

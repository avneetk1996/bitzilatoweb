// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { withNamespaces } from 'react-i18next';
import { Button, Typography, Grid, withStyles } from '@material-ui/core';
import moment from 'moment-timezone';
import ReactTimeout from 'react-timeout';

import { P2P_URL } from '../../config';
import {
    loadOneTrade,
    tradeAction,
    loadTradeMessages,
    sendTradeMessage,
    sendTradeFile,
    tradeFeedback,
    tradeTimeout,
    loadDisputeAdminMessages,
    sendDisputeAdminMessage,
    sendDisputeAdminFile,
    setTradeDetails,
    cancelPromptTradeDetails,
    describeDispute
} from '../../redux/actions';
import TradeHistory from './History';
import Chat from '../common/Chat';
import DisputeBlock from './DisputeBlock';
import FeedbackBlock from './Feedback';
import Prompt from '../common/Prompt';
import Confirm from '../common/Confirm';

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    paper: {
        padding: 24
        // textAlign: 'center',
        // color: theme.palette.text.secondary,
    },
    tableWrapper: {
        overflowX: 'auto'
    },
    disputeWrap: {
        marginRight: 16,
        display: 'inline-block'
    },
    timer: {
        color: theme.palette.secondary.main
    }
});

class Trade extends Component {
    constructor() {
        super();

        this.state = {
            confirm: null
        };
    }

    static propTypes = {
        setInterval: PropTypes.func.isRequired,
        t: PropTypes.func,
        classes: PropTypes.object,
        match: PropTypes.object,

        tradeFeedback: PropTypes.func,
        loadOneTrade: PropTypes.func,
        loadMessages: PropTypes.func,
        loadDisputeAdminMessages: PropTypes.func,
        tradeAction: PropTypes.func,
        sendMessage: PropTypes.func,
        sendFile: PropTypes.func,
        sendDisputeAdminMessage: PropTypes.func,
        sendDisputeAdminFile: PropTypes.func,
        setTradeDetails: PropTypes.func,
        cancelPromptTradeDetails: PropTypes.func,
        describeDispute: PropTypes.func,
        tradeTimeout: PropTypes.func,

        trade: PropTypes.object,
        feedbackRatings: PropTypes.array,
        messages: PropTypes.array,
        adminChats: PropTypes.array
    };

    componentDidMount() {
        this.loadTradeData();
        this.interval = this.props.setInterval(() => this.setState({ time: Date.now() }), 10000);
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.tradeId != prevProps.match.params.tradeId) {
            this.loadTradeData();
        }
    }

    loadTradeData() {
        this.props.loadOneTrade();
        this.props.loadMessages();
        this.props.loadDisputeAdminMessages();
    }

    tradeAction(action) {
        const { trade, t } = this.props;

        if (action === 'cancel') {
            this.setState({
                confirm: {
                    title: t('confirmCancelTitle'),
                    message: t('confirmCancel', trade),
                    handleOk: () => this.props.tradeAction(action)
                }
            });
        } else if (action === 'confirm-payment') {
            this.setState({
                confirm: {
                    title: t('confirmPaymentTitle'),
                    message: t('confirmPayment', trade),
                    handleOk: () => this.props.tradeAction(action)
                }
            });
        } else {
            this.props.tradeAction(action);
        }
    }

    sendMessage({ message }) {
        this.props.sendMessage(message);
    }

    sendDisputeMessage({ message }) {
        this.props.sendDisputeAdminMessage(message);
    }

    async createDispute(reason) {
        await this.props.tradeAction('dispute');
        await this.props.describeDispute(reason);
    }

    markTrade = code => {
        this.props.tradeFeedback(code);
    };

    closePromptDetailsDialog = () => {
        this.props.cancelPromptTradeDetails();
    };

    setDetailsAndAccept = async details => {
        await this.props.setTradeDetails(details);
        await this.props.tradeAction('confirm-trade');
    };

    render() {
        const { trade, classes, feedbackRatings, t } = this.props;
        const { confirm } = this.state;
        const loaded = Boolean(trade) && Boolean(trade.id);

        const availableActions = {};
        const partner = loaded && trade.partner;

        if (loaded) {
            _.forEach(trade.availableActions, action => (availableActions[action] = true));
        } else {
            return null;
        }

        return (
            <Grid container spacing={16}>
                <Helmet>
                    <title>Сделка</title>
                </Helmet>

                <Grid item xs={12}>
                    <Grid container>
                        <Grid item xs />
                        <Grid item>
                            <Button component="div">
                                <Link to={`${P2P_URL}/trades`}>{t('Back to active trades list')}</Link>
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item xs={12}>
                    <Typography variant="h2">Сделка</Typography>
                </Grid>

                {!_.isEmpty(confirm) && (
                    <Confirm
                        opened={true}
                        title={confirm.title}
                        message={confirm.message}
                        handleOk={() => {
                            confirm.handleOk();
                            this.setState(this.setState({ confirm: null }));
                        }}
                        handleCancel={() => this.setState({ confirm: null })}
                    />
                )}

                {Boolean(trade && trade.promptDetails) && (
                    <div style={{minWidth: '50%'}}>
                        <Prompt
                            opened={true}
                            title="Укажите реквизиты"
                            message="Реквизиты"
                            closeDialog={this.closePromptDetailsDialog}
                            handlePrompt={this.setDetailsAndAccept}
                            multiline
                            rows={8}
                            variant={'outlined'}
                        />
                    </div>

                )}

                <Grid item xs={12}>
                    <Grid container spacing={24}>
                        <Grid item xs={12} sm={6}>
                            <Grid container spacing={8}>
                                <Grid item xs={12}>
                                    <Typography variant="subtitle1">
                                        {trade.status === 'trade_created' && (
                                            <span>Сделка создана, ожидаем ответа продавца </span>
                                        )}

                                        {trade.status === 'confirm_trade' && (
                                            <span>Сделка подтверждена, ожидаем перевода фиатных денег</span>
                                        )}

                                        {trade.status === 'payment' && (
                                            <span>Платеж поступил, ожидаем подтверждения второго участника сделки</span>
                                        )}

                                        {trade.status === 'confirm_payment' && <span>Сделка успешно завершена</span>}

                                        {trade.status === 'dispute' && (
                                            <span> Что-то пошло не так, открыт диспут по сделке</span>
                                        )}

                                        {trade.status === 'cancel' && <span>Сделка отменена</span>}

                                        {trade.details && (
                                            <p>
                                                {' '}
                                                <strong>Реквизиты сделки: </strong>
                                                <strong>{trade.details}</strong>
                                            </p>
                                        )}
                                    </Typography>
                                </Grid>

                                <Grid item xs={12}>
                                    <Typography variant="body2">
                                        <strong>Сумма сделки: </strong>
                                        {trade.currency.amount} {trade.currency.code}
                                    </Typography>
                                </Grid>

                                <Grid item xs={12}>
                                    <Typography variant="body2">
                                        <strong>Криптовалюта:</strong> {trade.cryptocurrency.amount}{' '}
                                        {trade.cryptocurrency.code}
                                    </Typography>
                                </Grid>

                                <Grid item xs={12}>
                                    <Typography variant="body2">
                                        <strong>Курс:</strong> {trade.rate} {trade.paymethod.currency}
                                    </Typography>
                                </Grid>

                                <Grid item xs={12}>
                                    <Typography variant="body2">
                                        <strong>Способ оплаты:</strong> {trade.paymethod.name}
                                    </Typography>
                                </Grid>

                                <Grid item xs={12}>
                                    <Typography variant="body2">
                                        <strong>Партнер по сделке: </strong>
                                        {partner.name}
                                        {/* , кол-во сделок {partner.totalCount}, объем торгов{' '} {partner.totalAmount} */}
                                    </Typography>
                                </Grid>

                                {trade.times.autocancel &&
                                    trade.times.autocancel.getTime() > Date.now() && (
                                        <Grid item xs={12}>
                                            <Typography variant="subtitle1" className={classes.timer}>
                                                Автоотмена сделки произойдёт{' '}
                                                <strong>
                                                    {moment
                                                        .duration(moment(trade.times.autocancel).diff(moment()))
                                                        .locale('ru')
                                                        .humanize(true)}
                                                </strong>
                                            </Typography>
                                        </Grid>
                                    )}

                                {trade.times.dispute &&
                                    trade.times.dispute.getTime() > Date.now() && (
                                        <Grid item xs={12}>
                                            <Typography variant="subtitle1" className={classes.timer}>
                                                Возможность открыть диспут появиться{' '}
                                                <strong>
                                                    {moment
                                                        .duration(moment(trade.times.dispute).diff(moment()))
                                                        .locale('ru')
                                                        .humanize(true)}
                                                </strong>
                                            </Typography>
                                        </Grid>
                                    )}
                            </Grid>
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <Grid container spacing={8}>
                                {availableActions['dispute'] && (
                                    <Grid item xs={12}>
                                        <DisputeBlock createDispute={reason => this.createDispute(reason)} />
                                    </Grid>
                                )}

                                {trade.status === 'dispute' && (
                                    <Grid item xs={12}>
                                        <Chat
                                            onSubmit={message => this.sendDisputeMessage(message)}
                                            sendFile={this.props.sendDisputeAdminFile}
                                            form={'disputeChatTradeForm'}
                                            buttonVariant={'contained'}
                                            messages={this.props.adminChats}
                                            chatName={'Чат по спору с администратором'}
                                        />
                                    </Grid>
                                )}

                                {availableActions['cancel'] && (
                                    <Grid item xs={12}>
                                        <Button
                                            fullWidth
                                            onClick={() => this.tradeAction('cancel')}
                                            color="secondary"
                                            variant="contained"
                                        >
                                            Отменить сделку
                                        </Button>
                                    </Grid>
                                )}

                                {availableActions['confirm-trade'] && (
                                    <Grid item xs={12}>
                                        <Button
                                            onClick={() => this.tradeAction('confirm-trade')}
                                            color="primary"
                                            fullWidth
                                            variant="contained"
                                        >
                                            Подвердить сделку
                                        </Button>
                                    </Grid>
                                )}

                                {availableActions['addtime'] && (
                                    <Grid item xs={12}>
                                        <Button
                                            onClick={() => this.props.tradeTimeout(10)}
                                            color="primary"
                                            fullWidth
                                            variant="contained"
                                        >
                                            🕑 +10 минут
                                        </Button>
                                    </Grid>
                                )}

                                {availableActions['payment'] && (
                                    <Grid item xs={12}>
                                        <Button
                                            onClick={() => this.tradeAction('payment')}
                                            color="primary"
                                            fullWidth
                                            variant="contained"
                                        >
                                            Я заплатил
                                        </Button>
                                    </Grid>
                                )}

                                {availableActions['confirm-payment'] && (
                                    <Grid item xs={12}>
                                        <Button
                                            onClick={() => this.tradeAction('confirm-payment')}
                                            variant="contained"
                                            fullWidth
                                            color="primary"
                                        >
                                            Отпустить монеты
                                        </Button>
                                    </Grid>
                                )}

                                {availableActions['feedback'] && (
                                    <Grid item xs={12}>
                                        <Typography variant="subtitle1">Оценить сделку</Typography>

                                        <FeedbackBlock feedbackRatings={feedbackRatings} markTrade={this.markTrade} />
                                    </Grid>
                                )}
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item xs={12}>
                    <Grid container spacing={24}>
                        <Grid item xs={12} sm={6}>
                            <Chat
                                onSubmit={message => this.sendMessage(message)}
                                sendFile={this.props.sendFile}
                                form={'chatTradeForm'}
                                buttonVariant={'contained'}
                                messages={this.props.messages}
                                chatName={`Чат с ${trade.partner.name}`}
                                disabled={
                                    availableActions['feedback'] ||
                                    trade.status === 'cancel' ||
                                    trade.status === 'confirm_payment'
                                }
                            />
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <Grid container spacing={8}>
                                <Grid item xs={12}>
                                    <Typography variant="subtitle1">История сделки</Typography>
                                </Grid>

                                <Grid item xs={12}>
                                    <TradeHistory history={trade.history} />
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = (state, props) => {
    const tradeId = props.match.params.tradeId;

    return {
        trade: _.get(state, ['trades', 'items', tradeId]),
        messages: _.get(state, ['trades', 'chats', tradeId], []),
        adminChats: _.get(state, ['trades', 'adminChats', tradeId], []),
        feedbackRatings: state.refs.feedbackRatings
    };
};

const mapDispatchToProps = (dispatch, props) => {
    const tradeId = props.match.params.tradeId;

    return {
        loadOneTrade: () => dispatch(loadOneTrade(tradeId)),
        loadMessages: () => dispatch(loadTradeMessages(tradeId)),
        tradeAction: action => dispatch(tradeAction(tradeId, action)),
        tradeTimeout: timeout => dispatch(tradeTimeout(tradeId, timeout)),
        describeDispute: description => dispatch(describeDispute(tradeId, description)),
        sendMessage: message => dispatch(sendTradeMessage(tradeId, message)),
        sendFile: file => dispatch(sendTradeFile(tradeId, file)),
        tradeFeedback: code => dispatch(tradeFeedback(tradeId, code)),
        sendDisputeAdminMessage: message => dispatch(sendDisputeAdminMessage(tradeId, message)),
        loadDisputeAdminMessages: () => dispatch(loadDisputeAdminMessages(tradeId)),
        sendDisputeAdminFile: file => dispatch(sendDisputeAdminFile(tradeId, file)),
        setTradeDetails: details => dispatch(setTradeDetails(tradeId, details)),
        cancelPromptTradeDetails: () => dispatch(cancelPromptTradeDetails(tradeId))
    };
};

export default compose(
    ReactTimeout,
    withNamespaces('trades'),
    withStyles(styles),
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(Trade);

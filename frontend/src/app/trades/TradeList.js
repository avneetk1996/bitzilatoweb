// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';
import {
    withStyles,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableRow,
    TableHead,
    Grid,
    Typography,
    Tab,
    Tabs
} from '@material-ui/core';
import { push } from 'connected-react-router';
import Helmet from 'react-helmet';

import { P2P_URL, MARKET_URL } from '../../config';
import { loadTrades } from '../../redux/actions/';
import { Link } from 'react-router-dom';

const styles = () => ({
    table: {
        minWidth: 500
    },
    tableWrapper: {
        overflowX: 'auto'
    },
    noneDecoration: {
        textDecoration: 'none'
    },
    row: {
        '&:hover': {
            backgroundColor: '#efefef',
            cursor: 'pointer'
        }
    }
});

class TradeList extends Component {
    static propTypes = {
        t: PropTypes.func.isRequired,
        classes: PropTypes.object.isRequired,

        trades: PropTypes.array,
        loadTrades: PropTypes.func,
        openTrade: PropTypes.func,
        openMarketTrades: PropTypes.func,
        marketAvailable: PropTypes.bool
    };

    componentDidMount() {
        this.props.loadTrades();
    }

    handleChangeTab = (e, value) => {
        if (value === 'market') this.props.openMarketTrades();
    };

    render() {
        const { t, trades, classes, marketAvailable } = this.props;

        return (
            <Grid container spacing={24}>
                <Helmet>
                    <title>{t('tradeListTitle')}</title>
                </Helmet>

                <Grid item xs={12}>
                    <Typography variant="h6">{t('tradeListTitle')}</Typography>
                </Grid>

                {marketAvailable && (
                    <Grid item xs={12}>
                        <Tabs value="" onChange={this.handleChangeTab}>
                            <Tab label="P2P" value="" />
                            <Tab label={t('Market')} value="market" />
                        </Tabs>
                    </Grid>
                )}

                <Grid item xs={12}>
                    <Paper>
                        <div className={classes.tableWrapper}>
                            <Table className={classes.table}>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>{t('Status')}</TableCell>
                                        <TableCell>{t('Paymethod')}</TableCell>
                                        <TableCell>{t('Amount')}</TableCell>
                                        <TableCell>{t('Partner')}</TableCell>
                                    </TableRow>
                                </TableHead>

                                <TableBody>
                                    {trades && trades.map(trade => (
                                        <TableRow
                                            key={trade.id}
                                            className={classes.row}
                                            onClick={() => this.props.openTrade(trade.id)}
                                        >
                                            <TableCell>{t(_.camelCase(trade.status))}</TableCell>

                                            <TableCell>{trade.paymethod.name}</TableCell>

                                            <TableCell>
                                                {trade.currency.amount} {trade.currency.code}/
                                                {trade.cryptocurrency.amount} {trade.cryptocurrency.code}
                                            </TableCell>

                                            <TableCell>
                                                <Link
                                                    to={`${P2P_URL}/users/${trade.partner}`}
                                                    className={classes.noneDecoration}
                                                    onClick={e => e.stopPropagation()}
                                                >
                                                    {trade.partner}
                                                </Link>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </div>
                    </Paper>
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = state => ({
    trades: state.trades.active,
    marketAvailable: _.includes(_.get(state, 'profile.features'), 'MARKET')
});

const mapDispatchToProps = dispatch => ({
    loadTrades: () => dispatch(loadTrades()),
    openTrade: id => dispatch(push(`${P2P_URL}/trades/${id}`)),
    openMarketTrades: id => dispatch(push(`${MARKET_URL}/orders/`))
});

export default compose(
    withNamespaces('trades'),
    withStyles(styles),
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(TradeList);

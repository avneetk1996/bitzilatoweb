// -*- rjsx -*-
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, Form, reduxForm } from 'redux-form';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import { renderTextField } from '../common/redux-form-helpers';

class DisputeBlock extends Component {
    constructor() {
        super();

        this.state = {
            opened: false
        };
    }

    static propTypes = {
        handleSubmit: PropTypes.func.isRequired,
        createDispute: PropTypes.func
    };

    openDialog = () => {
        this.setState({
            opened: true
        });
    };

    closeDialog = () => {
        this.setState({
            opened: false
        });
    };

    onSubmit = values => {
        this.props.createDispute(values.reason);
    };

    render() {
        const { handleSubmit } = this.props;
        const { opened } = this.state;

        return (
            <span>
                <Button onClick={this.openDialog} fullWidth color="secondary" variant="contained">
                    Начать спор
                </Button>

                <Dialog open={opened} onClose={this.closeDialog} aria-labelledby="form-dialog-title">
                    <Form onSubmit={handleSubmit(this.onSubmit)}>
                        <DialogTitle id="form-dialog-title">Открытие спора</DialogTitle>
                        <DialogContent>
                            <DialogContentText>Пожалуйста, опишите причину спора</DialogContentText>
                            <Field
                                name="reason"
                                component={renderTextField}
                                label="Причина спора"
                                fullWidth
                                multiline
                                type="text"
                            />
                        </DialogContent>

                        <DialogActions>
                            <Button onClick={this.closeDialog} color="default">
                                Отмена
                            </Button>
                            <Button color="secondary" variant="contained" type="submit">
                                Открыть спор
                            </Button>
                        </DialogActions>
                    </Form>
                </Dialog>
            </span>
        );
    }
}

export default reduxForm({ form: 'requestForDisputeForm' })(DisputeBlock);

// -*- rjsx -*-
import React from 'react';
import PropTypes from 'prop-types';
import { IconButton } from '@material-ui/core';

const Feedback = props => {
    const { feedbackRatings, markTrade } = props;

    return (
        <div>
            {Boolean(feedbackRatings) &&
                feedbackRatings.map(rating => (
                    <IconButton key={rating.code} onClick={() => markTrade(rating.code)}>
                        {rating.emoji}
                    </IconButton>
                ))}
        </div>
    );
};

Feedback.propTypes = {
    feedbackRatings: PropTypes.array,
    markTrade: PropTypes.func
};

export default Feedback;

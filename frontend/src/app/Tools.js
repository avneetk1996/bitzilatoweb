// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';
import Helmet from 'react-helmet';
import { Grid, withStyles, Button, Typography } from '@material-ui/core';
import green from '@material-ui/core/colors/green';

import { updateProfile, openAccessToken } from '../redux/actions';

const styles = () => ({
    greenBtn: {
        backgroundColor: green[600],
        '&:hover': {
            backgroundColor: green[800]
        }
    }
});

class Tools extends Component {
    static propTypes = {
        t: PropTypes.func.isRequired,
        classes: PropTypes.object.isRequired,

        safeModeEnabled: PropTypes.bool.isRequired,
        marketAvailable: PropTypes.bool.isRequired,
        updateProfile: PropTypes.func.isRequired,
        openAccessToken: PropTypes.func.isRequired
    };

    render() {
        const { t, classes, safeModeEnabled, marketAvailable, updateProfile, openAccessToken } = this.props;

        return (
            <Grid container direction="column" spacing={32}>
                <Grid item>
                    <Helmet>
                        <title>🛠 {t('Tools')}</title>
                    </Helmet>

                    <Grid container>
                        <Grid item xs={12}>
                            <Typography variant="h6">🛠 {t('Tools')}</Typography>
                        </Grid>

                        <Grid item xs={12}>
                            <Typography variant="body2">
                                <strong>{t('toolsDescription')}</strong>
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item>
                    <Grid container spacing={8}>
                        <Grid item xs={12} sm={3}>
                            <Typography variant="body1">
                                <strong>{t('Safe Mode')}</strong>
                            </Typography>
                        </Grid>

                        <Grid item xs={12} sm={9}>
                            <Typography variant="body2">{t('safeModeDescription')}</Typography>
                        </Grid>

                        <Grid item xs={12}>
                            {safeModeEnabled === true && (
                                <Button
                                    variant="contained"
                                    color="secondary"
                                    type="submit"
                                    onClick={() => updateProfile({ safeModeEnabled: !safeModeEnabled })}
                                    fullWidth
                                >
                                    {safeModeEnabled ? t('Disable safe mode') : t('Enable safe mode')}
                                </Button>
                            )}

                            {safeModeEnabled === false && (
                                <Button
                                    variant="contained"
                                    className={classes.greenBtn}
                                    type="submit"
                                    onClick={() => updateProfile({ safeModeEnabled: !safeModeEnabled })}
                                    fullWidth
                                >
                                    {safeModeEnabled ? t('Disable safe mode') : t('Enable safe mode')}
                                </Button>
                            )}

                            {safeModeEnabled === null && (
                                <Typography variant="body1">{t('unavailableSafeModeDisable')}</Typography>
                            )}
                        </Grid>
                    </Grid>
                </Grid>

                {marketAvailable && (
                    <Grid item>
                        <Grid container spacing={8}>
                            <Grid item xs={12} sm={3}>
                                <Typography variant="body1">
                                    <strong>{t('API token')}</strong>
                                </Typography>
                            </Grid>

                            <Grid item xs={12} sm={9}>
                                <Typography variant="body2">{t('APItokenDesc')}</Typography>
                            </Grid>

                            <Grid item xs={12}>
                                <Button
                                    variant="contained"
                                    color="secondary"
                                    type="submit"
                                    onClick={openAccessToken}
                                    fullWidth
                                >
                                    {t('Show API token')}
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                )}
            </Grid>
        );
    }
}

const mapStateToProps = state => ({
    safeModeEnabled: _.get(state, 'profile.settings.safeModeEnabled'),
    marketAvailable: _.includes(_.get(state, 'profile.features'), 'MARKET')
});

export default compose(
    withNamespaces('tools'),
    withStyles(styles),
    connect(
        mapStateToProps,
        { updateProfile, openAccessToken }
    )
)(Tools);

// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Route, Switch, NavLink } from 'react-router-dom';
import { withNamespaces } from 'react-i18next';
import { MenuItem } from '@material-ui/core';
import { withStyles, Paper, Drawer, Divider } from '@material-ui/core';
import AccountCircle from '@material-ui/icons/AccountCircle';
import PersonAdd from '@material-ui/icons/PersonAdd';
import Settings from '@material-ui/icons/Settings';
import Assessment from '@material-ui/icons/Assessment';

import { PROFILE_URL } from '../../config';
import { loadReferralLinks, loadProfile, updateProfile, loadDealStats } from '../../redux/actions';

import ReferralBlock from './ReferralBlock';
import ProfileBlock from './ProfileBlock';
import SettingsBlock from './SettingsBlock';
import TelegramBlock from './TelegramBlock';
import Reports from './Reports';

//const drawerWidth = 240;

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    appFrame: {
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%'
    },
    drawerPaper: {
        position: 'relative',
        [theme.breakpoints.down('sm')]: {
            overflowX: 'hidden',
            width: theme.spacing.unit * 7
        }
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 5,
        [theme.breakpoints.down('sm')]: {
            padding: '10px 2px'
        }
    },
    menuIcon: {
        marginRight: 20
    },
    menuText: {
        textDecoration: 'none'
    },
    telegramIcon: {
        width: 24,
        height: 24,
        verticalAlign: 'middle'
    }
});

class ProfileContainer extends Component {
    static propTypes = {
        t: PropTypes.func.isRequired,
        classes: PropTypes.object.isRequired,

        referralLinks: PropTypes.object,
        profile: PropTypes.object,
        dealStats: PropTypes.array,

        loadReferralLinks: PropTypes.func.isRequired,
        loadProfile: PropTypes.func.isRequired,
        updateProfile: PropTypes.func.isRequired,
        loadDealStats: PropTypes.func.isRequired
    };

    componentDidMount() {
        this.props.loadReferralLinks(_.get(this.props, 'profile.cryptocurrency'));
        this.props.loadDealStats();
    }

    acceptLicenceAgreement = () => {
        this.props.updateProfile({ licensingAgreementAccepted: true });
    };

    render() {
        const { profile, dealStats, referralLinks, t, classes } = this.props;
        const paperStyle = { padding: 15, marginBottom: 15 };

        return (
            <div>
                <Helmet>
                    <title>{t('Profile')}</title>
                </Helmet>

                {profile && profile.verification && !profile.verification.status && (
                    <Paper style={paperStyle}>
                        <a
                            href={profile.verification.url}
                            rel="noopener noreferrer"
                            target="_blank"
                            style={{ color: 'orange' }}
                        >
                            {t('Please, verify your account')}
                        </a>
                    </Paper>
                )}

                <Paper className={classes.appFrame}>
                    {this.drawer()}

                    <Paper className={classes.content}>
                        <Switch>
                            <Route exact path={PROFILE_URL}>
                                <ProfileBlock profile={{ ...profile, dealStats }} />
                            </Route>

                            <Route exact path={`${PROFILE_URL}/referral`}>
                                <ReferralBlock referralLinks={referralLinks} />
                            </Route>

                            <Route exact path={`${PROFILE_URL}/settings`}>
                                <SettingsBlock />
                            </Route>

                            <Route exact path={`${PROFILE_URL}/telegram`}>
                                <TelegramBlock telegram={profile.telegram} />
                            </Route>

                            <Route exact path={`${PROFILE_URL}/reports`}>
                                <Reports />
                            </Route>
                        </Switch>
                    </Paper>
                </Paper>
            </div>
        );
    }

    drawer() {
        const { t, classes } = this.props;
        const activeStyle = { backgroundColor: '#efefef' };

        return (
            <Drawer
                variant="permanent"
                classes={{
                    paper: classes.drawerPaper
                }}
                anchor="left"
            >
                <NavLink activeStyle={activeStyle} exact to={PROFILE_URL}>
                    <MenuItem>
                        <div className={classes.menuIcon}>
                            <AccountCircle />
                        </div>

                        <div className={classes.menuText}>{t('My profile')}</div>
                    </MenuItem>
                </NavLink>
                <Divider />

                <NavLink activeStyle={activeStyle} to={`${PROFILE_URL}/settings`}>
                    <MenuItem>
                        <div className={classes.menuIcon}>
                            <Settings />
                        </div>

                        <div className={classes.menuText}>{t('Profile settings')}</div>
                    </MenuItem>
                </NavLink>
                <Divider />

                <NavLink activeStyle={activeStyle} to={`${PROFILE_URL}/referral`}>
                    <MenuItem>
                        <div className={classes.menuIcon}>
                            <PersonAdd />
                        </div>

                        <div className={classes.menuText}>{t('Referrals')}</div>
                    </MenuItem>
                </NavLink>
                <Divider />

                <NavLink activeStyle={activeStyle} to={`${PROFILE_URL}/telegram`}>
                    <MenuItem>
                        <div className={classes.menuIcon}>
                            {/*telegram icon*/}
                            <img
                                alt=""
                                className={classes.telegramIcon}
                                src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTguMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDE4OS40NzMgMTg5LjQ3MyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMTg5LjQ3MyAxODkuNDczOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4Ij4KPGc+Cgk8cGF0aCBkPSJNMTUyLjUzMSwxNzkuNDc2Yy0xLjQ4LDAtMi45NS0wLjQzOC00LjIxMS0xLjI5M2wtNDcuNjQxLTMyLjMxNmwtMjUuNTUyLDE4LjM4NmMtMi4wMDQsMS40NDEtNC41ODcsMS44MDQtNi45MTQsMC45NzIgICBjLTIuMzI0LTAuODM0LTQuMDg5LTIuNzU5LTQuNzE5LTUuMTQ2bC0xMi44My00OC42MjJMNC44MjEsOTMuOTI4Yy0yLjg4Ni0xLjEwNC00LjgtMy44NjUtNC44MjEtNi45NTUgICBjLTAuMDIxLTMuMDksMS44NTUtNS44NzcsNC43MjctNy4wMmwxNzQuMzEyLTY5LjM2YzAuNzkxLTAuMzM2LDEuNjI4LTAuNTMsMi40NzItMC41ODJjMC4zMDItMC4wMTgsMC42MDUtMC4wMTgsMC45MDYtMC4wMDEgICBjMS43NDgsMC4xMDQsMy40NjUsMC44MTYsNC44MDUsMi4xM2MwLjEzOSwwLjEzNiwwLjI3MSwwLjI3NSwwLjM5NiwwLjQyYzEuMTEsMS4yNjgsMS43MiwyLjgxNCwxLjgzNSw0LjM4OSAgIGMwLjAyOCwwLjM5NiwwLjAyNiwwLjc5Ny0wLjAwOSwxLjE5OGMtMC4wMjQsMC4yODYtMC4wNjUsMC41NzEtMC4xMjMsMC44NTRMMTU5Ljg5OCwxNzMuMzhjLTAuNDczLDIuNDgtMi4xNjEsNC41NTYtNC40OTMsNS41MjMgICBDMTU0LjQ4LDE3OS4yODcsMTUzLjUwMywxNzkuNDc2LDE1Mi41MzEsMTc5LjQ3NnogTTEwNC44NjIsMTMwLjU3OWw0Mi40MzcsMjguNzg1TDE3MC4xOTMsMzkuMjRsLTgyLjY4Nyw3OS41NjZsMTcuMTU2LDExLjYzOCAgIEMxMDQuNzMxLDEzMC40ODcsMTA0Ljc5NywxMzAuNTMzLDEwNC44NjIsMTMwLjU3OXogTTY5LjUzNSwxMjQuMTc4bDUuNjgyLDIxLjUzbDEyLjI0Mi04LjgwOWwtMTYuMDMtMTAuODc0ICAgQzcwLjY4NCwxMjUuNTIxLDcwLjA0NiwxMjQuODkzLDY5LjUzNSwxMjQuMTc4eiBNMjguMTM2LDg2Ljc4MmwzMS40NzgsMTIuMDM1YzIuMjU1LDAuODYyLDMuOTU3LDIuNzU4LDQuNTczLDUuMDkybDMuOTkyLDE1LjEyOSAgIGMwLjE4My0xLjc0NSwwLjk3NC0zLjM4NywyLjI1OS00LjYyNEwxNDkuMjI3LDM4LjZMMjguMTM2LDg2Ljc4MnoiIGZpbGw9IiMwMDAwMDAiLz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K"
                            />
                            {/*telegram icon*/}
                        </div>

                        <div className={classes.menuText}>{t('Telegram merge')}</div>
                    </MenuItem>
                </NavLink>
                <Divider />

                <NavLink activeStyle={activeStyle} to={`${PROFILE_URL}/reports`}>
                    <MenuItem>
                        <div className={classes.menuIcon}>
                            <Assessment />
                        </div>

                        <div className={classes.menuText}>{t('Reports')}</div>
                    </MenuItem>
                </NavLink>
                <Divider />
            </Drawer>
        );
    }
}

const mapStateToProps = state => {
    return {
        referralLinks: state.profile.referralLinks,
        profile: { ...state.profile.settings, ...state.profile.stats },
        dealStats: _.transform(
            state.wallets,
            (result, value, key) => {
                result.push({
                    cryptocurrency: key,
                    totalCount: _.get(value, 'stats.totalCount'),
                    totalAmount: _.get(value, 'stats.totalAmount')
                });
            },
            []
        )
    };
};

const mapDispatchToProps = dispatch => {
    return {
        loadReferralLinks: data => dispatch(loadReferralLinks(data)),
        loadProfile: data => dispatch(loadProfile(data)),
        updateProfile: data => dispatch(updateProfile(data)),
        loadDealStats: () => dispatch(loadDealStats())
    };
};

export default withNamespaces('profile')(
    withStyles(styles)(
        connect(
            mapStateToProps,
            mapDispatchToProps
        )(ProfileContainer)
    )
);

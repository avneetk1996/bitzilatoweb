// -*- rjsx -*-
import React from 'react';
import { Grid, Typography, withStyles } from '@material-ui/core';
import { withNamespaces } from 'react-i18next';

import TimeBall from '../users/TimeBall';
import UserInfo from '../users/UserInfo';

const style = {
    thumbUp: {
        marginBottom: '-4px'
    },
    thumbDown: {
        marginBottom: '-10px'
    },
    ratingStyle: {
        marginTop: '-6px'
    },
    noMargin: {
        marginTop: 0
    },
    margin: {
        marginTop: 12
    },
    paperPadding: {
        padding: 20,
        marginBottom: 40
    },
    blockButton: {
        float: 'right'
    },
    greeting: {
        whiteSpace: 'pre-wrap'
    },
    greetingWrap: {
        padding: 20
    }
};

const ProfileBlock = props => {
    const { profile } = props;

    const user = {
        ...profile,
        verification: profile.verification.status
    };

    return (
        profile && (
            <Grid container>
                <Grid item xs={12} sm={6} style={{ padding: '10px' }}>
                    <Typography variant="h6">
                        {user.name} <TimeBall time={user.lastActivity} />
                    </Typography>

                    <UserInfo user={user} />
                </Grid>
            </Grid>
        )
    );
};

ProfileBlock.propTypes = {};

export default withNamespaces('userInfo')(withStyles(style)(ProfileBlock));

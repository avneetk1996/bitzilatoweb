// -*- rjsx -*-
import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { withNamespaces } from 'react-i18next';
import copy from 'copy-to-clipboard';
import { Grid, withStyles, Table, TableBody, TableCell, TableRow, Typography, Button } from '@material-ui/core';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3
    },
    table: {
        minWidth: 500
    },
    tableWrapper: {
        overflowX: 'auto'
    }
});

function linkType(type) {
    if (type === 'telegram') {
        return 'Telegram';
    } else if (type === 'money-changer') {
        return 'Веб P2P';
    } else {
        return type;
    }
}

const ReferralBlockView = props => {
    const { referralLinks, t } = props;

    return (
        <Grid container spacing={16}>
            <Helmet>
                <title>{t('Referral program')}</title>
            </Helmet>

            <Grid item xs={12}>
                <Typography variant="h6">{t('Referral program')}</Typography>
            </Grid>

            <Grid item xs={12}>
                <Typography>{t('Referral description text1')}</Typography>

                <ul>
                    <li>
                        <Typography>
                            Комиссия при сделке — 0,8%. Вы получаете 75% от комиссии сервиса или 0,6% от суммы сделки.
                        </Typography>
                    </li>

                    <li>
                        <Typography>
                            Совсем скоро мы запустим биржу. Вы будете получать{' '}
                            <strong>90% от комиссии сервиса с каждого вашего реферала!</strong>
                        </Typography>
                    </li>

                    <li>
                        <Typography>
                            Реферальная программа одноуровневая, не имеет лимита приглашений и начинает действовать
                            моментально.
                        </Typography>
                    </li>

                    <li>
                        <Typography>
                            Вознаграждение за сделки приглашенных вами пользователей перечисляется моментально на ваш
                            счет.
                        </Typography>
                    </li>

                    <li>
                        <Typography>
                            Пользователь будет считаться вашим рефералом весь срок действия реферальной программы.
                        </Typography>
                    </li>
                </ul>

                <Typography>
                    Используйте ссылку ниже для инвайта, вы также можете использовать чеки и ссылки на объявления.
                </Typography>
            </Grid>

            <Grid item xs={12}>
                <Typography variant="subtitle1">{t('List of referral links')}</Typography>
            </Grid>

            <Grid item xs={12}>
                <Table>
                    <TableBody>
                        {_.map(referralLinks && referralLinks.links, link => (
                            <TableRow key={link.url}>
                                <TableCell>
                                    <a href={link.url}>{linkType(link.type)}</a>
                                </TableCell>

                                <TableCell align="right">
                                    <Button color="primary" onClick={() => copy(link.url)}>
                                        copy
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Grid>
        </Grid>
    );
};

ReferralBlockView.propTypes = {
    t: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    referralLinks: PropTypes.object
};

export default withNamespaces('profile')(withStyles(styles)(ReferralBlockView));

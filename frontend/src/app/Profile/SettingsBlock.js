// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import { connect } from 'react-redux';
import { Field, reduxForm, Form } from 'redux-form';
import Helmet from 'react-helmet';
import { Button, Grid, Typography, withStyles } from '@material-ui/core';
import green from '@material-ui/core/colors/green';

import { renderSelectField, renderTextField } from '../common/redux-form-helpers';
import { updateProfile } from '../../redux/actions';

const styles = () => ({
    greenBtn: {
        backgroundColor: green[600],
        '&:hover': {
            backgroundColor: green[800]
        }
    }
});

class SettingsBlock extends Component {
    static propTypes = {
        t: PropTypes.func.isRequired,
        classes: PropTypes.object.isRequired,
        handleSubmit: PropTypes.func.isRequired,

        currencies: PropTypes.array,
        cryptocurrencies: PropTypes.array,
        serviceFee: PropTypes.bool,
        safeModeEnabled: PropTypes.bool,

        updateProfile: PropTypes.func.isRequired
    };

    onSubmit = values => {
        this.props.updateProfile(values);
    };

    changeLanguage = () => {};

    render() {
        const languages = [
            {
                value: 'en',
                label: 'English'
            },
            {
                value: 'ru',
                label: 'Русский'
            }
        ];

        const { currencies, cryptocurrencies, serviceFee, safeModeEnabled, t, classes, handleSubmit } = this.props;

        return (
            <Grid container direction="column" spacing={24}>
                <Grid item xs={12}>
                    <Helmet>
                        <title>{t('Profile settings')}</title>
                    </Helmet>

                    <Typography variant="h6">{t('Profile settings')}</Typography>
                </Grid>

                {(_.isBoolean(serviceFee) || _.isBoolean(safeModeEnabled)) && (
                    <Grid item xs={12}>
                        <Grid container spacing={8}>
                            {_.isBoolean(serviceFee) && (
                                <Grid item xs={12}>
                                    {serviceFee ? (
                                        <Button
                                            variant="contained"
                                            color="secondary"
                                            type="submit"
                                            onClick={() => this.props.updateProfile({ serviceFee: false })}
                                            fullWidth
                                        >
                                            {t('Disable service fee')}
                                        </Button>
                                    ) : (
                                        <Button
                                            variant="contained"
                                            className={classes.greenBtn}
                                            type="submit"
                                            onClick={() => this.props.updateProfile({ serviceFee: true })}
                                            fullWidth
                                        >
                                            {t('Enable service fee')}
                                        </Button>
                                    )}
                                </Grid>
                            )}
                        </Grid>
                    </Grid>
                )}

                <Grid item xs={12}>
                    <Form onSubmit={handleSubmit(this.onSubmit)}>
                        <Grid container direction="column" spacing={24}>
                            <Grid item>
                                <Typography variant="subtitle1">{t('Greeting')}</Typography>
                            </Grid>

                            <Grid item xs>
                                <Field
                                    name="greeting"
                                    multiline={true}
                                    rows={5}
                                    rowsMax={12}
                                    helperText="Показывается в общедоступном профиле"
                                    component={renderTextField}
                                    fullWidth
                                />
                            </Grid>

                            <Grid item xs>
                                <Grid container direction="row" spacing={16}>
                                    <Grid item xs>
                                        {currencies && (
                                            <Field
                                                select
                                                name="currency"
                                                label={t('Default currency')}
                                                component={renderSelectField}
                                                options={_.map(currencies, item => ({
                                                    value: item.code,
                                                    label: `${item.code} - ${item.name}`
                                                }))}
                                                fullWidth
                                            />
                                        )}

                                        {!currencies && (
                                            <Field
                                                select
                                                disabled
                                                name="currency"
                                                label={t('Default currency')}
                                                component={renderSelectField}
                                                options={[{}]}
                                                fullWidth
                                            />
                                        )}
                                    </Grid>

                                    <Grid item xs>
                                        {cryptocurrencies && (
                                            <Field
                                                select
                                                name="cryptocurrency"
                                                label={t('Default cryptocurrency')}
                                                component={renderSelectField}
                                                options={_.map(cryptocurrencies, item => ({
                                                    value: item.code,
                                                    label: `${item.code} - ${item.name}`
                                                }))}
                                                fullWidth
                                            />
                                        )}

                                        {!cryptocurrencies && (
                                            <Field
                                                select
                                                disabled
                                                name="cryptocurrency"
                                                label={t('Default cryptocurrency')}
                                                component={renderSelectField}
                                                options={[{}]}
                                                fullWidth
                                            />
                                        )}
                                    </Grid>

                                    <Grid item xs>
                                        <Field
                                            select
                                            name="lang"
                                            label={t('Language')}
                                            component={renderSelectField}
                                            options={languages}
                                            fullWidth
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid item xs>
                                <Button variant="contained" color="primary" type="submit">
                                    {t('Save')}
                                </Button>
                            </Grid>
                        </Grid>
                    </Form>
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = state => {
    return {
        currencies: state.refs.currencies,
        cryptocurrencies: state.refs.cryptocurrencies,
        initialValues: {
            currency: state.profile.settings.currency,
            cryptocurrency: state.profile.settings.cryptocurrency,
            lang: state.profile.settings.lang,
            greeting: state.profile.settings.greeting
        },
        serviceFee: _.get(state, 'profile.settings.serviceFee'),
        safeModeEnabled: _.get(state, 'profile.settings.safeModeEnabled')
    };
};

const mapDispatchToProps = dispatch => {
    return {
        updateProfile: data => dispatch(updateProfile(data))
    };
};

export default withNamespaces('profile')(
    withStyles(styles)(
        connect(
            mapStateToProps,
            mapDispatchToProps
        )(
            reduxForm({
                form: 'profileSettingsForm',
                enableReinitialize: true
            })(SettingsBlock)
        )
    )
);

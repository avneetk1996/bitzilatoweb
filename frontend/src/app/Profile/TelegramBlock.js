// -*- rjsx -*-
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Typography } from '@material-ui/core';
import Helmet from 'react-helmet';
import { withNamespaces } from 'react-i18next';
import TelegramLoginButton from 'react-telegram-login';
import { BOT_NAME } from '../../config';
import { mergeProfile } from '../../redux/actions';

class TelegramBlock extends Component {
    static propTypes = {
        t: PropTypes.func.isRequired,
        telegram: PropTypes.bool,
        mergeProfile: PropTypes.func.isRequired
    };

    handleTelegramResponse = authData => {
        this.props.mergeProfile(authData);
    };

    render() {
        const { t, telegram } = this.props;

        return (
            <div>
                <Helmet>
                    <title>{t('Telegram merge')}</title>
                </Helmet>

                <Typography variant="h6">{t('Telegram merge')}</Typography>
                <br />
                <br />
                {telegram && <p>{t('Merged account')}</p>}

                {!telegram && (
                    <div>
                        <p>
                            {' '}
                            Веб-обменник Bitzlato интегрирован с telegram-ботами! Привяжите аккаунт telegram, с которого
                            будете совершать сделки, и вы сможете покупать или продавать криптовалюты как в веб-версии
                            на p2p.bitzlato.com, так и в боте @BTC_CHANGE_BOT или других наших{' '}
                            <a href="https://bitzlato.com/ru/p2p-exchange">ботах</a>!<br /> <br />
                            ⚠️ На территории России вам необходимо использовать vpn для интеграции учетной записи с
                            Вашим telegram-аккаунтом.
                        </p>

                        <TelegramLoginButton dataOnauth={this.handleTelegramResponse} botName={BOT_NAME} />
                    </div>
                )}
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    mergeProfile: data => dispatch(mergeProfile(data))
});

export default withNamespaces('profile')(
    connect(
        null,
        mapDispatchToProps
    )(TelegramBlock)
);

// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';
import { Grid, Typography, Tabs, Tab } from '@material-ui/core';
import Helmet from 'react-helmet';
import { push } from 'connected-react-router';

import { PROFILE_URL } from '../../config';
import { loadReportList } from '../../redux/actions';

class Reports extends Component {
    static propTypes = {
        t: PropTypes.func.isRequired,

        auth: PropTypes.object,
        reports: PropTypes.array,
        loadReportList: PropTypes.func.isRequired,
        push: PropTypes.func.isRequired,
    };

    handleChangeTab = (e, value) => {
        if (value === 'market') this.props.push(`${PROFILE_URL}/market-history`);
    };

    componentDidMount() {
        this.props.loadReportList();
    }

    render() {
        const { reports, t } = this.props;

        return (
            <Grid container spacing={16}>
                <Helmet>
                    <title>{t('Reports')}</title>
                </Helmet>

                <Grid item xs={12}>
                    <Typography variant="h6">📑 {t('Reports')}</Typography>
                </Grid>

                <Grid item xs={12}>
                    {_.map(reports, ({ code, description, format }) => (
                        <Typography key={code}>
                            <a href={`${PROFILE_URL}/reports/${code}`} donwload={`${description}.${format}`}>
                                {description}.{format}
                            </a>
                        </Typography>
                    ))}
                </Grid>
            </Grid>
        );
    }
}

export default compose(
    withNamespaces('profile'),
    connect(
        state => ({
            reports: state.refs.reports,
        }),
        { loadReportList, push }
    )
)(Reports);

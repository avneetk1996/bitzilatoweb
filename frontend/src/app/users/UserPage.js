// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';
import { Grid, Typography, Button, withStyles } from '@material-ui/core';
import Chat from '../common/Chat';

import TimeBall from './TimeBall';
import { loadUserInfo, loadUserChat, blockUser, transferToUser, sendUserMessage } from '../../redux';

import UserInfo from './UserInfo';
import SendMoneyDialog from './SendMoneyDialog';

const style = theme => ({
    thumbUp: {
        marginBottom: '-4px',
        color: '#4CAF50'
    },
    thumbDown: {
        marginBottom: '-10px',
        color: '#F44336'
    },
    ratingStyle: {
        marginTop: '-6px'
    },
    noMargin: {
        marginTop: 0
    },
    margin: {
        marginTop: 12
    },
    paperPadding: {
        padding: 20,
        marginBottom: 40
    },
    blockButton: {
        float: 'right'
    },
    greetingBlock: {
        whiteSpace: 'pre-wrap',
        margin: '10px 0',
        padding: '10px'
    },
    greetingText: {
        padding: '10px'
    },
    commandButton: {
        width: '90%'
    },
    userInfo: {
        margin: '20px 0'
    },
    tableWrapper: {
        overflowX: 'auto'
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default
        }
    },
    cell: {
        [theme.breakpoints.down('sm')]: {
            fontSize: '80%',
            padding: '5px'
        }
    }
});

class UserPage extends Component {
    state = {
        sendMoneyOpened: false
    };

    static propTypes = {
        t: PropTypes.func.isRequired,
        classes: PropTypes.object.isRequired,

        isLogged: PropTypes.bool.isRequired,
        user: PropTypes.object,
        cryptocurrencies: PropTypes.array,
        messages: PropTypes.array,

        loadUserInfo: PropTypes.func,
        loadUserChat: PropTypes.func,
        blockUser: PropTypes.func,
        transferToUser: PropTypes.func,
        sendMessage: PropTypes.func
    };

    componentDidMount() {
        this.props.loadUserInfo();
        if (this.props.isLogged) {
            this.props.loadUserChat();
        }
    }

    openSendMoneyDialog = () => {
        this.setState({
            sendMoneyOpened: true
        });
    };

    closeSendMoneyDialog = () => {
        this.setState({
            sendMoneyOpened: false
        });
    };

    sendMoney = values => {
        if (values && values.cryptocurrency && values.amount) {
            this.props.transferToUser(values.cryptocurrency, values.amount);
        }

        this.closeSendMoneyDialog();
    };

    sendMessage = value => {
        this.props.sendMessage(value.message);
    };

    render() {
        const user = this.props.user || {};
        const { t, isLogged, cryptocurrencies, classes } = this.props;
        const { sendMoneyOpened } = this.state;

        return (
            <Grid container>
                <Grid item xs={12} sm={6} style={{ padding: '10px' }}>
                    <Grid container spacing={8}>
                        <Grid item xs={12}>
                            <Typography variant="h6">
                                {user.name} <TimeBall time={user.lastActivity} />
                            </Typography>
                        </Grid>

                        <Grid item xs={12}>
                            <UserInfo user={user} />
                        </Grid>

                        {isLogged &&
                            user.blocked && (
                                <Grid item xs={12}>
                                    <Grid container spacing={8}>
                                        <Grid item xs={12}>
                                            <Typography variant="subtitle1">
                                                {t('You blocked this user earlier and can unblock him now')}
                                            </Typography>
                                        </Grid>

                                        <Grid item xs={12}>
                                            <Button
                                                color="secondary"
                                                variant="contained"
                                                onClick={() => this.props.blockUser(false)}
                                            >
                                                {t('Unblock')}
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            )}

                        {isLogged &&
                            !user.blocked && (
                                <Grid item xs={12}>
                                    <Grid container>
                                        {/* <Grid item xs={12} sm={6}> */}
                                        {/*     <Button */}
                                        {/*         onClick={this.openSendMoneyDialog} */}
                                        {/*         color="primary" */}
                                        {/*         className={classes.commandButton} */}
                                        {/*         variant="contained" */}
                                        {/*     > */}
                                        {/*         {t('Send money')} */}
                                        {/*     </Button> */}
                                        {/* </Grid> */}

                                        <Grid item xs={12} sm={6}>
                                            <Button
                                                color="secondary"
                                                onClick={() => this.props.blockUser(true)}
                                                className={classes.commandButton}
                                                variant="contained"
                                            >
                                                {t('Block')}
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            )}
                    </Grid>
                </Grid>

                {isLogged &&
                    user.chatAvailable && (
                        <Grid item xs={12} sm={6}>
                            <Chat
                                onSubmit={message => this.sendMessage(message)}
                                form={'chatWithUserForm'}
                                buttonVariant={'contained'}
                                messages={this.props.messages || []}
                                chatName="Чат"
                            />
                        </Grid>
                    )}

                {isLogged && (
                    <SendMoneyDialog
                        opened={sendMoneyOpened}
                        onSubmit={this.sendMoney}
                        cryptocurrencies={cryptocurrencies}
                        closeDialog={this.closeSendMoneyDialog}
                    />
                )}
            </Grid>
        );
    }
}

function mapStateToProps(state, ownProps) {
    const user = state.users && state.users[ownProps.match.params.publicName];
    return {
        user,
        cryptocurrencies: state.refs.cryptocurrencies,
        isLogged: !_.isEmpty(state.auth),
        messages: _.get(user, 'chat.messages')
    };
}

function mapDispatchToProps(dispatch, ownProps) {
    const publicName = ownProps.match.params.publicName;

    return {
        loadUserInfo: () => dispatch(loadUserInfo(publicName)),
        loadUserChat: () => dispatch(loadUserChat(publicName)),
        blockUser: flag => dispatch(blockUser(publicName, flag)),
        transferToUser: (cryptocurrency, amount) => dispatch(transferToUser(publicName, cryptocurrency, amount)),
        sendMessage: message => dispatch(sendUserMessage(publicName, message))
    };
}

export default withNamespaces('userInfo')(
    withRouter(
        withStyles(style)(
            connect(
                mapStateToProps,
                mapDispatchToProps
            )(UserPage)
        )
    )
);

// -*- rjsx -*-
import React from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import { Tooltip, withStyles } from '@material-ui/core';
import green from '@material-ui/core/colors/green';
import yellow from '@material-ui/core/colors/yellow';
import grey from '@material-ui/core/colors/grey';

const styles = () => ({
    wrap: {
        display: 'inline-block',
        verticalAlign: 'middle',
        marginLeft: '10px'
    },
    timeBall: {
        width: '15px',
        height: '15px',
        borderRadius: '50%',
        border: '1px solid #ccc'
    },
    green: {
        backgroundColor: green[500]
    },
    yellow: {
        backgroundColor: yellow[500]
    },
    grey: {
        backgroundColor: grey[500]
    }
});

export function lastActivityColor(time) {
    const then = new Date(time);
    const now = new Date();
    const diffInMinutes = Math.round((now.getTime() - then.getTime()) / (1000 * 60));

    if (diffInMinutes < 120) {
        return green[500];
    } else if (diffInMinutes >= 120 && diffInMinutes < 180) {
        return yellow[500];
    } else {
        return grey[500];
    }
}

const TimeBall = props => {
    const { time, t, classes } = props;
    const color = lastActivityColor(time);

    return (
        <span className={classes.wrap}>
            <Tooltip
                title={
                    <React.Fragment>
                        {t('Last online')}: {t('lastActivity', { lastActivity: time })}
                    </React.Fragment>
                }
            >
                <div className={classes.timeBall} style={{ backgroundColor: color }} />
            </Tooltip>
        </span>
    );
};

TimeBall.propTypes = {
    t: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    time: PropTypes.object
};

export default withNamespaces('userInfo')(withStyles(styles)(TimeBall));

// -*- rjsx -*-
import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { Grid, Typography, Paper, withStyles, Table, TableBody, TableRow, TableCell } from '@material-ui/core';
import { ThumbUp, ThumbDown } from '@material-ui/icons';
import { withNamespaces } from 'react-i18next';
import Emoji from 'react-emoji-render';
import Markdown from 'react-remarkable';

const style = theme => ({
    thumbUp: {
        marginBottom: '-4px',
        color: '#4CAF50'
    },
    thumbDown: {
        marginBottom: '-10px',
        color: '#F44336'
    },
    ratingStyle: {
        marginTop: '-6px'
    },
    greetingBlock: {
        whiteSpace: 'pre-wrap',
        margin: '10px 0',
        padding: '10px'
    },
    greetingText: {
        padding: '10px'
    },
    tableWrapper: {
        overflowX: 'auto'
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default
        }
    },
    cell: {
        [theme.breakpoints.down('sm')]: {
            fontSize: '80%',
            padding: '5px'
        }
    }
});

const UserInfo = props => {
    const { t, user = {}, classes } = props;

    let verification = t('adverts:no');
    if (user.suspicious) {
        verification = `🆘 ${t('adverts:Suspicious')}`;
    } else if (user.verification) {
        verification = `✅ ${t('adverts:Identity card')}`;
    }

    return (
        <Grid container spacing={8}>
            {user.greeting && (
                <Grid item>
                    <Paper className={classes.greetingBlock}>
                        <Emoji text={user.greeting || ''} />
                    </Paper>
                </Grid>
            )}

            <Grid item>
                <Grid container>
                    <Grid item className={classes.tableWrapper} xs={12}>
                        <Table>
                            <TableBody>
                                <TableRow className={classes.row}>
                                    <TableCell className={classes.cell}>{t('Verification')}</TableCell>
                                    <TableCell className={classes.cell}>{verification}</TableCell>
                                </TableRow>

                                <TableRow className={classes.row}>
                                    <TableCell className={classes.cell}>{t('Rating')}</TableCell>
                                    <TableCell className={classes.cell}>{user.rating}</TableCell>
                                </TableRow>

                                <TableRow className={classes.row}>
                                    <TableCell className={classes.cell}>{t('Feedback')}</TableCell>
                                    <TableCell className={classes.cell}>
                                        ({user.feedbacks && user.feedbacks.thumbUp}){' '}
                                        <ThumbUp className={classes.thumbUp} />
                                        &nbsp;/&nbsp; ({user.feedbacks && user.feedbacks.thumbDown}){' '}
                                        <ThumbDown className={classes.thumbDown} />
                                    </TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </Grid>

                    {_.some(user.dealStats, item => item.totalCount > 0) && (
                        <Grid item xs={12}>
                            <Typography variant="body2" component="div">
                                <Markdown>
                                    {t('dealsTotalTitle', {
                                        time: user.startOfUseDate && Date.now() - user.startOfUseDate.getTime()
                                    })}
                                </Markdown>
                            </Typography>
                        </Grid>
                    )}

                    {_.some(user.dealStats, item => item.totalCount > 0) && (
                        <Grid item className={classes.tableWrapper} xs={12}>
                            <Table>
                                <TableBody>
                                    {_.map(user.dealStats, item => {
                                        return (
                                            Boolean(item.totalCount > 0) && (
                                                <TableRow className={classes.row} key={item.cryptocurrency}>
                                                    <TableCell className={classes.cell}>
                                                        <Markdown>
                                                            {t('dealsTotalInfo', {
                                                                count: item.totalCount,
                                                                amount: item.totalAmount,
                                                                currency: item.cryptocurrency
                                                            })}
                                                        </Markdown>
                                                    </TableCell>
                                                </TableRow>
                                            )
                                        );
                                    })}
                                </TableBody>
                            </Table>
                        </Grid>
                    )}
                </Grid>
            </Grid>
        </Grid>
    );
};

UserInfo.propTypes = {
    t: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired
};

export default compose(
    withNamespaces(['userInfo', 'adverts']),
    withStyles(style)
)(UserInfo);

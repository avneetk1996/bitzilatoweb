// -*- rjsx -*-
import _ from 'lodash';
import React from 'react';
import { compose } from 'redux';
import { withNamespaces } from 'react-i18next';
import PropTypes from 'prop-types';
import { Grid, Button, Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';
import { reduxForm, Field, Form } from 'redux-form';
import { renderSelectField, renderTextField } from '../common/redux-form-helpers';

const SendMoneyDialog = props => {
    const { opened, closeDialog, handleSubmit, cryptocurrencies, t } = props;

    return (
        <Dialog open={opened} onClose={closeDialog} aria-labelledby="form-dialog-title">
            <Form onSubmit={handleSubmit}>
                <DialogTitle>Отправка средств</DialogTitle>

                <DialogContent>
                    <Grid container spacing={16}>
                        <Grid item xs={12}>
                            <Field
                                component={renderTextField}
                                label={t('Want to send')}
                                name="amount"
                                placeholder={t('Amount')}
                                margin="normal"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true
                                }}
                            />
                        </Grid>

                        <Grid item xs={12}>
                            <Field
                                select
                                name="cryptocurrency"
                                label={t('Cryptocurrency')}
                                component={renderSelectField}
                                options={_.map(cryptocurrencies, item => ({
                                    value: item.code,
                                    label: item.code
                                }))}
                                fullWidth
                            />
                        </Grid>
                    </Grid>
                </DialogContent>

                <DialogActions>
                    <Button fullWidth variant="contained" type="submit" color="primary">
                        {t('Send')}
                    </Button>
                </DialogActions>
            </Form>
        </Dialog>
    );
};

SendMoneyDialog.propTypes = {
    t: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    opened: PropTypes.bool,
    openDialog: PropTypes.func,
    closeDialog: PropTypes.func,
    cryptocurrencies: PropTypes.array
};

export default compose(
    withNamespaces('userInfo'),
    reduxForm({
        form: 'sendMoneyToUserForm',
        validate: (values, { t }) => {
            const errors = {};

            const amount = _.toNumber(values.amount);
            if (!_.isFinite(amount) || amount <= 0) {
                errors.amount = t('Invalid value');
            }

            if (_.isEmpty(values.cryptocurrency)) {
                errors.cryptocurrency = t('Required');
            }

            return errors;
        }
    })
)(SendMoneyDialog);

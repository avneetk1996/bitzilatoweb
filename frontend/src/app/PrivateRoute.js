// -*- rjsx -*-
import _ from 'lodash';
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const PrivateRoute = ({ component: Component, isLogged, ...rest }) => (
    <Route {...rest} render={props => (isLogged ? <Component {...props} /> : <Redirect to="/" />)} />
);

PrivateRoute.propTypes = {
    isLogged: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
    isLogged: !_.isEmpty(state.auth)
});

export default connect(mapStateToProps)(PrivateRoute);

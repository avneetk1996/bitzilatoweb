// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Helmet from 'react-helmet';
import { withNamespaces } from 'react-i18next';
import { Link } from 'react-router-dom';
import { Grid, withStyles, Tabs, Tab, Typography, Button, Paper } from '@material-ui/core';
import green from '@material-ui/core/colors/green';

import { loadAdverts, updateTradeStatus, loadTradeStatus } from '../../redux/actions/index';
import { P2P_URL } from '../../config';
import AdvertList from './AdvertList';

const styles = theme => ({
    // root: {
    //     flexGrow: 1,
    //     width: '100%',
    //     backgroundColor: theme.palette.background.paper,
    //     display: 'flex',
    //     flexDirection: 'column'
    // },
    // tabs: {
    //     //width: '100%',
    //     flex: '0 0 auto'
    // },
    // tabContent: {
    //     flexGrow: 1,
    //     padding: theme.spacing.unit * 5,
    //     [theme.breakpoints.down('sm')]: {
    //         padding: '10px 2px'
    //     }
    // }
    greenBtn: {
        backgroundColor: green[600],
        '&:hover': {
            backgroundColor: green[800]
        }
    },
    gridPaper: {
        padding: '15px',
        textAlign: 'center'
    }
});

class MyAdverts extends Component {
    static propTypes = {
        t: PropTypes.func,
        classes: PropTypes.object,

        defaultCryptocurrency: PropTypes.string,
        cryptocurrencies: PropTypes.array,

        adverts: PropTypes.array,
        tradeStatus: PropTypes.object,
        loadAdverts: PropTypes.func,
        loadTradeStatus: PropTypes.func,
        updateTradeStatus: PropTypes.func
    };

    constructor(props) {
        super();
        this.state = {
            cryptocurrency: props.defaultCryptocurrency
        };
    }

    componentDidMount() {
        this.props.loadAdverts();
        this.props.loadTradeStatus();
    }

    render() {
        const { t, classes, adverts, cryptocurrencies, tradeStatus } = this.props;
        const { cryptocurrency } = this.state;

        const displayedAdverts = _.filter(adverts, advert => advert.cryptocurrency === cryptocurrency);

        return (
            <Grid container spacing={24}>
                <Helmet>
                    <title>{t('advertListTitle')}</title>
                </Helmet>

                <Grid item xs={12}>
                    <Typography variant="h6">{t('advertListTitle')}</Typography>
                </Grid>

                <Grid item xs={12}>
                    <Tabs
                        value={cryptocurrency}
                        onChange={(event, value) => this.setState({ cryptocurrency: value })}
                        variant="scrollable"
                    >
                        {_.map(cryptocurrencies, item => (
                            <Tab label={item} value={item} key={item} />
                        ))}
                    </Tabs>
                </Grid>

                <Grid item xs={12}>
                    <Grid container spacing={8}>
                        {!_.isEmpty(displayedAdverts) && (
                            <Grid item xs={12} sm={6}>
                                {tradeStatus[cryptocurrency] && (
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        fullWidth
                                        onClick={() => this.props.updateTradeStatus(cryptocurrency, false)}
                                    >
                                        {t('Pause all adverts')}
                                    </Button>
                                )}

                                {!tradeStatus[cryptocurrency] && (
                                    <Button
                                        variant="contained"
                                        className={classes.greenBtn}
                                        fullWidth
                                        onClick={() => this.props.updateTradeStatus(cryptocurrency, true)}
                                    >
                                        {t('Start all adverts')}
                                    </Button>
                                )}
                            </Grid>
                        )}

                        <Grid item xs={12} sm={_.isEmpty(displayedAdverts) ? 12 : 6}>
                            <Link to={`${P2P_URL}/adverts/create`}>
                                <Button color="primary" fullWidth variant="contained">
                                    {t('Create advert')}
                                </Button>
                            </Link>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item xs={12}>
                    <Grid container spacing={16}>
                        <Grid item xs={12} md={6}>
                            <Paper className={classes.gridPaper}>
                                <Grid container>
                                    <Grid item xs={12}>
                                        <Typography variant="overline">{t('purchase')}</Typography>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <AdvertList
                                            adverts={_.filter(displayedAdverts, ads => ads.type === 'purchase')}
                                        />
                                    </Grid>
                                </Grid>
                            </Paper>
                        </Grid>

                        <Grid item xs={12} md={6}>
                            <Paper className={classes.gridPaper}>
                                <Grid container>
                                    <Grid item xs={12}>
                                        <Typography variant="overline">{t('selling')}</Typography>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <AdvertList
                                            adverts={_.filter(displayedAdverts, ads => ads.type === 'selling')}
                                        />
                                    </Grid>
                                </Grid>
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = state => {
    return {
        defaultCryptocurrency: state.profile.settings.cryptocurrency,
        cryptocurrencies: _.map(state.refs.cryptocurrencies, item => item.code),
        adverts: state.adverts.all,
        tradeStatus: state.adverts.tradeStatus
    };
};

const mapDispatchToProps = dispatch => {
    return {
        loadAdverts: () => dispatch(loadAdverts()),
        updateTradeStatus: (cryptocurrency, flag) => dispatch(updateTradeStatus(cryptocurrency, flag)),
        loadTradeStatus: () => dispatch(loadTradeStatus())
    };
};

export default compose(
    withNamespaces('adverts'),
    withStyles(styles),
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(MyAdverts);

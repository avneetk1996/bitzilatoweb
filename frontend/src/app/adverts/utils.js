export function validatePartAdvert(values) {
    const errors = {};

    if (values.ratePercent) {
        if (!Number(values.ratePercent) && values.ratePercent !== '0') {
            errors.ratePercent = 'Поле заполнено неверно';
        }
    }

    if (values.rateValue) {
        if (!Number(values.rateValue) && values.rateValue !== '0') {
            errors.rateValue = 'Поле заполнено неверно';
        }
        if (values.rateValue < 0) {
            errors.rateValue = 'Поле заполнено неверно';
        }
    }

    const rateMessage = 'Заполните курс или % от биржевого курса';
    if (!values.ratePercent && !values.rateValue) {
        errors.rateValue = rateMessage;
        errors.ratePercent = rateMessage;
    }

    if (values.rateValue && values.ratePercent) {
        errors.rateValue = rateMessage;
        errors.ratePercent = rateMessage;
    }

    if (!values.limit) {
        errors.limit = 'Please, set limit';
    }

    if (!values.minAmount) {
        errors.minAmount = 'Установите минимальную сумму';
    }

    if (values.minAmount) {
        if (!Number(values.minAmount) && values.minAmount !== '0') {
            errors.minAmount = 'Поле заполнено неверно';
        }
        if (values.minAmount < 0.01) {
            errors.minAmount = 'Поле заполнено неверно';
        }
    }

    if (!values.maxAmount) {
        errors.maxAmount = 'Установите максимальную сумму';
    }

    if (values.maxAmount) {
        if (!Number(values.maxAmount) && values.maxAmount !== '0') {
            errors.maxAmount = 'Поле заполнено неверно';
        }
        if (values.maxAmount < 0) {
            errors.maxAmount = 'Поле заполнено неверно';
        }
    }

    return errors;
}

export function validateFullAdvert(values) {
    const errors = validatePartAdvert(values);

    if (!values.tradeType) {
        errors.tradeType = 'Please, select trade type';
    }

    if (!values.cryptocurrency) {
        errors.cryptocurrency = 'Выберите криптовалюту ';
    }

    if (!values.currency) {
        errors.currency = 'Выберите валюту';
    }

    if (!values.rate) {
        errors.rate = 'Установите курс';
    }

    if (!values.paymethod) {
        errors.paymethod = 'Выберите способ';
    }

    return errors;
}

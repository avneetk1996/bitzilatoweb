// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';
import Helmet from 'react-helmet';
import { Button, Typography, InputAdornment, TextField, Grid, Paper, withStyles } from '@material-ui/core';
import { Field, Form, reduxForm, formValueSelector } from 'redux-form';

import { renderSelectField, renderTextField } from '../common/redux-form-helpers';
import { createAdvert, loadWalletList } from '../../redux';
import { BackendClient } from '../../redux/actions/api';
import WalletsCommon from '../Wallets/WalletsCommon';
import { validateFullAdvert } from './utils';

const styles = () => ({
    condition: { padding: '15px', backgroundColor: '#FFF8E1' }
});

class CreateAdvert extends Component {
    constructor(props) {
        super();

        this.state = {
            rate: null,
            paymethods: null,
            loading: false
        };

        this.backend = new BackendClient(props.auth.userId, props.auth.idToken);
    }

    static propTypes = {
        t: PropTypes.func.isRequired,
        classes: PropTypes.object.isRequired,
        handleSubmit: PropTypes.func.isRequired,

        auth: PropTypes.object,
        cryptocurrencies: PropTypes.array,
        currencies: PropTypes.array,
        formValues: PropTypes.object,

        createAdvert: PropTypes.func.isRequired,
        loadWalletList: PropTypes.func.isRequired
    };

    componentDidMount() {
        this.props.loadWalletList();
    }

    componentDidUpdate(prevProps) {
        if (!_.isEqual(prevProps.formValues, this.props.formValues)) {
            this.recalcRate();
        }
    }

    onSubmit = values => {
        this.props.createAdvert(values);
    };

    onChange = async (field, event) => {
        const { formValues, change } = this.props;
        let { type, currency, cryptocurrency } = formValues;

        switch (field) {
            case 'type':
                type = event.target.value;
                break;
            case 'currency':
                currency = event.target.value;
                break;
            case 'cryptocurrency':
                cryptocurrency = event.target.value;
                break;
        }

        change('paymethod', null);

        if (type && currency && cryptocurrency) {
            this.setState({ loading: true });
            const paymethods = await this.backend.dsaPaymethods(type, currency, cryptocurrency);
            this.setState({ loading: false, paymethods });
        } else {
            this.setState({ loading: false, paymethods: null });
        }
    };

    recalcRate = _.debounce(async () => {
        const { currency, cryptocurrency, rateValue = null, ratePercent = null } = this.props.formValues;

        if (
            currency &&
            cryptocurrency &&
            (!_.isNull(rateValue) || !_.isNull(ratePercent)) &&
            (_.isNull(rateValue) || _.isNull(ratePercent))
        ) {
            const rate = await this.backend.calcRate(cryptocurrency, currency, {
                value: rateValue,
                percent: ratePercent
            });
            this.setState({ rate });
        } else {
            this.setState({ rate: null });
        }
    }, 500);

    render() {
        const { handleSubmit, cryptocurrencies, currencies, formValues, t, classes } = this.props;
        const { rate, paymethods, loading } = this.state;

        const sellActions = [{ value: 'selling', label: 'Продать' }, { value: 'purchase', label: 'Купить' }];

        let paymethodText;
        if (!formValues.type) paymethodText = 'тип';
        else if (!formValues.currency) paymethodText = 'валюту';
        else paymethodText = 'криптовалюту';

        paymethodText = `Способ оплаты. Выберите ${paymethodText}`;

        // type - покупка (purchase) или продажа (selling)
        // cryptocurrency - криптовалюты
        // paymethod - платёжная система (для её выбора сначала потребуется выбрать валюту и криптовалюту)
        // rate - курс объявления. Может быть либо прямая сумма (rate.value), либо процент (rate.percen) от биржевого курса.
        //     limit.min и limit.max - лимиты объявления
        // Кроме того, могут быть указаны следующие опциональные параметры:
        //
        //     terms - Условия торговли, показывается в описании объявления
        // details - реквизиты для перевода, только для объявления типа selling

        return (
            <Grid container spacing={40}>
                <Helmet>
                    <title>{t('createAdvert')}</title>
                </Helmet>

                <WalletsCommon />

                <Grid item xs={12}>
                    <Typography variant="h6">{t('createAdvert')}</Typography>
                </Grid>

                <Grid item xs={12}>
                    <Form onSubmit={handleSubmit(this.onSubmit)}>
                        <Grid container spacing={24}>
                            <Grid item xs={12}>
                                <Typography variant="subtitle1">
                                    Тип объявления
                                    {/*<Tooltip title="Если вы хотите продать криптовалюту, убедитесь, что в вашем кошельке Bitzlato есть криптовалюта" placement="top">
                                        </Tooltip>*/}
                                </Typography>

                                <Field
                                    select
                                    name="type"
                                    label="Я хочу..."
                                    component={renderSelectField}
                                    options={sellActions}
                                    onChange={e => this.onChange('type', e)}
                                    fullWidth
                                />
                                <p>
                                    Если вы хотите продать криптовалюту, убедитесь, что в вашем кошельке Bitzlato есть
                                    криптовалюта
                                </p>

                                <Grid container spacing={24}>
                                    <Grid item xs={12} sm={4}>
                                        <Field
                                            select
                                            name="cryptocurrency"
                                            label="Криптовалюта"
                                            component={renderSelectField}
                                            options={cryptocurrencies.map(crypto => ({
                                                value: crypto.code,
                                                label: crypto.name
                                            }))}
                                            onChange={e => this.onChange('cryptocurrency', e)}
                                            fullWidth
                                        />
                                    </Grid>

                                    <Grid item xs={12} sm={4}>
                                        <Field
                                            select
                                            name="currency"
                                            label="Валюта"
                                            component={renderSelectField}
                                            options={currencies.map(currency => ({
                                                value: currency.code,
                                                label: `${currency.code} (${currency.name})`
                                            }))}
                                            onChange={e => this.onChange('currency', e)}
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={4}>
                                        {paymethods ? (
                                            <div>
                                                <Field
                                                    select
                                                    name="paymethod"
                                                    label="Способ оплаты"
                                                    component={renderSelectField}
                                                    options={paymethods.map(paymethod => ({
                                                        value: paymethod.id,
                                                        label: `${paymethod.description}`
                                                    }))}
                                                    disabled={loading}
                                                    fullWidth
                                                />
                                                <p>
                                                    Если нет необходимого вам способа оплаты, то обратитесь в службу{' '}
                                                    <a href="https://changebot.freshdesk.com/support/home">поддержки</a>{' '}
                                                    с заявкой на подключение нового способа.
                                                </p>
                                            </div>
                                        ) : (
                                            <TextField disabled fullWidth label={paymethodText} />
                                        )}
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>

                        <Grid container spacing={24}>
                            <Grid item xs={12} sm={3}>
                                <Field
                                    name="ratePercent"
                                    component={renderTextField}
                                    label={
                                        rate && rate.value === formValues.rateValue ? rate.percent : t('ratePercent')
                                    }
                                    fullWidth
                                    InputProps={{ endAdornment: <InputAdornment position="end">%</InputAdornment> }}
                                />
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <Field
                                    name="rateValue"
                                    component={renderTextField}
                                    label={
                                        rate && rate.percent === formValues.ratePercent ? rate.value : t('rateValue')
                                    }
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <Field
                                    name="minAmount"
                                    component={renderTextField}
                                    label="Минимальная сумма в валюте"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <Field
                                    name="maxAmount"
                                    component={renderTextField}
                                    label="Максимальная сумма в валюте"
                                    fullWidth
                                />
                            </Grid>
                        </Grid>

                        <Grid container spacing={24}>
                            <Grid item xs={12} sm={6}>
                                <Field
                                    name="terms"
                                    component={renderTextField}
                                    label="Условия торговли"
                                    placeholder="Наример: Приму перевод на ВТБ. Пожалуйста, не шлите с киви, сбербанка и альфы. Отпускаю биткоины только по получении 100% суммы."
                                    fullWidth
                                    multiline
                                    rows={5}
                                    type="text"
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <Field
                                    name="details"
                                    component={renderTextField}
                                    label="Реквизиты для перевода денег. Только для продажи криптовалюты"
                                    fullWidth
                                    multiline
                                    rows={5}
                                    type="text"
                                />
                            </Grid>
                        </Grid>

                        <Grid container spacing={24}>
                            <Grid item xs={12}>
                                <Button variant="contained" color="primary" type="submit" fullWidth disabled={loading}>
                                    Создать
                                </Button>
                            </Grid>
                        </Grid>
                    </Form>
                </Grid>

                <Grid item xs={12}>
                    <Paper className={classes.condition}>
                        <Grid container spacing={24}>
                            <Grid item xs={12}>
                                <Typography variant="h5">Правила и требования к объявлению</Typography>

                                <Typography component="div">
                                    <ul>
                                        <li>
                                            Для отображения объявлений вам необходимо иметь криптовалюту в кошельке
                                            Bitzlato
                                        </li>
                                        <li>
                                            Сбор с разместивших объявление с ETH или DOGE пользователей за каждую
                                            совершенную сделку составляет 0.8% от общей суммы сделки.
                                        </li>
                                        <li>
                                            Сбор с разместивших объявление с BTC, LTC, BCC или DASH пользователей за
                                            каждую совершенную сделку составляет 2% от общей суммы сделки.
                                        </li>
                                        <li>
                                            Информация обо всех сборах приводится на соответствующей{' '}
                                            <a href="https:bitzlato.com/ru/fees">странице</a> нашего сайта.
                                        </li>
                                        <li>После открытия сделки изменить цену нельзя.</li>
                                        <li>
                                            Вам не разрешается покупать или продавать биткоины от имени других лиц
                                            (брокерство).
                                        </li>
                                        <li>
                                            Вы можете пользоваться лишь счетами для оплаты, зарегистрированными на ваше
                                            собственное имя (платежи от третьих сторон запрещены!).
                                        </li>
                                        <li>
                                            Вы должны указывать свои платежные реквизиты в объявлении или чате по
                                            сделке.
                                        </li>
                                        <li>Весь обмен информацией должен происходить на сайте p2p.bitzlato.com.</li>
                                        <li>Будьте внимательны при проведении сделок.</li>
                                    </ul>
                                </Typography>
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>

                <Grid item xs={12}>
                    <Paper className={classes.condition}>
                        <Grid container spacing={24}>
                            <Grid item xs={12}>
                                <Typography variant="h5">Правила отображения лимитов объявления</Typography>

                                <Typography component="div">
                                    <ul>
                                        <li>
                                            У Продавца криптовалюты <b>верхний</b> лимит не может быть больше количества
                                            криптовалюты в его кошельке.
                                        </li>
                                        <li>
                                            В случае, если баланс кошелька Продавца криптовалюты становится меньше{' '}
                                            <b>верхнего</b> лимита, то в поиске будет указана максимальная возможная
                                            сумма на продажу.
                                        </li>
                                        <li>
                                            Если баланс кошелька продавца меньше <b>нижнего</b> лимита, то объявление
                                            будет приостановлено.
                                        </li>
                                    </ul>
                                </Typography>
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
            </Grid>
        );
    }
}

const createAdvertFormSelector = formValueSelector('createAdvertForm');

const mapStateToProps = state => ({
    auth: state.auth,
    cryptocurrencies: state.refs.cryptocurrencies,
    currencies: state.refs.currencies,
    formValues: createAdvertFormSelector(state, 'type', 'currency', 'cryptocurrency', 'ratePercent', 'rateValue')
});

export default compose(
    withNamespaces('adverts'),
    withStyles(styles),
    connect(
        mapStateToProps,
        { createAdvert, loadWalletList }
    ),
    reduxForm({ form: 'createAdvertForm', validate: validateFullAdvert })
)(CreateAdvert);

// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { push } from 'connected-react-router';
import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';

import { withStyles, Table, TableBody, TableCell, TableRow, TableHead, Grid } from '@material-ui/core';
import green from '@material-ui/core/colors/green';

import { P2P_URL } from '../../config';

const styles = () => ({
    root: {
        //width: '100%',
        //marginTop: theme.spacing.unit * 3,
        padding: '20px'
    },
    table: {
        minWidth: 500
    },
    tableWrapper: {
        overflowX: 'auto'
    },
    createAdvertButtonWrap: {
        height: 36,
        textDecoration: 'none'
    },
    noneDecoration: {
        textDecoration: 'none'
    },
    createAdvertButton: {},
    headlineWrap: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    row: {
        '&:hover': {
            backgroundColor: '#efefef',
            cursor: 'pointer'
        }
    },
    greenBtn: {
        backgroundColor: green[600],
        '&:hover': {
            backgroundColor: green[800]
        }
    }
});

class AdvertList extends Component {
    static propTypes = {
        t: PropTypes.func,
        classes: PropTypes.object,

        adverts: PropTypes.array,
        openAdvert: PropTypes.func
    };

    render() {
        const { t, adverts, classes } = this.props;

        return (
            <Grid container spacing={24}>
                <Grid item xs={12}>
                    <div className={classes.tableWrapper}>
                        <Table className={classes.table}>
                            <TableHead>
                                <TableRow>
                                    <TableCell>{t('Status')}</TableCell>
                                    <TableCell>{t('Currency')}</TableCell>
                                    <TableCell>{t('Rate')}</TableCell>
                                </TableRow>
                            </TableHead>

                            <TableBody>
                                {Boolean(adverts) &&
                                    _.flatten(_.values(_.groupBy(adverts, ad => ad.paymethod.currency))).map(advert => {
                                        return (
                                            <TableRow
                                                key={advert.id}
                                                className={classes.row}
                                                onClick={() => this.props.openAdvert(advert.id)}
                                            >
                                                <TableCell>
                                                    {advert.status === 'active' ? t('active') : t('paused')}
                                                </TableCell>

                                                <TableCell>
                                                    {advert.paymethod.name} ({advert.paymethod.currency})
                                                </TableCell>

                                                <TableCell>
                                                    {advert.rate.value} {advert.paymethod.currency} /{' '}
                                                    {advert.cryptocurrency}
                                                </TableCell>
                                            </TableRow>
                                        );
                                    })}
                            </TableBody>
                        </Table>
                    </div>
                </Grid>
            </Grid>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        openAdvert: id => dispatch(push(`${P2P_URL}/adverts/${id}`))
    };
};

export default withNamespaces('adverts')(
    withStyles(styles)(
        connect(
            null,
            mapDispatchToProps
        )(AdvertList)
    )
);

// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';
import Helmet from 'react-helmet';
import {
    withStyles,
    Paper,
    Grid,
    Button,
    Typography,
    InputAdornment,
    Table,
    TableBody,
    TableCell,
    TableRow
} from '@material-ui/core';
import green from '@material-ui/core/colors/green';
import { Field, reduxForm, Form, reset, formValueSelector } from 'redux-form';
import Emoji from 'react-emoji-render';

import { P2P_URL } from '../../config';
import { renderTextField } from '../common/redux-form-helpers';
import { loadOneAdvert, updateAdvert, removeAdvert } from '../../redux/actions';
import { BackendClient } from '../../redux/actions/api';
import { validatePartAdvert } from './utils';

const styles = theme => ({
    paper: {
        padding: 24
    },
    terms: {
        whiteSpace: 'pre-line',
        padding: '24px',
        [theme.breakpoints.down('sm')]: {
            padding: '4px 4px'
        }
    },
    active: {
        color: green[500]
    },
    error: {
        color: theme.palette.secondary.main
    }
});

class Advert extends Component {
    constructor(props) {
        super();

        this.state = {
            id: props.match.params.advertId,
            editingState: false,
            deleteAdvertConfirmation: false,
            rate: null
        };
        this.backend = new BackendClient(props.auth.userId, props.auth.idToken);
        this._isMounted = false;
    }

    static propTypes = {
        t: PropTypes.func,
        initialize: PropTypes.func,
        handleSubmit: PropTypes.func,
        match: PropTypes.object,
        classes: PropTypes.object,

        auth: PropTypes.object,
        advert: PropTypes.object,
        initialValues: PropTypes.object,
        formValues: PropTypes.object,

        loadOneAdvert: PropTypes.func,
        reset: PropTypes.func,
        updateAdvert: PropTypes.func,
        removeAdvert: PropTypes.func
    };

    componentDidMount() {
        this._isMounted = true;
        this.props.loadOneAdvert();
    }

    componentDidUpdate(prevProps) {
        const pick = obj => _.pick(obj, ['ratePercent', 'rateValue']);
        if (!_.isEqual(pick(prevProps.formValues), pick(this.props.formValues))) {
            this.recalcRate();
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    startEditing = () => {
        this.setState({
            editingState: true
        });

        const advert = this.props.advert;

        this.props.initialize({
            rateValue: advert.rate.percent ? null : advert.rate.value,
            ratePercent: advert.rate.percent,
            minAmount: advert.limit.min,
            maxAmount: advert.limit.max,
            terms: advert.terms,
            details: advert.details
        });
    };

    cancelEditing = () => {
        this.setState({
            editingState: false
        });
    };

    saveAdvertChanges = async values => {
        const finalUpdate = {
            rate: {
                value: values.rateValue,
                percent: values.ratePercent
            },
            details: values.details,
            terms: values.terms,
            limit: {
                min: values.minAmount,
                max: values.maxAmount
            },
            status: values.status
        };

        await this.props.updateAdvert(finalUpdate);

        this.cancelEditing();
    };

    turnAdvertStatus = async flag => {
        await this.props.updateAdvert({
            status: flag ? 'active' : 'pause'
        });
    };

    deletingAdvertStart = () => {
        this.setState({
            deleteAdvertConfirmation: true
        });
    };

    deletingAdvertEnd = () => {
        this.setState({
            deleteAdvertConfirmation: false
        });
    };

    deleteAdvert = () => {
        this.props.removeAdvert();
    };

    recalcRate = _.debounce(async () => {
        const advert = this.props.advert;
        const rateValue = _.isEmpty(this.props.formValues.rateValue) ? null : this.props.formValues.rateValue;
        const ratePercent = _.isEmpty(this.props.formValues.ratePercent) ? null : this.props.formValues.ratePercent;

        if ((!_.isNull(rateValue) || !_.isNull(ratePercent)) && (_.isNull(rateValue) || _.isNull(ratePercent))) {
            const rate = await this.backend.calcRate(advert.cryptocurrency, advert.paymethod.currency, {
                value: rateValue,
                percent: ratePercent
            });

            if (this._isMounted) {
                this.setState({ rate });
            }
        } else {
            this.setState({
                rate: null
            });
        }
    }, 500);

    render() {
        const { t, advert } = this.props;
        const { editingState } = this.state;
        let headlineText;
        const loaded = Boolean(advert) && advert.type;

        if (Boolean(advert) && advert.type && advert.type === 'purchase') {
            headlineText = t('advertHeaderBuy', {
                cryptocurrency: advert.cryptocurrency,
                paymethod: advert.paymethod.name,
                currency: advert.paymethod.currency
            });
        } else if (Boolean(advert) && advert.type && advert.type !== 'purchase') {
            headlineText = t('advertHeaderSell', {
                cryptocurrency: advert.cryptocurrency,
                paymethod: advert.paymethod.name,
                currency: advert.paymethod.currency
            });
        }

        if (!loaded) return null;

        return (
            <Grid container direction="column" spacing={24}>
                <Helmet>
                    <title>{headlineText}</title>
                </Helmet>

                <Grid item xs={12}>
                    <Grid container>
                        <Grid item xs />
                        <Grid item>
                            <Button component="div">
                                <Link to={`${P2P_URL}/adverts`}>{t('Back to adverts list')}</Link>
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item>
                    <Typography variant="h6">{headlineText}</Typography>
                </Grid>
                {this.commandButtons()}
                {editingState ? this.editForm() : this.viewAdvert()}
                {this.deleteButton()}
            </Grid>
        );
    }

    commandButtons() {
        const { t, classes, advert, handleSubmit } = this.props;
        const { editingState } = this.state;

        if (editingState) {
            return (
                <Grid item>
                    <Grid container justify="flex-end" spacing={8}>
                        <Grid item>
                            <Button variant="contained" color="primary" onClick={handleSubmit(this.saveAdvertChanges)}>
                                {t('Save')}
                            </Button>
                        </Grid>

                        <Grid item>
                            <Button variant="contained" onClick={this.cancelEditing} color="secondary">
                                {t('Cancel')}
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            );
        } else {
            return (
                <Grid item>
                    <Grid container justify="flex-end" spacing={8}>
                        <Grid item>
                            <Button
                                variant="contained"
                                className={classes.reportButton}
                                color={advert.status === 'active' ? 'secondary' : 'primary'}
                                onClick={() => this.turnAdvertStatus(advert.status !== 'active')}
                            >
                                {t(advert.status === 'active' ? 'Turn Off' : 'Turn On')}
                            </Button>
                        </Grid>

                        <Grid item>
                            <Button variant="contained" onClick={this.startEditing} className={classes.reportButton}>
                                {t('Edit')}
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            );
        }
    }

    deleteButton() {
        const { t } = this.props;
        const { editingState, deleteAdvertConfirmation } = this.state;

        if (!editingState) return null;

        if (deleteAdvertConfirmation) {
            return (
                <Grid item>
                    <Grid container spacing={8}>
                        <Grid item>
                            <Button variant="contained" onClick={this.deletingAdvertEnd}>
                                {t('Cancel')}
                            </Button>
                        </Grid>

                        <Grid item>
                            <Button variant="contained" onClick={this.deleteAdvert} color="secondary">
                                {t('Delete anyway')}
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            );
        } else {
            return (
                <Grid item>
                    <Button variant="contained" onClick={this.deletingAdvertStart} color="secondary">
                        {t('Delete')}
                    </Button>
                </Grid>
            );
        }
    }

    viewAdvert() {
        const { t, classes, advert } = this.props;

        return (
            <Grid item>
                <Grid container spacing={24} direction="row">
                    <Grid item xs={12} sm={6}>
                        <Table>
                            <TableBody>
                                <TableRow>
                                    <TableCell>
                                        <strong>{t('Rate')}</strong>
                                    </TableCell>
                                    <TableCell>{advert.rate.value}</TableCell>
                                </TableRow>

                                <TableRow>
                                    <TableCell>
                                        <strong>{t('Limits')}</strong>
                                    </TableCell>
                                    <TableCell>
                                        {advert.limit.min} - {advert.limit.max}
                                    </TableCell>
                                </TableRow>

                                <TableRow>
                                    <TableCell>
                                        <strong>{t('Details')}</strong>
                                    </TableCell>
                                    <TableCell>{advert.details}</TableCell>
                                </TableRow>

                                <TableRow>
                                    <TableCell>
                                        <strong>{t('Status')}</strong>
                                    </TableCell>
                                    <TableCell className={advert.status === 'active' ? classes.active : classes.error}>
                                        <strong>{t(advert.status)}</strong>
                                    </TableCell>
                                </TableRow>

                                {advert.unactiveReason && (
                                    <TableRow>
                                        <TableCell>
                                            <strong>{t('Unactive reason')}</strong>
                                        </TableCell>
                                        <TableCell className={classes.error}>{advert.unactiveReason}</TableCell>
                                    </TableRow>
                                )}

                                <TableRow>
                                    <TableCell>
                                        <strong>{t('Links')}</strong>
                                    </TableCell>
                                    <TableCell>
                                        <Grid container direction="column" spacing={8}>
                                            {_.map(advert.links, link => (
                                                <Grid item key={link.type}>
                                                    <a href={link.url}>{_.capitalize(link.type)}</a>
                                                </Grid>
                                            ))}
                                        </Grid>
                                    </TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        <Paper className={classes.terms}>
                            <Grid container direction="column" spacing={8}>
                                <Grid item>
                                    <Typography variant="subtitle1">{t('Terms of use')}</Typography>
                                </Grid>

                                <Grid item>
                                    <Emoji text={advert.terms || ''} />
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        );
    }

    editForm() {
        const { t, classes, advert, formValues, handleSubmit } = this.props;
        const rate = this.state.rate;

        return (
            <Grid item>
                <Form onSubmit={handleSubmit(this.saveAdvertChanges)}>
                    <Grid container spacing={24}>
                        <Grid item xs={12} sm={6}>
                            <Grid container spacing={24}>
                                <Grid item xs={12} md={6}>
                                    <Field
                                        name="ratePercent"
                                        label={
                                            rate && rate.value === formValues.rateValue
                                                ? rate.percent
                                                : t('ratePercent')
                                        }
                                        component={renderTextField}
                                        fullWidth
                                        InputProps={{ endAdornment: <InputAdornment position="end">%</InputAdornment> }}
                                    />
                                </Grid>

                                <Grid item xs={12} md={6}>
                                    <Field
                                        name="rateValue"
                                        label={
                                            rate && rate.percent === formValues.ratePercent
                                                ? rate.value
                                                : t('rateValue')
                                        }
                                        component={renderTextField}
                                        fullWidth
                                    />
                                </Grid>

                                <Grid item xs={12} md={6}>
                                    <Field
                                        name="minAmount"
                                        label={t('Min')}
                                        component={renderTextField}
                                        fullWidth
                                        type="number"
                                    />
                                </Grid>

                                <Grid item xs={12} md={6}>
                                    <Field
                                        name="maxAmount"
                                        label={t('Max')}
                                        component={renderTextField}
                                        fullWidth
                                        type="number"
                                    />
                                </Grid>

                                {advert.type === 'selling' && (
                                    <Grid item xs={12}>
                                        <Field
                                            name="details"
                                            label={t('Details')}
                                            component={renderTextField}
                                            helperText="Опишите реквизиты для перевода средств"
                                            fullWidth
                                            multiline
                                            rows={5}
                                            variant={'outlined'}
                                        />
                                    </Grid>
                                )}
                            </Grid>
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <Paper className={classes.terms}>
                                <Field
                                    name="terms"
                                    label={t('Terms of use')}
                                    multiline={true}
                                    rows={5}
                                    rowsMax={12}
                                    component={renderTextField}
                                    fullWidth
                                    //variant={'outlined'}
                                />
                            </Paper>
                        </Grid>
                    </Grid>
                </Form>
            </Grid>
        );
    }
}

const editAdvertFormSelector = formValueSelector('editAdvertForm');

const mapStateToProps = (state, props) => {
    const advertId = props.match.params.advertId;
    const advert = state.adverts && state.adverts.items && state.adverts.items[advertId];

    return {
        auth: state.auth,
        advert: advert,
        //initialValues: advert,
        formValues: editAdvertFormSelector(state, 'ratePercent', 'rateValue')
    };
};

const mapDispatchToProps = (dispatch, props) => {
    const advertId = props.match.params.advertId;

    return {
        loadOneAdvert: () => dispatch(loadOneAdvert(advertId)),
        reset: () => dispatch(reset('editAdvertForm')),
        updateAdvert: data => dispatch(updateAdvert(advertId, data)),
        removeAdvert: () => dispatch(removeAdvert(advertId))
    };
};

export default compose(
    withNamespaces('adverts'),
    withStyles(styles),
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    reduxForm({
        form: 'editAdvertForm',
        validate: validatePartAdvert
    })
)(Advert);

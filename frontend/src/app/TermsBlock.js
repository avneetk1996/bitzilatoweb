// -*- rjsx -*-
import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { withNamespaces } from 'react-i18next';
import { Button, Typography, withStyles } from '@material-ui/core';
//import './terms.css';

const styles = theme => ({
    terms: {
        overflow: 'auto',
        height: '400px',
        fontSize: '80%',
        [theme.breakpoints.down('sm')]: {
            height: '200px'
        }
    }
});

const LicenceBlock = props => {
    const { t, acceptLicenceAgreement, licensingAgreementAccepted, classes } = props;

    return (
        <div>
            <Helmet>
                <title>{t('Licence agreement')}</title>
            </Helmet>

            <Typography variant="h6">{t('Licence agreement')}</Typography>

            <div className={classes.terms}>
                <div id="page_1">
                    <p className="p0 ft0">Terms of use</p>
                    <p className="p1 ft1">Date of last version: Dec 29, 2017;</p>
                    <p className="p2 ft3">
                        <span className="ft2">Russian:</span> Условия предоставлены только на Английском языке.
                        Пожалуйста, воспользуйтесь услугами переводчика или специальной программы для того, чтобы
                        ознакомиться с условиями сервиса если вы не понимаете их.
                    </p>
                    <p className="p3 ft3">
                        <span className="ft2">Spanish:</span> Las condiciones se ofrecen únicamente en Inglés. Por
                        favor, utilice los servicios de un intérprete o de un programa especial para leer los términos
                        de servicio si usted no entiende.
                    </p>
                    <p className="p4 ft3">
                        <span className="ft2">Italian</span>
                        <span className="ft4">:</span>
                        <span className="ft5">​</span>
                        Condizioni concesse solo in lingua Inglese. Si prega di utilizzare i servizi di un interprete o
                        di un programma speciale per familiarizzare con i termini di servizio se non si capisce la loro.
                    </p>
                    <p className="p5 ft6">Introduction</p>
                    <p className="p6 ft7">
                        ChangeBot is an AD board for cryptocurrency traders with wallet and escrow service.
                    </p>
                    <p className="p7 ft8">
                        It is important to us that you, and our other visitors, have the best possible time while using
                        changeBot, and that when you use changeBot you are fully aware of your respective legal rights
                        and obligations. For that reason, we have created these Terms of Service as the legally binding
                        terms to govern your use of any of changeBot services.
                    </p>
                    <p className="p8 ft9">
                        Please read these Terms of Service carefully before using changeBot services because they affect
                        your legal rights and obligations!
                    </p>
                    <p className="p3 ft8">
                        These Terms of Service (&quot;agreement&quot;) is a legal agreement covering the provision of
                        changeBot services (&quot;service&quot; or &quot;services&quot;) by BITZLATO LIMITED
                        (&quot;BITZLATO&quot; or &quot;us&quot; or &quot;our&quot; or &quot;we&quot;) to you as an
                        individual (&quot;you&quot; or &quot;your&quot;). Your use of the services will be governed by
                        this agreement, along with the changeBot privacy policy. Please read this agreement carefully
                        before using changeBot. By using the service, you agree to become bound by the terms and
                        conditions of this agreement.
                    </p>
                    <p className="p9 ft6">Your acceptance of terms</p>
                    <p className="p8 ft12">
                        If you do not agree with any of these Terms of Use please do not use changeBot, because by using
                        our services you will be deemed to have irrevocably agreed to these terms. Please note that
                        these <span className="ft10">Terms</span>
                        <span className="ft11">​ </span>
                        <span className="ft10">of Use may be revised and reissued without notice at any time</span>.
                        <span className="ft11">​</span>
                        You should visit this page regularly to review the current Terms of Use, since your continued
                        use of the bot will be deemed as irrevocable acceptance of any revisions.
                    </p>
                    <p className="p10 ft6">Allowed jurisdiction</p>
                    <p className="p11 ft8">
                        <span className="ft3">
                            Services provided by BITZLATO LIMITED might not be legally allowed in certain jurisdictions.
                            By accepting these terms and services you confirm that you are not resident of a such
                            jurisdiction
                        </span>
                        <span className="ft13">​</span>, including, but not limited to the Federal Republic of Germany
                        and State of New York in the United States of America.
                    </p>
                </div>
                <div id="page_2">
                    <div id="id_1">
                        <p className="p0 ft6">Jurisdictional Issues</p>
                        <p className="p12 ft3">
                            Users of changeBot are themselves responsible for making sure they are in compliance with
                            legislation of the jurisdiction they operate in. The Materials and all other content in
                            changeBot are presented solely for the purpose of providing entertainment and information
                            and promoting programs, films, music, and other products available. ChangeBot is controlled
                            and operated by BITZLATO LIMITED, HK.
                        </p>
                        <p className="p9 ft6">Limitation of Liability</p>
                        <p className="p8 ft12">
                            BITZLATO LIMITED does not accept any liability for any loss or damage, direct or indirect,
                            resulting from any use of, or inability to use, the material, changeBot services,
                            information, software, facilities, services or other content, regardless of the basis upon
                            which liability is claimed and even if we has been advised of the possibility of such loss
                            or damage. Without limitation, you (and not BITZLATO LIMITED) assume the entire cost of all
                            necessary servicing, repair or correction or correction in the event of any such loss or
                            damage arising.
                        </p>
                        <p className="p10 ft6">System Abuse</p>
                        <p className="p8 ft12">
                            Without limitation, you agree not to send, create or reply to so called mailbombs (i.e.,
                            emailing or messaging copies of a single message to many users, or sending large or multiple
                            files or messages to a single user with malicious intent) or engage in spamming (i.e.,
                            unsolicited emailing or messaging for business or other purposes) or undertake any other
                            activity which may adversely affect the operation or enjoyment of this service by any other
                            person. You may not reproduce, sell, resell or otherwise exploit any resource, or access to
                            any resource, contained on this service.
                        </p>
                        <p className="p10 ft6">Trademarks and Copyrights</p>
                        <p className="p8 ft12">
                            ChangeBot and all materials incorporated by BITZLATO are protected by copyrights, or other
                            proprietary copyrights. Some of the logos or other images incorporated on this bot are also
                            protected.
                        </p>
                        <p className="p10 ft6">Registration details</p>
                        <p className="p8 ft3">
                            To access changeBot services, you may be asked to provide registration details. It is a
                            condition of use of this bot that all the details you provide will be correct, current, and
                            complete. If we at BITZLATO believe the details are not correct, current, or complete, we
                            have the right to refuse you access to the our services, and to terminate or suspend your
                            account.
                        </p>
                        <p className="p9 ft6">Competitions</p>
                    </div>
                    <div id="id_2">
                        <p className="p13 ft3">
                            BITZLATO may occasionally run competitions with other resources and we will ask visitors for
                            their contact information. Any information that you supply will be held in the strictest
                            confidence and will only be used in conjunction with the competition.
                        </p>
                    </div>
                </div>
                <div id="page_3">
                    <p className="p0 ft6">Your Use of Material</p>
                    <p className="p12 ft12">
                        Your right to make use of changeBot and any material or other content appearing on it is subject
                        to your compliance with these Terms of Use. Modification or use of the material or any other
                        content on changeBot for any purpose not permitted by these Terms of Use may be a violation of
                        the Copyright and is prohibited. You may access and display material and all other content
                        displayed on changeBot for <nobr>non-commercial,</nobr> personal, entertainment. The material
                        and all other content on changeBot may not otherwise be copied, reproduced, republished,
                        uploaded, posted, transmitted, distributed or used in any way unless specifically authorised by
                        BITZLATO.
                    </p>
                    <p className="p14 ft6">Wallet</p>
                    <p className="p15 ft1">
                        By accepting these terms you confirm that you will use your cryptocurrency only for legal
                        operations
                    </p>
                    <p className="p16 ft1">
                        in your jurisdiction. BITZLATO
                        <span className="ft14">​ </span>
                        does not accept any liability for your activity.
                    </p>
                    <p className="p2 ft15">
                        Any <nobr>“fork-coins”</nobr> or “tokens“ formed or deposited by any causes on not suitable
                        wallet (Like ETC deposited on ETH wallet) reserved by BITZLATO and will be used a on our
                        discretion.
                    </p>
                    <p className="p10 ft6">Escrow fees</p>
                    <p className="p8 ft12">
                        Service charge 2% fee dependent of exchange volume. Service charge fee from Seller and Buyer
                        after completed transaction. Users who have 200 or more rating points can
                        <span className="ft16">​ </span>
                        <span className="ft10">stop service fees</span>.<span className="ft11">​</span>
                    </p>
                    <p className="p10 ft6">Wallet fees</p>
                    <p className="p8 ft12">
                        <span className="ft10">T</span>
                        ransactions
                        <span className="ft11">​ </span>
                        send to the network with a timeout. Real network fee and time to first confirmation depends of
                        current network status and settings of cryptocurrency wallet that we set by our discretion.
                    </p>
                    <p className="p17 ft1">Withdrawal plans:</p>
                    <p className="p16 ft1">
                        <span className="ft17">Optimal</span>
                        <span className="ft14">​</span>- tx send in 1h; (0.0015 BTC, 0.001 BCC)
                    </p>
                    <p className="p16 ft1">
                        <span className="ft17">Priority</span>
                        <span className="ft14">​</span>- tx send in several minutes; (0.004 BTC, 0.02 LTC, 2 DOGE,
                        0.00004 DASH, 0.0019 BCC)
                    </p>
                    <p className="p18 ft6">Referral payments</p>
                    <p className="p19 ft18">
                        If you invite new user via your personal link and he is locked as your “referral” you will get a
                        dividends from his exchange activity. We pay dividends after service fee will be charged
                    </p>
                    <p className="p0 ft21">
                        <span className="ft19">from his </span>
                        <span className="ft20">positive</span>
                        <span className="ft14">​ </span>
                        <span className="ft20">balance</span>
                        <span className="ft19">.</span>
                        <span className="ft14">​</span>
                        We
                        <span className="ft14">​ </span>
                        didn’t pay dividends for wallet fees!
                    </p>
                    <p className="p20 ft19">
                        <span className="ft20">Current percentage: </span>
                        1%
                        <span className="ft14">​ </span>
                        from exchange volume;
                    </p>
                </div>
                <div id="page_4">
                    <p className="p0 ft6">Services</p>
                    <p className="p12 ft3">
                        BITZLATO reserves the right to modify or discontinue, temporarily or permanently, all or any
                        part of any changeBot software, facilities and services, with or without notice, and/or to
                        establish general guidelines and limitations on their use.
                    </p>
                    <p className="p20 ft6">Messages</p>
                    <p className="p8 ft12">
                        The materials, informations and opinions included and/or expressed in or on the messages,
                        feedbacks, advertisements or other pages on this service are not necessarily those of BITZLATO
                        or it&#39;s affiliated or related entities or content providers. BITZLATO is not responsible to
                        monitor or review the messages, feedbacks or advertisements and the content of the messages,
                        feedbacks or advertisements is not the responsibility of changeBot. We may remove or modify any
                        content without notice of liability at any time in sole discretion. Any use of the messages,
                        feedbacks, advertisements or any other content on changeBot services will be at your own risk
                        and will be subject to the disclaimers and limitations on liability set out above.
                    </p>
                    <p className="p10 ft6">Advertisements and trading</p>
                    <p className="p8 ft12">
                        With a registered account you can create cryptocurrency trade advertisements
                        (&quot;advertisement&quot; or &quot;ad&quot; or &quot;ads&quot; or “advert” or “adverts”) to
                        advertise that you are purchasing or selling cryptocurrency. You may initiate trades
                        (&quot;trades&quot; or &quot;contacts&quot; or “transactions” or “deals”) by contacting
                        advertisements created by other users or wait for users to initiate trades with you by
                        contacting ads created by yourself.
                    </p>
                    <p className="p21 ft12">
                        <span className="ft10">
                            By initiating a trade you agree to be bound by the advertisers terms of trade
                        </span>{' '}
                        set forth by the advertiser and displayed as terms of trade on the advertisement page. The terms
                        of trade as determined by the advertiser are valid in all cases except when the advertisers
                        terms contradict the changeBot Terms of Service or are illegal or if both parties of the trade
                        consent to alter the terms of trade. The price or price equation defined in the advertisement
                        must contain any and all fees the advertiser requires the user initiating the trade to pay.
                        After initiation of a trade the cryptocurrency price is locked and cannot be changed or any
                        additional fees added without consent from both parties of the trade and changeBot.
                    </p>
                    <p className="p22 ft12">
                        <span className="ft10">Advertisements must be placed in the correct payment category</span>,
                        <span className="ft11">​</span>
                        if a category does not exist for the payment method it must be placed in the Other category.
                    </p>
                    <p className="p22 ft3">
                        <span className="ft4">We may hide, remove, or limit your advertisements</span>,
                        <span className="ft13">​</span>
                        if you violate these Terms of Service or any other agreement you enter into with changeBot, or
                        as otherwise specified in these Terms of Service or other agreement you have entered into with
                        changeBot. We may also use proprietary fraud and risk modeling when assessing the risk
                        associated with your Account.
                    </p>
                    <p className="p23 ft3">
                        When a user buying cryptocurrency
                        <span className="ft13">​ </span>
                        with traditional electronic payment methods using the escrow service, the buyer is required to
                        transfer <span className="ft4">accurate amount of fiat currency</span> specified in the exchange
                        transaction
                        <span className="ft5">​</span>
                        <span className="ft4">in one payment</span>.<span className="ft13">​</span>
                    </p>
                </div>
                <div id="page_5">
                    <p className="p24 ft3">
                        In case of violation of these terms and conditions sellers are not required to release
                        cryptocurrency without a request of changeBot support staff and the transaction may be canceled
                        at the discretion of the support staff.
                    </p>
                    <p className="p3 ft12">
                        <span className="ft10">
                            Buyer and seller should be ready to prove his/her identity and payments proofs
                        </span>{' '}
                        for BITZLATO or transaction may be canceled at the discretion of the support staff. Buyer and
                        seller should check the counterparty identity and fiat money legality on their own as is
                        possible on any step of the deal. BITZLATO can not check the legality of fiat money or
                        cryptocurrency sources and does not accept any liability for your activity. By accepting these
                        terms you confirm that you will buy cryptocurrency only for money from legal for your
                        jurisdiction source and you will sell cryptocurrency only from{' '}
                        <span className="ft10">legal for your jurisdiction source</span>.<span className="ft11">​</span>
                    </p>
                    <p className="p25 ft22">
                        If you induce changeBot users to dealing without escrow or induce to release his cryptocurrency
                        without fact of receiving fiat money by seller, your account will be terminated at the staff
                        discretion. (Prevent fraud)
                    </p>
                    <p className="p3 ft23">
                        Cryptocurrency must be held in escrow during the whole trade process until the seller has
                        received payment in full.
                    </p>
                    <p className="p22 ft23">
                        Cryptocurrency buyers must be able to, upon request by BITZLATO, provide adequate proof of
                        payment for up to 180 days after the trade has been completed.
                    </p>
                    <p className="p22 ft24">
                        We use proprietary fraud and risk modeling when assessing the risk associated with your account,
                        your ads and your trades. If we find your account to pose a high risk to changeBot or our
                        customers we will consider you in breach of these terms of service and may temporarily limit
                        your account and freeze your cryptocurrency wallet in accordance with these terms of service.
                    </p>
                    <p className="p9 ft6">Disputes with trades</p>
                    <p className="p8 ft12">
                        If the buyer and seller of a cryptocurrency trade are in disagreement over a trade either party
                        or changeBot may initiate a dispute. Disputed trades are resolved by changeBot support staff.
                    </p>
                    <p className="p22 ft3">
                        Support staff of changeBot resolves disputes by carefully evaluating the terms of trade, payment
                        evidence, trade chat messages, user reputation, other data submitted to us by our users and data
                        collected by us as part of the privacy policy.
                    </p>
                    <p className="p3 ft3">
                        When sellers turn unresponsive, we will resolve the dispute to the buyer if we are confident the
                        seller has received valid payment from the buyer. If the buyer is unresponsive the seller may
                        dispute the trade and we will resolve the dispute to the seller.
                    </p>
                    <p className="p26 ft12">
                        After the trade has been released by the seller the trade is considered finished by us and
                        cannot be disputed, reversed or altered.
                    </p>
                    <p className="p22 ft12">
                        Providing fraudulent information or documents in a dispute or making false claims or otherwise
                        trying to force a certain outcome of a disputed trade is considered a violation of this
                        agreement.
                    </p>
                </div>
                <div id="page_6">
                    <p className="p24 ft3">
                        Disputes for &quot;Cash&quot; deals and other payment methods with personal meetings are not
                        allowed to disputes.
                    </p>
                    <p className="p4 ft3">
                        Providing fraudulent information or documents in a dispute or making false claims or otherwise
                        trying to force a certain outcome of a disputed trade is considered a violation of this
                        agreement. If payment details are provided outside of changeBot the dispute may be resolved
                        without taking the payment details into consideration.
                    </p>
                    <p className="p17 ft6">Disclaimer of Warranties/Liability</p>
                    <p className="p8 ft12">
                        Your use of changeBot is at your own risk. changeBot and all the materials, information,
                        software, facilities, services and other content are provided &#39;As Is&#39; and &#39;As
                        Available&#39; without warranties of any kind, either expresses or implied. BITZLATO does not
                        warrant that the functions contained in changeBot will be available, uninterrupted or{' '}
                        <nobr>error-free,</nobr> that defects will be corrected, or that changeBot or the servers that
                        make them available are free of viruses or other harmful components. BITZLATO LIMITED does not
                        warrant or make any representation.
                    </p>
                    <p className="p27 ft6">Personal Information</p>
                    <p className="p8 ft12">
                        We collect the personal data that you volunteer on forms which you submit to us for online
                        competitions and in messages that you send to us. In addition we automatically gather your Id in
                        Telegram messenger. We do not release this information to any outside party, except in suspected
                        fraud cases. When fraudulent activity is suspected, we may release the users details to the
                        party that the user has been trading with.
                    </p>
                    <p className="p10 ft6">Correct/Update</p>
                    <p className="p8 ft12">
                        You may request that we amend any personal data that we are holding about you which is factually
                        inaccurate. You contact us stating your wishes.
                    </p>
                    <p className="p10 ft25">Registration and user account</p>
                    <p className="p12 ft26">
                        To access changeBot resources it has to offer, you may be asked to provide registration details
                        in order to register a user account (&quot;account&quot;). It is a condition of use of changeBot
                        that all the details you provide will be correct, current, and complete. If we at BITZLATO
                        believe the details are not correct, current, or complete, we have the right to refuse you
                        access to changeBot, or any of its resources, and to terminate or suspend your account.
                    </p>
                    <p className="p28 ft12">
                        A user can only act on his own behalf. No BITZLATO account can be used to act as an intermediary
                        or broker. A user can only use his own trading accounts to trade at changeBot. A user account
                        must not contain misleading information about the user, including, but not limited to having{' '}
                        <nobr>non-personal</nobr> phone number or faking the country of origin. A user is not allowed to
                        share his or hers account access with any other person or entity without prior approval from
                        changeBot administrator. Sharing account access with any 3rd party might result your changeBot
                        account(s) to be terminated.
                    </p>
                </div>
                <div id="page_7">
                    <p className="p29 ft8">
                        We may at any time require you to complete our ID verification process and may also require you
                        to submit additional identification documents to changeBot if we deem it necessary. Failing to
                        complete ID verification will be considered a violation of this agreement.
                    </p>
                    <p className="p23 ft8">
                        If we determine that you have at any previous point in time violated this agreement or any other
                        agreement that you have entered into with us we will immediately close, suspend or limit your
                        account and cryptocurrency balance as per the terms outlined in this agreement.
                    </p>
                    <p className="p30 ft26">
                        We may close, suspend, or limit your access to your account or to other Services we offer,
                        and/or limit access to your cryptocurrency balance for up to 180 days (or longer if pursuant to
                        a court order or other legal process) if you violate these Terms of Service or any other
                        agreement you enter into with BITZLATO, or as otherwise specified in these Terms of Service or
                        other agreement you have entered into with BITZLATO. We may also use proprietary fraud and risk
                        modeling when assessing the risk associated with your account, high risk users are deemed to be
                        in violation of this agreement.
                    </p>
                    <p className="p14 ft6">
                        Submissions, Messages, Postings and <nobr>E-mails</nobr>
                    </p>
                    <p className="p31 ft26">
                        BITZLATO is always interested in hearing from you. It is, however, our policy that we will not
                        accept or consider unsolicited submissions of concepts, stories, or other potential content.
                        Therefore, please do not send BITZLATO any unsolicited submissions. From time to time, areas on
                        changeBot may expressly request submissions of concepts, stories, or other potential content
                        from you. Where this is the case, please carefully read any specific rules or other terms and
                        conditions which appear elsewhere on changeBot to govern those submissions, since they will
                        affect your legal rights. If no Additional Terms govern those submissions, then these Terms of
                        Service will apply in full to any submissions you make.
                    </p>
                    <p className="p32 ft26">
                        Please act responsibly when using changeBot. You may only use changeBot and its contents for
                        lawful purposes and in accordance with applicable law and you are prohibited from storing,
                        distributing or transmitting any unlawful material through changeBot. You recognize that
                        storing, distributing or transmitting unlawful material could expose you to criminal and/or
                        civil liability. You agree that if a third party claims that material you have contributed to
                        changeBot is unlawful, you will bear the burden of establishing that it is lawful. You
                        understand and agree that all materials publicly posted (other than by BITZLATO) or privately
                        transmitted on or through changeBot are the sole responsibility of the sender, not BITZLATO, and
                        that you are responsible for all material you upload, post or otherwise transmit to or through
                        changeBot.
                    </p>
                    <p className="p22 ft26">
                        We at BITZLATO require that you do not post emails or submit to or publish through Forums or
                        otherwise make available on changeBot any content, or act in a way, which in our opinion:
                    </p>
                    <p className="p33 ft26">
                        <span className="ft7">1.</span>
                        <span className="ft27">
                            libels, defames, invades privacy, stalks, is obscene, pornographic, racist, abusive,
                            harassing, threatening or offensive;
                        </span>
                    </p>
                    <p className="p33 ft26">
                        <span className="ft7">2.</span>
                        <span className="ft27">
                            seeks to exploit or harm children by exposing them to inappropriate content, asking for
                            personally identifiable details or otherwise;
                        </span>
                    </p>
                    <p className="p33 ft26">
                        <span className="ft7">3.</span>
                        <span className="ft27">
                            infringes any intellectual property or other right of any entity or person, including
                            violating anyone&#39;s copyrights or trademarks or their rights of publicity;
                        </span>
                    </p>
                    <p className="p34 ft7">
                        <span className="ft7">4.</span>
                        <span className="ft28">violates any law or may be considered to violate any law;</span>
                    </p>
                    <p className="p35 ft26">
                        <span className="ft7">5.</span>
                        <span className="ft27">
                            you do not have the right to transmit under any contractual or other relationship (e.g.,
                            inside information, proprietary or confidential information received in the context of an
                            employment or a{' '}
                        </span>
                        <nobr>non-disclosure</nobr>
                        agreement);
                    </p>
                    <p className="p34 ft7">
                        <span className="ft7">6.</span>
                        <span className="ft28">advocates or promotes illegal activity;</span>
                    </p>
                </div>
                <div id="page_8">
                    <p className="p33 ft26">
                        <span className="ft7">7.</span>
                        <span className="ft27">
                            impersonates or misrepresents your connection to any other entity or person or otherwise
                            manipulates headers or identifiers to disguise the origin of the content;
                        </span>
                    </p>
                    <p className="p36 ft26">
                        <span className="ft7">8.</span>
                        <span className="ft27">
                            advertises any commercial endeavor (e.g., offering for sale products or services) or
                            otherwise engages in any commercial activity (e.g., conducting raffles or contests,
                            displaying sponsorship banners, and/or soliciting goods or services) except as specifically
                            authorized on changeBot;
                        </span>
                    </p>
                    <p className="p34 ft7">
                        <span className="ft7">9.</span>
                        <span className="ft28">solicits funds, advertisers or sponsors;</span>
                    </p>
                    <p className="p35 ft26">
                        <span className="ft7">10.</span>
                        <span className="ft29">
                            includes programs which contain viruses, worms and/or &#39;Trojan horses&#39; or any other
                            computer code, files or programs designed to interrupt, destroy or limit the functionality
                            of any computer software or hardware or telecommunications;
                        </span>
                    </p>
                    <p className="p36 ft26">
                        <span className="ft7">11.</span>
                        <span className="ft29">
                            disrupts the normal flow of dialogue, causes a screen to scroll faster than other users are
                            able to type, or otherwise act in a way which affects the ability of other people to engage
                            in real time activities via changeBot;
                        </span>
                    </p>
                    <p className="p34 ft7">
                        <span className="ft7">12.</span>
                        <span className="ft30">
                            copies any other pages or images on changeBot except with appropriate authority;
                        </span>
                    </p>
                    <p className="p37 ft7">
                        <span className="ft7">13.</span>
                        <span className="ft30">includes MP3 format files;</span>
                    </p>
                    <p className="p37 ft7">
                        <span className="ft7">14.</span>
                        <span className="ft30">amounts to a &#39;pyramid&#39; or similar scheme;</span>
                    </p>
                    <p className="p35 ft26">
                        <span className="ft7">15.</span>
                        <span className="ft29">
                            amounts to &#39;data warehousing&#39; (i.e., using any web space made available to you as
                            storage for large files which are only linked from other sites). You must provide a
                            reasonable amount of content to accompany such material in order that at least some of the
                            traffic to your site comes directly via us;
                        </span>
                    </p>
                    <p className="p36 ft8">
                        <span className="ft7">16.</span>
                        <span className="ft31">
                            disobeys any policy or regulations established from time to time regarding use of changeBot
                            or any networks connected to changeBot; or contains links to other sites that contain the
                            kind of content, which falls within the descriptions above.
                        </span>
                    </p>
                    <p className="p38 ft8">
                        In addition, you are prohibited from removing any sponsorship banners or other material inserted
                        by BITZLATO anywhere on changeBot (e.g., on any web space made available for your use).
                    </p>
                    <p className="p5 ft6">Security</p>
                    <p className="p8 ft12">
                        You are prohibited from using any services or facilities provided in connection with changeBot
                        to compromise security or tamper with system resources and/or accounts. The use or distribution
                        of tools designed for compromising security (e.g. password guessing programs, cracking tools or
                        network probing tools) are strictly prohibited. If you become involved in any violation of
                        system security, BITZLATO reserves the right to release your details to system administrators at
                        other services and companies and government in order to assist them in resolving security
                        incidents.
                    </p>
                    <p className="p10 ft6">
                        Links and <nobr>third-Party</nobr> sites
                    </p>
                    <p className="p8 ft12">
                        ChangeBot contains links to other resources. BITZLATO is not responsible for the privacy
                        practices of such resources. This privacy statement applies solely to information collected by
                        BITZLATO.
                    </p>
                    <p className="p22 ft12">
                        ChangeBot may link you to other services, sites and other resources (“resources”) on the
                        Internet. These resources may contain information or material that some people may find
                        inappropriate or offensive. These other resources are not under the control of BITZLATO, and you
                        acknowledge that (whether or not such resources are affiliated in any way with changeBot)
                        BITZLATO is not responsible for the accuracy, copyright compliance, legality, decency, or any
                        other aspect of the content of such resources. The inclusion of such a link does not imply
                        endorsement of any resources by changeBot or any association with its operators. BITZLATO cannot
                        ensure that you will be satisfied with any products or services that you purchase from any{' '}
                        <nobr>third-party</nobr> resources that links to
                    </p>
                </div>
                <div id="page_9">
                    <p className="p24 ft3">
                        or from changeBot since other bots/stores are owned and operated by independent
                        webmasters/retailers.
                    </p>
                    <p className="p5 ft6">Termination</p>
                    <p className="p8 ft12">
                        Account of any user who violated these Terms of Use may be suspended on indefinite period or
                        terminated with the fully or partly confiscation of the coins and cancellation of all active
                        deals.
                    </p>
                    <p className="p10 ft6">Public Forums/Chat Room</p>
                    <p className="p8 ft3">
                        ChangeBot has a message board and chat room available to its users. Please remember that any
                        information that is disclosed in these areas become public information and you should exercise
                        caution when deciding to disclose your personal information.
                    </p>
                    <p className="p17 ft6">Investigations</p>
                    <p className="p8 ft12">
                        BITZLATO reserves the right to investigate suspected violations of these Terms of Use, including
                        without limitation any violation arising from any submission, posting or <nobr>e-mails</nobr>{' '}
                        you make or send to any Forum. BITZLATO may seek to gather information from the user who is
                        suspected of violating these Terms of Use, and from any other user. BITZLATO may suspend any
                        users whose conduct or postings are under investigation and may remove such material from its
                        servers as it deems appropriate and without notice. If we believes, in its sole discretion, that
                        a violation of these Terms of Use has occurred, it may edit or modify any submission, posting or{' '}
                        <nobr>e-mails,</nobr> remove the material permanently, cancel postings, warn users, suspend
                        users and passwords, terminate accounts or take other corrective action it deems appropriate.
                        BITZLATO will fully <nobr>co-operate</nobr> with any law enforcement authorities or court order
                        requesting or directing BITZLATO to disclose the identity of anyone posting any{' '}
                        <nobr>e-mails,</nobr> or publishing or otherwise making available any materials that are
                        believed to violate these Terms of Use.
                    </p>
                    <p className="p5 ft25">Data retention policy</p>
                    <p className="p12 ft32">
                        We retain your personal information as long as it is necessary and relevant for our operations.
                        In addition, we may retain personal information from closed accounts to comply with national
                        laws, prevent fraud, collect any fees owed, resolve disputes, troubleshoot problems, assist with
                        any investigation, enforce our Terms of Service and take other actions permitted or required by
                        applicable national laws. Messages and links to file attachments hosted on Telegram servers and
                        related to trades are stored for 180 days after the trade closes.
                    </p>
                    <p className="p14 ft6">How To Contact Us</p>
                    <p className="p15 ft1">BITZLATO LIMITED, HONG KONG.</p>
                    <p className="p16 ft1">Helpdesk in Telegram: @CBhelpBot</p>
                    <p className="p16 ft1">Helpdesk site: changebot.freshdesk.com</p>
                    <p className="p16 ft1">Site: changebot.info</p>
                </div>
            </div>

            {!licensingAgreementAccepted && (
                <div>
                    <br />
                    <br />
                    <Button variant="contained" onClick={acceptLicenceAgreement} color="primary">
                        {t('Accept')}
                    </Button>
                </div>
            )}
        </div>
    );
};

LicenceBlock.propTypes = {
    t: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,

    acceptLicenceAgreement: PropTypes.func,
    licensingAgreementAccepted: PropTypes.bool
};

export default withStyles(styles)(withNamespaces('profile')(LicenceBlock));

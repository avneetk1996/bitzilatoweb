// -*- rjsx -*-
import React from 'react';

const initialWidthContext = React.createContext('xs');

export const IWProvider = initialWidthContext.Provider;
export const IWConsumer = initialWidthContext.Consumer;

export default function withInitialWidth(Component) {
    return function Wrapper(props) {
        return <IWConsumer>{initialWidth => <Component initialWidth={initialWidth} {...props} />}</IWConsumer>;
    };
}

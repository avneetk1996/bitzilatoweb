// -*- rjsx -*-
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';
import { Button, DialogTitle, DialogContent, DialogContentText, DialogActions, Dialog } from '@material-ui/core';
import Markdown from 'react-remarkable';

import { clearAlerts } from '../../redux/actions';

class Alert extends Component {
    static propTypes = {
        t: PropTypes.func.isRequired,
        clearAlerts: PropTypes.func,
        alerts: PropTypes.object
    };

    clearAlerts = () => {
        this.props.clearAlerts();
    };

    render() {
        const { t, alerts } = this.props;

        const title =
            (alerts.type === 'alert' && 'Внимание') ||
            (alerts.type === 'warning' && 'Внимание') ||
            (alerts.type === 'error' && 'Произошла ошибка');

        return (
            <div>
                <Dialog open={Boolean(alerts.type)} onClose={this.clearAlerts} aria-labelledby="form-alert-title">
                    <DialogTitle>{title}</DialogTitle>
                    <DialogContent>
                        <DialogContentText component="div">
                            <Markdown>{t(alerts.text)}</Markdown>
                        </DialogContentText>
                    </DialogContent>

                    <DialogActions>
                        <Button onClick={this.clearAlerts}>OK</Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        alerts: state.alerts
    };
};

const mapDispatchToProps = dispatch => {
    return {
        clearAlerts: () => dispatch(clearAlerts())
    };
};

export default withNamespaces('errors')(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(Alert)
);

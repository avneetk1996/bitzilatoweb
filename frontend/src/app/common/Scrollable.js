// -*- rjsx -*-
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';

const styles = () => ({
    scrollable: {
        overflowY: 'auto',
        overflowX: 'hidden',
        '&::-webkit-scrollbar': { '-webkit-appearance': 'none' },
        '&::-webkit-scrollbar:vertical': { width: '11px' },
        '&::-webkit-scrollbar:horizontal': { height: '11px' },
        '&::-webkit-scrollbar-thumb': {
            borderRadius: '8px',
            border: '2px solid white',
            backgroundColor: 'rgba(0, 0, 0, .5)'
        },
        '&::-webkit-scrollbar-track ': {
            backgroundColor: '#fff',
            borderRadius: '8px'
        }
    }
});

const Scrollable = ({ enable = true, maxHeight, classes, children }) => {
    const props = enable ? { className: classes.scrollable, style: { maxHeight } } : {};
    return <div {...props}>{children}</div>;
};

Scrollable.propTypes = {
    maxHeight: PropTypes.string.isRequired,
    classes: PropTypes.object.isRequired,
    children: PropTypes.node.isRequired,
    enable: PropTypes.bool,
};

export default withStyles(styles)(Scrollable);

// -*- rjsx -*-
import React from 'react';
import PropTypes from 'prop-types';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button } from '@material-ui/core';
import Markdown from 'react-remarkable';
import { withNamespaces } from 'react-i18next';

const Confirm = props => {
    const { opened, handleOk, handleCancel, title, message, t } = props;

    return (
        <Dialog open={opened}>
            <DialogTitle>{title}</DialogTitle>

            <DialogContent>
                <Markdown>{message}</Markdown>
            </DialogContent>

            <DialogActions>
                <Button onClick={handleCancel} color="primary">
                    {t('Cancel')}
                </Button>

                <Button onClick={handleOk} color="primary">
                    {t('Ok')}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

Confirm.propTypes = {
    t: PropTypes.func.isRequired,

    opened: PropTypes.bool,
    title: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    handleOk: PropTypes.func.isRequired,
    handleCancel: PropTypes.func
};

export default withNamespaces('common')(Confirm);

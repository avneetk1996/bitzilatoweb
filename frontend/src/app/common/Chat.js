// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Typography, withStyles, IconButton, Grid, Tooltip } from '@material-ui/core';
import { reduxForm, Field, Form, reset } from 'redux-form';
import { renderTextField } from './redux-form-helpers';
import { withNamespaces } from 'react-i18next';
import AttachFile from '@material-ui/icons/AttachFile';
import SendIcon from '@material-ui/icons/Send';
//import Viewer from 'react-viewer';
//import 'react-viewer/dist/index.css';

const styles = () => ({
    paper: {
        padding: 24
    },
    messages: {
        minHeight: '300px',
        maxHeight: '300px',
        overflowY: 'scroll',
        border: '1px solid #ccc',
        '& img': {
            maxWidth: '100px',
            maxHeight: '100px'
        }
    },
    messageWrap: {
        minHeight: 50,
        paddingBottom: 5,
        paddingTop: 5,
        paddingRight: 20,
        paddingLeft: 20,
        marginBottom: '-1px'
    },
    messageBodyIn: {
        minHeight: 50,
        paddingBottom: 5,
        paddingTop: 5,
        paddingRight: 10,
        backgroundColor: '#f3f3f3',
        position: 'relative'
    },
    messageBodyOut: {
        minHeight: 50,
        paddingBottom: 5,
        paddingTop: 5,
        paddingRight: 10,
        backgroundColor: '#dff1ff',
        position: 'relative'
    },
    timeString: {
        position: 'absolute',
        right: 10,
        bottom: 3
    },
    messageTextBlock: {
        display: 'flex',
        alignItems: 'center'
    },
    messageText: {
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 20,
        wordBreak: 'break-word'
    },
    nameBlock: {
        paddingRight: 10
    },
    breaker: {
        wordBreak: 'break-all',
        paddingTop: 15,
        paddingBottom: 10
    },
    timeBlock: {
        textAlign: 'center'
    }
});

class Chat extends Component {

    /*
    state = {
      isViewerOpened: false
    };*/

    static propTypes = {
        t: PropTypes.func.isRequired,
        classes: PropTypes.object.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        onSubmit: PropTypes.func.isRequired,
        clearForm: PropTypes.func.isRequired,

        messages: PropTypes.array.isRequired,
        chatName: PropTypes.string.isRequired,
        buttonVariant: PropTypes.string,
        form: PropTypes.string.isRequired,
        sendFile: PropTypes.func,

        disabled: PropTypes.boolean
    };

    componentDidMount() {
        //this.scrollToBottom();
    }

    componentDidUpdate() {
        //this.scrollToBottom();
    }

    scrollToBottom = () => {
        this.messagesEnd.scrollIntoView({ behavior: 'smooth' });
    };

    sendMessage = values => {
        if (!_.isEmpty(values)) {
            this.props.clearForm(this.props.form);
            this.props.onSubmit(values);
        }
    };

    sendFile = async e => {
        e.preventDefault();
        await this.props.sendFile(e.target.files[0]);
    };

    render() {
        const { classes, chatName, disabled } = this.props;
        const messages = this.props.messages;

        return (
            <Grid container spacing={8}>
                <Grid item xs={12}>
                    <Typography variant="subtitle1">{chatName}</Typography>
                </Grid>

                <Grid item xs={12}>
                    <div className={classes.messages}>
                        {this.renderMessages(messages)}
                        <div
                            style={{ float: 'left', clear: 'both' }}
                            ref={el => {
                                this.messagesEnd = el;
                            }}
                        />
                    </div>
                </Grid>

                <Grid item xs={12}>
                    {this.sendMessageForm()}
                </Grid>
            </Grid>
        );
    }

    renderMessages() {
        const { classes, messages, t } = this.props;

        return _.map(messages, ({ id, created: date, type, file, message }) => {
            return (
                <div className={classes.messageWrap} key={id}>
                    <Grid container>
                        {type === 'out' && <Grid item xs={2} sm={1} />}

                        <Grid item xs={10} sm={11}>
                            <Grid container className={type === 'out' ? classes.messageBodyOut : classes.messageBodyIn}>
                                <Grid item xs={10} sm={11} className={classes.messageTextBlock}>
                                    {message && (
                                        <div className={classes.messageText}>
                                            <Typography variant="body2">{message}</Typography>
                                        </div>
                                    )}
                                    {
                                        file &&
                                        <a href={file.url} target={'_blank'}>
                                            <img src={file.url} alt="" />
                                            {
                                                //<img src={file.url} alt="" onClick={() => {this.setState({ isViewerOpened: true });}}  />
                                                //this.state.isViewerOpened === true &&
                                                //<Viewer visible={true} onClose={() => { this.setState({ isViewerOpened: false });}} images={[{src: file, alt: ''}]}/>
                                            }
                                        </a>
                                    }

                                    <Tooltip title={t('timeWithDate', { time: date })}>
                                        <span className={classes.timeString}>
                                            <Typography variant="caption">
                                                {t('timeWithoutDate', { time: date })}
                                            </Typography>
                                        </span>
                                    </Tooltip>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </div>
            );
        });
    }

    sendMessageForm() {
        const { buttonVariant, handleSubmit, sendFile, disabled, t } = this.props;

        const chatFileInput = `${this.props.form}-file-input`;

        return (
            <Form action="" onSubmit={handleSubmit(this.sendMessage)}>
                <Grid container alignItems="flex-end">
                    {sendFile && (
                        <Grid item>
                            <IconButton
                                variant={buttonVariant}
                                aria-haspopup="true"
                                color="primary"
                                disabled={disabled}
                            >
                                <label htmlFor={chatFileInput}>
                                    <AttachFile />
                                </label>
                            </IconButton>

                            <input
                                id={chatFileInput}
                                style={{ display: 'none' }}
                                accept=".jpg, .jpeg, .png"
                                multiple={false}
                                type="file"
                                onChange={this.sendFile}
                                disabled={disabled}
                            />
                        </Grid>
                    )}

                    <Grid item xs>
                        <Field
                            name="message"
                            component={renderTextField}
                            label={t('Write a message...')}
                            fullWidth
                            type="text"
                            style={{ flexGrow: 1 }}
                            disabled={disabled}
                        />
                    </Grid>

                    <Grid item>
                        <IconButton
                            type="submit"
                            variant={buttonVariant}
                            aria-haspopup="true"
                            color="primary"
                            disabled={disabled}
                        >
                            <SendIcon />
                        </IconButton>
                    </Grid>
                </Grid>
            </Form>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        clearForm: formName => dispatch(reset(formName))
    };
};

export default reduxForm({})(
    withNamespaces('common')(
        withStyles(styles)(
            connect(
                null,
                mapDispatchToProps
            )(Chat)
        )
    )
);

// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { withNamespaces } from 'react-i18next';
import { compose } from 'redux';

import { Badge, Tooltip, Divider, withStyles, Menu, MenuItem, IconButton } from '@material-ui/core';

import MailIcon from '@material-ui/icons/Mail';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import CloseIcon from '@material-ui/icons/Close';

import Markdown from 'react-remarkable';

import { notificationInfo } from './notifications';

import {
    loadNotifications,
    showNotifications,
    hideNotifications,
    markNotificationAsReaded,
    markAllNotificationsAsReaded,
    showAlert
} from '../../redux/actions';

const styles = () => ({
    deleteButton: {
        position: 'absolute',
        right: '10px'
    },
    menuItem: {
        padding: 5,
        margin: 0,
        paddingRight: '35px',
        paddingLeft: '15px',
        height: 'auto',
        '& *': {
            padding: 0,
            margin: 0
        },
        whiteSpace: 'normal'
    }
});

class NotificationsList extends Component {
    static propTypes = {
        t: PropTypes.func.isRequired,
        classes: PropTypes.object.isRequired,

        notifications: PropTypes.array,
        anchorEl: PropTypes.object,
        userId: PropTypes.number,

        loadNotifications: PropTypes.func.isRequired,
        markNotificationAsReaded: PropTypes.func.isRequired,
        showAlert: PropTypes.func.isRequired,
        markAllNotificationsAsReaded: PropTypes.func.isRequired,
        showNotifications: PropTypes.func.isRequired,
        hideNotifications: PropTypes.func.isRequired,
        push: PropTypes.func.isRequired
    };

    componentDidMount() {
        this.props.loadNotifications();
    }

    info(item) {
        return notificationInfo(item, this.props);
    }

    openNotifications = event => {
        this.props.showNotifications(event.currentTarget);
    };

    closeNotifications = () => {
        this.props.hideNotifications();
    };

    markAsRead = id => {
        this.props.markNotificationAsReaded(id);
    };

    handleClick = (id, link, alert) => {
        this.props.markNotificationAsReaded(id);

        if (alert) {
            this.props.showAlert(alert);
        }

        if (link) {
            this.props.push(link);
        }
    };

    render() {
        const { notifications, anchorEl, t, classes } = this.props;
        const style = { display: 'inline-block' };

        return (
            <div style={style}>
                <Tooltip
                    title={
                        notifications && notifications.length !== 0
                            ? t('notificationsCount', { count: notifications.length })
                            : t('No notifications')
                    }
                >
                    <IconButton
                        aria-haspopup="true"
                        color="inherit"
                        onClick={notifications.length ? this.openNotifications : null}
                    >
                        {Boolean(notifications) && notifications.length !== 0 ? (
                            <Badge badgeContent={notifications.length} color="secondary">
                                <MailIcon />
                            </Badge>
                        ) : (
                            <MailIcon color="disabled" />
                        )}
                    </IconButton>
                </Tooltip>

                <Menu
                    id="header-notifications"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right'
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right'
                    }}
                    open={Boolean(anchorEl)}
                    onClose={this.closeNotifications}
                >
                    {notifications.reverse().map(item => {
                        const { text, link, alert } = this.info(item);

                        return (
                            <MenuItem
                                className={classes.menuItem}
                                onClick={() => this.handleClick(item.id, link, alert)}
                                key={item.id}
                            >
                                <Markdown>{text || 'Unknown notification'}</Markdown>

                                <IconButton
                                    key="close"
                                    aria-label="Close"
                                    color="inherit"
                                    className={classes.deleteButton}
                                    onClick={e => {
                                        e.stopPropagation();
                                        this.markAsRead(item.id);
                                    }}
                                >
                                    <CloseIcon />
                                </IconButton>
                            </MenuItem>
                        );
                    })}

                    {_.size(notifications) > 1 && <Divider />}

                    {_.size(notifications) > 1 && (
                        <MenuItem onClick={() => this.props.markAllNotificationsAsReaded()}>
                            <DeleteForeverIcon />
                            {t('Remove All')}
                        </MenuItem>
                    )}
                </Menu>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    notifications: _.get(state, 'notifications.items', []),
    anchorEl: _.get(state, 'notifications.anchorEl'),
    userId: _.get(state, 'auth.userId')
});

const mapDispatchToProps = dispatch => {
    return {
        showAlert: message => dispatch(showAlert('alert', message)),
        loadNotifications: () => dispatch(loadNotifications()),
        markNotificationAsReaded: id => dispatch(markNotificationAsReaded(id)),
        markAllNotificationsAsReaded: () => dispatch(markAllNotificationsAsReaded()),
        showNotifications: anchorEl => dispatch(showNotifications(anchorEl)),
        hideNotifications: () => dispatch(hideNotifications()),
        push: link => dispatch(push(link))
    };
};

export default compose(
    withNamespaces('notifications'),
    withStyles(styles),
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(NotificationsList);

// -*- rjsx -*-
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withSnackbar, SnackbarProvider } from 'notistack';
import { withNamespaces } from 'react-i18next';

import { removeSnackbar } from '../../redux';

class Notifier extends Component {
    static propTypes = {
        snackbars: PropTypes.array.isRequired,
        enqueueSnackbar: PropTypes.func.isRequired,
        removeSnackbar: PropTypes.func.isRequired,
        t: PropTypes.func.isRequired
    };

    displayed = [];

    shouldComponentUpdate({ snackbars: newSnacks = [] }) {
        const { snackbars: currentSnacks } = this.props;
        let notExists = false;
        for (let i = 0; i < newSnacks.length; i++) {
            if (notExists) continue;
            notExists = notExists || !currentSnacks.filter(({ key }) => newSnacks[i].key === key).length;
        }
        return notExists;
    }

    componentDidUpdate() {
        const { t, snackbars = [], enqueueSnackbar, removeSnackbar } = this.props;

        snackbars.forEach(n => {
            if (this.displayed.includes(n.key)) return;
            const msg = t(n.message, n.options);
            enqueueSnackbar(msg, n.snackOptions);
            this.displayed.push(n.key);
            removeSnackbar(n.key);
        });
    }

    render() {
        return null;
    }
}

const ConnectedNotifier = compose(
    withNamespaces('snacks'),
    connect(
        ({ snackbars }) => ({ snackbars }),
        { removeSnackbar }
    ),
    withSnackbar
)(Notifier);

const MainNotifier = () => (
    <SnackbarProvider>
        <ConnectedNotifier />
    </SnackbarProvider>
);

export default MainNotifier;

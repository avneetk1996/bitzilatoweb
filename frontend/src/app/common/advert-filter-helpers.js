import _ from 'lodash';

// https://stackoverflow.com/questions/9716468/pure-javascript-a-function-like-jquerys-isnumeric
const isInt = n => !isNaN(parseInt(n)) && isFinite(n);

export const DEFAULT_ROWS_SKIP = 0;
export const DEFAULT_ROWS_PER_PAGE = 10;

export function parseFilterParams(search) {
    const sp = new URLSearchParams(search);

    const searchObj = {
        limit: isInt(sp.get('limit')) ? parseInt(sp.get('limit')) : 10,
        skip: isInt(sp.get('skip')) ? parseInt(sp.get('skip')) : 0,
        isOwnerActive: sp.get('isOwnerActive') === 'true',
        isOwnerVerificated: sp.get('isOwnerVerificated') !== 'false'
    };

    if (sp.has('paymethod')) {
        const paymethod = sp
            .getAll('paymethod')
            .filter(p => isInt(p))
            .map(p => parseInt(p));

        if (paymethod.length > 0) searchObj.paymethod = paymethod;
    }

    if (sp.has('type')) searchObj.type = sp.get('type');
    if (sp.has('cryptocurrency')) searchObj.cryptocurrency = sp.get('cryptocurrency');
    if (sp.has('currency')) searchObj.currency = sp.get('currency');

    return searchObj;
}

export function pickFilter(filter) {
    const newFilter = { ...filter };

    if (newFilter.skip === DEFAULT_ROWS_SKIP) {
        _.unset(newFilter, 'skip');
    }

    if (newFilter.limit === DEFAULT_ROWS_PER_PAGE) {
        _.unset(newFilter, 'limit');
    }

    if (!newFilter.isOwnerActive) {
        _.unset(newFilter, 'isOwnerActive');
    }

    if (newFilter.isOwnerVerificated) {
        _.unset(newFilter, 'isOwnerVerificated');
    }

    return newFilter;
}

export function ensureFilter(filter) {
    return {
        type: 'purchase',
        currency: 'RUB',
        cryptocurrency: 'BTC',
        ...filter
    };
}

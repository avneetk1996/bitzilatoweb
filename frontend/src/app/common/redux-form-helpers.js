// -*- rjsx -*-
import React from 'react';
import PropTypes from 'prop-types';
import { MenuItem, TextField, Switch, FormHelperText, FormControl, FormControlLabel } from '@material-ui/core';

export function renderTextField(props) {
    const {
        input,
        fullWidth,
        multiline,
        rows,
        meta: { touched, error, warning },
        variant
    } = props;
    const errorColor = { color: 'red', position: 'absolute', bottom: -20 };
    const width = fullWidth ? { width: '100%'  } : { width: 'inherit' };

    return (
        <FormControl style={width} >
            <TextField {...props} {...input} rows={rows} multiline={multiline} variant={variant} margin="normal"/>
            {touched &&
                ((error && <FormHelperText style={errorColor}>{error}</FormHelperText>) ||
                    (warning && <FormHelperText>{warning}</FormHelperText>))}
        </FormControl>
    );
}

export function renderSelectField(props) {
    const {
        input,
        options,
        fullWidth,
        meta: { touched, error, warning }
    } = props;
    const errorColor = { color: 'red', position: 'absolute', bottom: -20 };
    const width = fullWidth ? { width: '100%' } : { width: 'inherit' };

    return (

        <FormControl style={width}>
            <TextField select fullWidth {...props} {...input}>
                {options &&
                    options.length > 0 &&
                    options.map((option, key) => (
                        <MenuItem key={`${option.value}-${key}`} value={option.value}>
                            {option.label}
                        </MenuItem>
                    ))}
            </TextField>

            {touched &&
                ((error && <FormHelperText style={errorColor}>{error}</FormHelperText>) ||
                    (warning && <FormHelperText>{warning}</FormHelperText>))}
        </FormControl>
    );
}

export function renderSwitch({ input, label }) {
    return (
        <FormControlLabel
            label={label}
            control={<Switch checked={input.value ? true : false} onChange={input.onChange} />}
        />
    );
}

renderSwitch.propTypes = {
    input: PropTypes.object,
    label: PropTypes.string
};

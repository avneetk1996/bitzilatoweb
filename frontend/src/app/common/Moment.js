import moment from 'moment-timezone';
import { connect } from 'react-redux';
import _ from 'lodash';

function Moment({ value, lang, timezone, format = 'lll' }) {
    return moment(value)
        .locale(lang)
        .tz(timezone)
        .format(format);
}

export default connect(({ timezone, profile }) => {
    const lang = _.get(profile, 'settings.lang', 'ru');
    return { timezone, lang };
})(Moment);

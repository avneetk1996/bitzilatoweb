// -*- rjsx -*-
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import { Button, Toolbar, IconButton, AppBar, Typography, withStyles, Divider, Grid, Hidden } from '@material-ui/core';
import { Menu, MenuItem } from '@material-ui/core';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Dashboard from '@material-ui/icons/Dashboard';
import Build from '@material-ui/icons/Build';
import Business from '@material-ui/icons/Business';
import Close from '@material-ui/icons/Close';
import CompareArrows from '@material-ui/icons/CompareArrows';
import ShowChart from '@material-ui/icons/ShowChart';
import ReactTimeout from 'react-timeout';

import { PROFILE_URL, WALLETS_URL, P2P_URL, AUTH_URL, STATIC_URL } from '../../config';
import NotificationsList from './NotificationsList';
import { showNotifications, loadLastDeal } from '../../redux';
import withInitialWidth from '../hocs/withInitialWidth';
import { ensureFilter } from './advert-filter-helpers';

const styles = () => ({
    // appBar: {
    //     backgroundColor: '#2196F3'
    // },
    logo: {
        height: '32px',
        marginTop: '8px'
    },
    headline: {
        margin: '0 auto'
    },
    link: {
        textDecoration: 'none',
        color: 'white'
    },
    toolbar: {
        width: '100%',
        maxWidth: '1200px',
        margin: '0 auto'
    },
    userName: {
        display: 'inline-block',
        verticalAlign: 'middle'
    },
    notifBlock: {
        marginRight: 10
    },
    menuIcon: {
        marginRight: 10
    },
    menuText: {
        textDecoration: 'none'
    },
    loginButton: {
        backgroundColor: '#FDA535'
    },
    marquee: {
        width: '100%',
        margin: '0 auto',
        whiteSpace: 'nowrap',
        boxSizing: 'border-box',
        overflow: 'hidden',
        verticalAlign: 'middle',
        height: '100%',

        '&>span': {
            display: 'inline-block',
            paddingLeft: '100%',
            textIndent: 0,
            animation: 'marquee 40s linear infinite',

            '&>span': {
                color: 'white',
                fontWeight: 'bold'
            }
        }
    },
    '@global': {
        '@keyframes marquee': {
            '0%': {
                transform: 'translate(0, 0)'
            },
            '100%': {
                transform: 'translate(-100%, 0)'
            }
        }
    }
});

class Header extends Component {
    constructor() {
        super();

        this.state = {
            menuEl: null
        };
    }

    static propTypes = {
        setInterval: PropTypes.func.isRequired,
        t: PropTypes.func.isRequired,
        classes: PropTypes.object.isRequired,
        initialWidth: PropTypes.string,

        maintenance: PropTypes.bool,
        isLogged: PropTypes.bool,
        username: PropTypes.string,
        notificationCount: PropTypes.number,
        lastDeal: PropTypes.object,

        showNotifications: PropTypes.func.isRequired,
        loadLastDeal: PropTypes.func.isRequired,
        lastFilter: PropTypes.object,
    };

    componentDidMount() {
        const { loadLastDeal, setInterval } = this.props;
        loadLastDeal();
        setInterval(async () => {
            await loadLastDeal();
            this.setState({ timestamp: Date.now() });
        }, 40 * 1000);
    }

    openMenu = event => {
        this.setState({ menuEl: event.currentTarget });
    };

    closeMenu = () => {
        this.setState({
            menuEl: null
        });
    };

    p2pUrl() {
        const sp = new URLSearchParams();

        Object.entries(ensureFilter(this.props.lastFilter)).forEach(([key, value]) => {
            if (Array.isArray(value)) {
                value.forEach(i => sp.append(key, i));
            } else {
                sp.append(key, value);
            }
        });

        return `${P2P_URL || '/'}?${sp.toString()}`;
    }

    render() {
        const { lastDeal, maintenance, isLogged, username, initialWidth, t, classes } = this.props;

        const { menuEl } = this.state;

        return (
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar>
                    <div className={classes.toolbar}>
                        <Grid container justify="space-between">
                            <Grid item>
                                <Typography variant="subtitle1" color="inherit" className={classes.headline}>
                                    <Link to={this.p2pUrl()} className={classes.link}>
                                        <img src={`${STATIC_URL}/logo.png`} alt="" className={classes.logo} />
                                    </Link>
                                </Typography>
                            </Grid>

                            <Hidden xsDown initialWidth={initialWidth}>
                                <Grid item xs={8}>
                                    {lastDeal && (
                                        <Typography
                                            variant="caption"
                                            component="div"
                                            className={classes.marquee}
                                            style={{ paddingTop: '15px' }}
                                        >
                                            <span>
                                                <span>{t('lastDeal', lastDeal)}</span>
                                            </span>
                                        </Typography>
                                    )}
                                </Grid>
                            </Hidden>

                            <Grid item>
                                {!maintenance && !isLogged && (
                                    <div>
                                        <Button
                                            variant="contained"
                                            className={classes.loginButton}
                                            href={`${AUTH_URL}/login`}
                                        >
                                            {t('Login')}
                                        </Button>
                                    </div>
                                )}

                                {!maintenance && isLogged && (
                                    <div>
                                        <Typography variant="subtitle2" color="inherit" className={classes.userName}>
                                            {username}
                                        </Typography>

                                        <NotificationsList />

                                        <IconButton
                                            aria-owns={menuEl ? 'menu-appbar' : null}
                                            aria-haspopup="true"
                                            color="inherit"
                                            onClick={this.openMenu}
                                        >
                                            <AccountCircle />
                                        </IconButton>

                                        <Menu
                                            id="menu-appbar"
                                            anchorEl={menuEl}
                                            anchorOrigin={{
                                                vertical: 'top',
                                                horizontal: 'right'
                                            }}
                                            transformOrigin={{
                                                vertical: 'top',
                                                horizontal: 'right'
                                            }}
                                            open={Boolean(menuEl)}
                                            onClose={this.closeMenu}
                                        >
                                            <Link to={PROFILE_URL}>
                                                <MenuItem onClick={this.closeMenu}>
                                                    <div className={classes.menuIcon}>
                                                        <AccountCircle />
                                                    </div>

                                                    <div className={classes.menuText}>{t('My profile')}</div>
                                                </MenuItem>
                                            </Link>

                                            <Link to={`${P2P_URL}/trades`}>
                                                <MenuItem onClick={this.closeMenu}>
                                                    <div className={classes.menuIcon}>
                                                        <Business />
                                                    </div>

                                                    <div className={classes.menuText}>{t('My trades')}</div>
                                                </MenuItem>
                                            </Link>

                                            <Link to={WALLETS_URL}>
                                                <MenuItem onClick={this.closeMenu}>
                                                    <div className={classes.menuIcon}>
                                                        <Business />
                                                    </div>

                                                    <div className={classes.menuText}>{t('My wallets')}</div>
                                                </MenuItem>
                                            </Link>

                                            <Link to={`${P2P_URL}/adverts`}>
                                                <MenuItem onClick={this.closeMenu}>
                                                    <div className={classes.menuIcon}>
                                                        <Dashboard />
                                                    </div>

                                                    <div className={classes.menuText}>{t('My adverts')}</div>
                                                </MenuItem>
                                            </Link>

                                            <Link to={`${P2P_URL}/tools`}>
                                                <MenuItem onClick={this.closeMenu}>
                                                    <div className={classes.menuIcon}>
                                                        <Build />
                                                    </div>

                                                    <div className={classes.menuText}>{t('Tools')}</div>
                                                </MenuItem>
                                            </Link>

                                            <Divider />

                                            <MenuItem onClick={() => window.location.replace(`${AUTH_URL}/logout`)}>
                                                <div className={classes.menuIcon}>
                                                    <Close />
                                                </div>

                                                <div className={classes.menuText}>{t('Logout')}</div>
                                            </MenuItem>
                                        </Menu>
                                    </div>
                                )}
                            </Grid>

                            <Hidden smUp initialWidth={initialWidth}>
                                <Grid item xs={12}>
                                    {lastDeal && (
                                        <Typography variant="caption" component="div" className={classes.marquee}>
                                            <span>
                                                <span>{t('lastDeal', lastDeal)}</span>
                                            </span>
                                        </Typography>
                                    )}
                                </Grid>
                            </Hidden>
                        </Grid>
                    </div>
                </Toolbar>
            </AppBar>
        );
    }
}

const mapStateToProps = state => {
    return {
        lastDeal: state.lastdeal,
        maintenance: state.maintenance,
        isLogged: !_.isEmpty(state.auth),
        username: _.get(state, 'profile.settings.name'),
        notificationCount: state.notifications.items.length,
        lastFilter: _.get(state, 'exchange.lastFilter'),
    };
};

const mapDispatchToProps = dispatch => {
    return {
        showNotifications: () => dispatch(showNotifications(true)),
        loadLastDeal: () => dispatch(loadLastDeal())
    };
};

export default compose(
    withInitialWidth,
    withNamespaces('header'),
    withStyles(styles),
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    ReactTimeout
)(Header);

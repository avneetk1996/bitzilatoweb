// -*- rjsx -*-
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button } from '@material-ui/core';
import { reduxForm, Form, Field } from 'redux-form';
import { withNamespaces } from 'react-i18next';
import { renderTextField } from './redux-form-helpers';

class Prompt extends Component {
    static propTypes = {
        t: PropTypes.func.isRequired,
        initialize: PropTypes.func.isRequired,
        handleSubmit: PropTypes.func.isRequired,

        multiline: PropTypes.any,
        rows: PropTypes.any,
        variant: PropTypes.any,
        opened: PropTypes.bool,
        title: PropTypes.string.isRequired,
        message: PropTypes.string.isRequired,
        closeDialog: PropTypes.func.isRequired,
        defaultValue: PropTypes.any,
        handlePrompt: PropTypes.func.isRequired
    };

    componentDidMount() {
        this.props.initialize({ value: this.props.defaultValue });
    }

    componentDidUpdate(prevProps) {
        if (prevProps.defaultValue !== this.props.defaultValue) {
            this.props.initialize({ value: this.props.defaultValue });
        }
    }

    onSubmit = ({ value }) => {
        this.props.handlePrompt(value);
    };

    render() {
        const { opened, handleSubmit, closeDialog, title, message, t, multiline, rows, variant } = this.props;
        return (
            <Dialog open={opened} onClose={closeDialog} fullWidth >
                <Form onSubmit={handleSubmit(this.onSubmit)}>
                    <DialogTitle>{title}</DialogTitle>

                    <DialogContent>
                        <Field
                            name="value"
                            label={message}
                            placeholder={message}
                            fullWidth
                            component={renderTextField}
                            multiline={multiline}
                            rows={rows}
                            variant={variant}
                        />
                    </DialogContent>

                    <DialogActions>
                        <Button fullWidth variant="contained" type="submit" color="primary">
                            {t('Ok')}
                        </Button>
                    </DialogActions>
                </Form>
            </Dialog>
        );
    }
}

export default reduxForm({ form: 'promptForm' })(withNamespaces('common')(Prompt));

// -*- rjsx -*-
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';
import { Button, DialogTitle, DialogContent, DialogActions, Dialog, Typography } from '@material-ui/core';

import { closeAccessToken, getAccessToken } from '../../redux/actions';
import Moment from '../common/Moment';

const commonPropTypes = {
    t: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    close: PropTypes.func.isRequired,
    get: PropTypes.func.isRequired,
    accessToken: PropTypes.string,
    expiryDate: PropTypes.number
};

class AccessToken extends Component {
    static propTypes = commonPropTypes;

    componentDidMount() {
        this.props.get();
    }

    render() {
        const { t, accessToken, expiryDate, get } = this.props;

        return accessToken === null ? (
            <DialogContent>
                <pre>Loading ...</pre>
            </DialogContent>
        ) : (
            <DialogContent>
                <pre>{accessToken === null ? 'Loading ...' : accessToken}</pre>
                <Typography>
                    {t('Exp. date')}: {expiryDate === null ? t('Indefinite') : <Moment value={expiryDate} />}
                </Typography>
                <br />
                <br />
                <Button onClick={() => get(true)} variant="outlined" color="primary">
                    {t('Generate new API token')}
                </Button>
            </DialogContent>
        );
    }
}

class Wrapper extends Component {
    static propTypes = commonPropTypes;

    render() {
        const { t, open, close } = this.props;

        return (
            <Dialog open={open} onClose={close} aria-labelledby={t('API token')}>
                <DialogTitle>{t('API token')}</DialogTitle>
                {open && <AccessToken {...this.props} />}

                <DialogActions>
                    <Button onClick={close} color="primary" autoFocus>
                        {t('close')}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default compose(
    withNamespaces('tools'),
    connect(
        ({ accessToken: { open, accessToken, expiryDate } }) => ({ open, accessToken, expiryDate }),
        { close: closeAccessToken, get: getAccessToken }
    )
)(Wrapper);

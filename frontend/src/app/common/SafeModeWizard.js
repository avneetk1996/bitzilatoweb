// -*- rjsx -*-
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';
import {
    Button,
    DialogTitle,
    DialogContent,
    DialogContentText,
    DialogActions,
    Dialog,
    FormControl,
    FormControlLabel,
    FormLabel,
    RadioGroup,
    Radio
} from '@material-ui/core';

import { closeSafeMode, nextStepSafeMode, setValueSafeMode, errorSafeMode, updateProfile } from '../../redux/actions';

const safemodeAnswers = { 1: '1', 2: '1', 3: '1' };

const Step = ({ t, value, setValue, question, o1, o2 }) => (
    <FormControl component="fieldset">
        <FormLabel component="legend">{question}</FormLabel>
        <RadioGroup aria-label="Gender" name="q1" value={value} onChange={e => setValue(e.target.value)}>
            <FormControlLabel value="0" control={<Radio />} label={t(o1)} />
            <FormControlLabel value="1" control={<Radio />} label={t(o2)} />
        </RadioGroup>
    </FormControl>
);

Step.propTypes = {
    t: PropTypes.func.isRequired,
    value: PropTypes.string,
    setValue: PropTypes.func.isRequired,
    question: PropTypes.string.isRequired,
    o1: PropTypes.string.isRequired,
    o2: PropTypes.string.isRequired
};

class SafeModeWizard extends Component {
    static propTypes = {
        t: PropTypes.func,
        closeSafeMode: PropTypes.func,
        opened: PropTypes.bool,
        step: PropTypes.number,
        value: PropTypes.string,
        close: PropTypes.func,
        error: PropTypes.bool,
        nextStep: PropTypes.func,
        setValue: PropTypes.func,
        errorSafeMode: PropTypes.func,
        updateProfile: PropTypes.func
    };

    nextStep = () => {
        const { step, value, nextStep, errorSafeMode, updateProfile } = this.props;

        if (step > 0 && step < 4) {
            if (safemodeAnswers[step] === value) {
                nextStep();
                if (step === 3) {
                    updateProfile({ passSafetyWizard: true });
                }
            } else {
                errorSafeMode();
            }
        } else {
            nextStep();
        }
    };

    render() {
        const { t, opened, step, value, close, error, setValue } = this.props;

        return (
            <Dialog open={opened} onClose={closeSafeMode} aria-labelledby={t('title')}>
                <DialogTitle>{t('title')}</DialogTitle>
                <DialogContent>
                    {error && (
                        <DialogContentText>
                            {t('error')}
                            <br />
                            <br />
                        </DialogContentText>
                    )}
                    {step === 0 && (
                        <div>
                            <DialogContentText>{t('begin')}</DialogContentText>
                        </div>
                    )}
                    {step > 0 && step < 4 && (
                        <Step
                            t={t}
                            value={value}
                            setValue={setValue}
                            question={t(`q${step}`)}
                            o1={t(`q${step}o1`)}
                            o2={t(`q${step}o2`)}
                        />
                    )}
                    {step === 4 && (
                        <div>
                            <DialogContentText>{t('success')}</DialogContentText>
                        </div>
                    )}
                </DialogContent>

                <DialogActions>
                    {step < 4 && (
                        <Button onClick={this.nextStep} color="primary" disabled={step > 0 && value === null}>
                            {step === 0 ? t('startTest') : t('nextQuestion')}
                        </Button>
                    )}
                    <Button onClick={close} color="primary" autoFocus>
                        {t('close')}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

const mapStateToProps = ({ safemode }) => ({ ...safemode });
const mapDispatchToProps = {
    close: closeSafeMode,
    nextStep: nextStepSafeMode,
    setValue: setValueSafeMode,
    errorSafeMode,
    updateProfile
};

export default compose(
    withNamespaces('safemode'),
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(SafeModeWizard);

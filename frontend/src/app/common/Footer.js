// -*- rjsx -*-
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, Grid } from '@material-ui/core';
import { withNamespaces } from 'react-i18next';

import { STATIC_URL } from '../../config';

const styles = theme => ({
    root: {
        backgroundColor: theme.palette.primary.main,
        fontFamily: theme.typography.fontFamily,
        fontSize: theme.typography.fontSize,
        color: 'white',
        letterSpacing: '0.85px',
        margin: '0 auto'
    },

    wrap: {
        width: '100%',
        maxWidth: '1200px',
        margin: '0 auto'
    },

    liBody: {
        fontSize: '11px',
        listStyleType: 'none',
        //margin: '8px'
        marginTop: '8px'
    },

    liHeader: {
        marginTop: '25px',
        listStyleType: 'none',
        //margin: '9px',
        //fontSize: '16px',
        fontSize: '13px',
        fontWeight: 'bold'
    },

    bitzlato: {
        marginTop: '25px',
        fontSize: '10px'
    },

    links: {
        color: 'white',
        visited: 'white',
        active: 'white',
        textDecoration: 'none',
        borderBottom: '1px solid white'
    },

    socialPics: {
        fill: '#ffffff',
        margin: '3px'
    }
});

function renderUl(data, head, styleHead, styleBody, styleLink) {
    return (
        <ol>
            <li className={styleHead}>{head}</li>
            {data.map(res => {
                if (res.link !== '') {
                    return (
                        <li className={styleBody} key={res.text}>
                            <a className={styleLink} href={res.link}>
                                {res.text}
                            </a>
                        </li>
                    );
                } else {
                    return (
                        <li className={styleBody} key={res.text}>
                            <label>{res.text}</label>
                        </li>
                    );
                }
            })}
        </ol>
    );
}

const Footer = props => {
    const { classes, t } = props;

    const products = [
        { text: t('P2PExch'), link: 'https://bitzlato.com/ru/p2p' },
        { text: t('Exch'), link: 'https://bitzlato.com/ru/market' },
        { text: t('Wallet'), link: 'https://bitzlato.com/ru/wallet' },
        { text: t('Merchant'), link: 'https://bitzlato.com/ru/merchant' }
    ];

    const company = [
        { text: t('ACompany'), link: 'https://bitzlato.com/ru/about_company' },
        { text: t('Blog'), link: 'https://bitzlato.com/ru/blog' },
        { text: t('RefProgram'), link: 'https://bitzlato.com/ru/referral_program' },
        { text: t('Commissions'), link: 'https://bitzlato.com/ru/fees' },
        { text: t('TermsOfUse'), link: 'https://bitzlato.com/en/terms' },
        { text: t('Security'), link: 'https://bitzlato.com/ru/security' }
    ];

    const support = [
        { text: t('HowToSelfSafe'), link: 'https://bitzlato.com/ru/blog/how_to_protect_yourself' },
        { text: t('Forum'), link: 'https://talk.bitzlato.com/' },
        { text: t('AccountVerification'), link: 'https://check.changebot.org/' },
        { text: t('SupportTelegram'), link: 'https://t.me/CBHelpBot' },
        { text: t('Support'), link: 'https://changebot.freshdesk.com/' }
    ];

    return (
        <div className={classes.root}>
            <Grid container spacing={16} /*justify="space-around"*/ justify="space-between" className={classes.wrap}>
                <Grid item className={classes.bitzlato}>
                    <img alt="" src={`${STATIC_URL}/logo.png`} /*width="120"*/ height="32" /> <br />
                    <br />
                    <a href="https://www.facebook.com/bitzlato.ru/" target="_blank" rel="noopener noreferrer">
                        <svg className={classes.socialPics} width="25px" height="25px" viewBox="0 0 48 48">
                            <desc>Facebook</desc>
                            <path d="M47.761,24c0,13.121-10.638,23.76-23.758,23.76C10.877,47.76,0.239,37.121,0.239,24c0-13.124,10.638-23.76,23.764-23.76C37.123,0.24,47.761,10.876,47.761,24 M20.033,38.85H26.2V24.01h4.163l0.539-5.242H26.2v-3.083c0-1.156,0.769-1.427,1.308-1.427h3.318V9.168L26.258,9.15c-5.072,0-6.225,3.796-6.225,6.224v3.394H17.1v5.242h2.933V38.85z" />
                        </svg>
                    </a>
                    <a href="https://twitter.com/bitzlato_ru" target="_blank" rel="noopener noreferrer">
                        <svg className={classes.socialPics} width="25px" height="25px" viewBox="0 0 48 48">
                            <desc>Twitter</desc>
                            <path d="M47.762,24c0,13.121-10.639,23.76-23.761,23.76S0.24,37.121,0.24,24c0-13.124,10.639-23.76,23.761-23.76 S47.762,10.876,47.762,24 M38.031,12.375c-1.177,0.7-2.481,1.208-3.87,1.481c-1.11-1.186-2.694-1.926-4.455-1.926 c-3.364,0-6.093,2.729-6.093,6.095c0,0.478,0.054,0.941,0.156,1.388c-5.063-0.255-9.554-2.68-12.559-6.367 c-0.524,0.898-0.825,1.947-0.825,3.064c0,2.113,1.076,3.978,2.711,5.07c-0.998-0.031-1.939-0.306-2.761-0.762v0.077 c0,2.951,2.1,5.414,4.889,5.975c-0.512,0.14-1.05,0.215-1.606,0.215c-0.393,0-0.775-0.039-1.146-0.109 c0.777,2.42,3.026,4.182,5.692,4.232c-2.086,1.634-4.712,2.607-7.567,2.607c-0.492,0-0.977-0.027-1.453-0.084 c2.696,1.729,5.899,2.736,9.34,2.736c11.209,0,17.337-9.283,17.337-17.337c0-0.263-0.004-0.527-0.017-0.789 c1.19-0.858,2.224-1.932,3.039-3.152c-1.091,0.485-2.266,0.811-3.498,0.958C36.609,14.994,37.576,13.8,38.031,12.375" />
                        </svg>
                    </a>
                    <a href="https://vk.com/bitzlato" target="_blank" rel="noopener noreferrer">
                        <svg className={classes.socialPics} width="25px" height="25px" viewBox="0 0 48 48">
                            <desc>VK</desc>
                            <path d="M47.761,24c0,13.121-10.639,23.76-23.76,23.76C10.878,47.76,0.239,37.121,0.239,24c0-13.123,10.639-23.76,23.762-23.76C37.122,0.24,47.761,10.877,47.761,24 M35.259,28.999c-2.621-2.433-2.271-2.041,0.89-6.25c1.923-2.562,2.696-4.126,2.45-4.796c-0.227-0.639-1.64-0.469-1.64-0.469l-4.71,0.029c0,0-0.351-0.048-0.609,0.106c-0.249,0.151-0.414,0.505-0.414,0.505s-0.742,1.982-1.734,3.669c-2.094,3.559-2.935,3.747-3.277,3.524c-0.796-0.516-0.597-2.068-0.597-3.171c0-3.449,0.522-4.887-1.02-5.259c-0.511-0.124-0.887-0.205-2.195-0.219c-1.678-0.016-3.101,0.007-3.904,0.398c-0.536,0.263-0.949,0.847-0.697,0.88c0.31,0.041,1.016,0.192,1.388,0.699c0.484,0.656,0.464,2.131,0.464,2.131s0.282,4.056-0.646,4.561c-0.632,0.347-1.503-0.36-3.37-3.588c-0.958-1.652-1.68-3.481-1.68-3.481s-0.14-0.344-0.392-0.527c-0.299-0.222-0.722-0.298-0.722-0.298l-4.469,0.018c0,0-0.674-0.003-0.919,0.289c-0.219,0.259-0.018,0.752-0.018,0.752s3.499,8.104,7.573,12.23c3.638,3.784,7.764,3.36,7.764,3.36h1.867c0,0,0.566,0.113,0.854-0.189c0.265-0.288,0.256-0.646,0.256-0.646s-0.034-2.512,1.129-2.883c1.15-0.36,2.624,2.429,4.188,3.497c1.182,0.812,2.079,0.633,2.079,0.633l4.181-0.056c0,0,2.186-0.136,1.149-1.858C38.281,32.451,37.763,31.321,35.259,28.999" />
                        </svg>
                    </a>
                    <a href="https://www.instagram.com/bitzlato.ru" target="_blank" rel="noopener noreferrer">
                        <svg className={classes.socialPics} width="25px" height="25px" viewBox="0 0 30 30">
                            <desc>Instagram</desc>
                            <path d="M15,11.014 C12.801,11.014 11.015,12.797 11.015,15 C11.015,17.202 12.802,18.987 15,18.987 C17.199,18.987 18.987,17.202 18.987,15 C18.987,12.797 17.199,11.014 15,11.014 L15,11.014 Z M15,17.606 C13.556,17.606 12.393,16.439 12.393,15 C12.393,13.561 13.556,12.394 15,12.394 C16.429,12.394 17.607,13.561 17.607,15 C17.607,16.439 16.444,17.606 15,17.606 L15,17.606 Z" />
                            <path d="M19.385,9.556 C18.872,9.556 18.465,9.964 18.465,10.477 C18.465,10.989 18.872,11.396 19.385,11.396 C19.898,11.396 20.306,10.989 20.306,10.477 C20.306,9.964 19.897,9.556 19.385,9.556 L19.385,9.556 Z" />
                            <path d="M15.002,0.15 C6.798,0.15 0.149,6.797 0.149,15 C0.149,23.201 6.798,29.85 15.002,29.85 C23.201,29.85 29.852,23.202 29.852,15 C29.852,6.797 23.201,0.15 15.002,0.15 L15.002,0.15 Z M22.666,18.265 C22.666,20.688 20.687,22.666 18.25,22.666 L11.75,22.666 C9.312,22.666 7.333,20.687 7.333,18.28 L7.333,11.734 C7.333,9.312 9.311,7.334 11.75,7.334 L18.25,7.334 C20.688,7.334 22.666,9.312 22.666,11.734 L22.666,18.265 L22.666,18.265 Z" />
                        </svg>
                    </a>
                    <a href="https://t.me/changebot_news" target="_blank" rel="noopener noreferrer">
                        <svg className={classes.socialPics} width="25px" height="25px" viewBox="0 0 60 60">
                            <desc>Telegram</desc>
                            <path d="M30 0C13.4 0 0 13.4 0 30s13.4 30 30 30 30-13.4 30-30S46.6 0 30 0zm16.9 13.9l-6.7 31.5c-.1.6-.8.9-1.4.6l-10.3-6.9-5.5 5.2c-.5.4-1.2.2-1.4-.4L18 32.7l-9.5-3.9c-.7-.3-.7-1.5 0-1.8l37.1-14.1c.7-.2 1.4.3 1.3 1z" />
                            <path d="M22.7 40.6l.6-5.8 16.8-16.3-20.2 13.3" />
                        </svg>
                    </a>
                    <br />
                    <br />
                    ©&nbsp;
                    {new Date().getFullYear()}
                    &nbsp;Bitzlato.com
                </Grid>

                <Grid item>{renderUl(products, t('Products'), classes.liHeader, classes.liBody, classes.links)}</Grid>

                <Grid item>{renderUl(company, t('Company'), classes.liHeader, classes.liBody, classes.links)}</Grid>

                <Grid item>
                    {renderUl(support, t('HelpAndSupport'), classes.liHeader, classes.liBody, classes.links)}
                </Grid>
            </Grid>
        </div>
    );
};

Footer.propTypes = {
    t: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired
};

export default withNamespaces('footer')(withStyles(styles)(Footer));

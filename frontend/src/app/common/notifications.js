import { P2P_URL, WALLETS_URL, PROFILE_URL } from '../../config';

export function notificationInfo(item, { userId, t }) {
    switch (item.name) {
        case 'tradeStatusChanged':
            return {
                text: t('tradeStatusChanged', item.data),
                link: `${P2P_URL}/trades/${item.data.tradeId}`
            };
        case 'tradeExtendWaitingTime':
            return {
                text: t('tradeExtendWaitingTime', {
                    tradeId: item.data.tradeId,
                    time: item.data.time
                }),
                link: `${P2P_URL}/trades/${item.data.tradeId}`
            };
        case 'tradeWillExpire':
            return {
                text: t('tradeWillExpire', item.data),
                link: `${P2P_URL}/trades/${item.data.tradeId}`
            };
        case 'disputeResolved':
            return {
                text: t(item.data.status === 'win' ? 'disputeSuccess' : 'disputeFail', {
                    tradeId: item.data.tradeId,
                    amount: item.data.cryptocurrency.amount,
                    cryptocurrency: item.data.cryptocurrency.code
                }),
                link: `${P2P_URL}/trades/${item.data.tradeId}`
            };
        case 'disputeAvailable':
            return {
                text: t('disputeAvailable'),
                link: `${P2P_URL}/trades/${item.data.tradeId}`
            };
        case 'userMessage':
            return {
                text: t('newMessage'),
                alert: item.data.message
            };
        case 'newChatMessage':
            if (item.data.tradeId) {
                return {
                    text: t('newTradeMessage', {
                        tradeId: item.data.tradeId
                    }),
                    link: `${P2P_URL}/trades/${item.data.tradeId}`
                };
            } else {
                return {
                    text: t('newChatMessage', {
                        publicName: item.data.from
                    }),
                    link: `${P2P_URL}/users/${item.data.from}`
                };
            }
        case 'checkCashed':
            return {
                text: t('checkCashed', item.data),
                link: `${WALLETS_URL}/${item.data.cryptocurrency}`
            };
        case 'moneyReceived':
            return {
                text: t('moneyReceived', item.data),
                link: `${WALLETS_URL}/${item.data.cryptocurrency.code}`
            };
        case 'userTradeStatusChanged':
            return {
                text: t('tradePause'),
                link: `${P2P_URL}/adverts/`
            };
        case 'newReferral':
            return {
                text: t('newReferral', {
                    publicName: item.data.refferal
                }),
                link: `${P2P_URL}/users/${item.data.publicName}`
            };
        case 'dividendsReceived':
            return {
                text: t('dividendsReceived', item.data),
                link: `${WALLETS_URL}/${item.data.cryptocurrency.code}`
            };
        case 'stockExchangeEvent':
            if (item.data.type !== 'wallet-balance-changed') {
                return {
                    text: t(item.data.type, item.data.eventData),
                    link: `${WALLETS_URL}/${item.data.eventData.currency}`
                };
            } else {
                return {};
            }
        case 'accountsMerged':
            return {
                text: t('Telegram and web accounts have been merged'),
                link: `${PROFILE_URL}/telegram`
            };
        case 'blockChainMoneyReceived':
            return {
                text: t('wallet-balance-loaded', item.data),
                link: `${WALLETS_URL}/${item.data.cryptocurrency}`
            };
        case 'blockChainMoneySent':
            return {
                text: t('payment-processed', item.data),
                link: `${WALLETS_URL}/${item.data.cryptocurrency}`
            };
        case 'verificationDecision':
            if (item.data.status) {
                return {
                    text: t('verification-confirmed'),
                    link: `${PROFILE_URL}`
                };
            } else {
                return {
                    text: t('verification-rejected'),
                    alert: item.data.reason
                };
            }
        case 'mute':
            return {
                text: t('mute', { duration: parseInt(item.data.duration) }),
                alert: t('muteReason', { reason: item.data.reason })
            };
        default:
            return {};
    }
}

// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Field, reduxForm, Form, reset, SubmissionError } from 'redux-form';
import Helmet from 'react-helmet';
import { Button, Grid, Paper, Typography, withStyles, Table, TableBody, TableRow, TableCell } from '@material-ui/core';
import WAValidator from 'wallet-address-validator';
import cashaddr from 'cashaddrjs';
import { BackendClient } from '../../redux/actions/api';
import { renderTextField } from '../common/redux-form-helpers';

import { loadWithdrawInfo } from '../../redux';

const styles = () => ({
    warn: {
        marginTop: '40px',
        padding: '15px',
        backgroundColor: '#FFF8E1'
    }
});

class Withdraw extends Component {
    constructor() {
        super();

        this.state = {
            withdrawal: null
        };
    }

    static propTypes = {
        generateWalletAddress: PropTypes.func,
        loadWithdrawInfo: PropTypes.func,
        handleSubmit: PropTypes.func,
        reset: PropTypes.func,
        classes: PropTypes.object,
        t: PropTypes.func,
        wallet: PropTypes.object,
        withdrawalRates: PropTypes.func,
        withdraw: PropTypes.func,
        cryptocurrency: PropTypes.string,
        info: PropTypes.object,
        auth: PropTypes.object
    };

    componentDidMount() {
        this.props.loadWithdrawInfo();
    }

    onSubmit = values => {
        const errors = validate(values, this.props);

        if (_.isEmpty(errors)) {
            this.updateRates(values);
        } else {
            this.setState({
                withdrawal: null,
                rates: null
            });

            throw new SubmissionError(errors);
        }
    };

    updateRates = _.debounce(async ({ addressCrypto, amount }) => {
        const apiClient = new BackendClient(this.props.auth.userId, this.props.auth.idToken, '');
        const rates = await apiClient.withdrawalRates(this.props.cryptocurrency, addressCrypto, amount);

        this.setState({
            withdrawal: {
                address: addressCrypto,
                amount: amount
            },
            rates: rates
        });
    }, 300);

    withdraw = async rate => {
        const { withdrawal } = this.state;

        await this.props.withdraw(withdrawal.address, withdrawal.amount, rate);

        this.setState({
            withdrawal: null
        });

        //clear form
        this.props.reset();
    };

    render() {
        const { t, handleSubmit, cryptocurrency, classes, info } = this.props;
        const rates = this.state.rates;

        const rateButton = {
            marginRight: 16
        };

        return (
            <Grid container spacing={24}>
                <Helmet>
                    <title>{t('withdrawTitle', { cryptocurrency })}</title>
                </Helmet>

                <Grid item xs={12}>
                    <Typography variant="h6">
                        <span role="img" aria-label="withdraw">
                            📥
                        </span>{' '}
                        {t('withdrawTitle', { cryptocurrency })}
                    </Typography>
                </Grid>

                {!_.isEmpty(info) && (
                    <Grid item xs={12}>
                        <Table>
                            <TableBody>
                                <TableRow>
                                    <TableCell>{t('Min. value')}</TableCell>
                                    <TableCell>
                                        {info.min} {cryptocurrency}
                                    </TableCell>
                                </TableRow>

                                <TableRow>
                                    <TableCell>{t('Balance')}</TableCell>
                                    <TableCell>
                                        {info.balance} {cryptocurrency}
                                    </TableCell>
                                </TableRow>

                                <TableRow>
                                    <TableCell>{t('Available for withdrawal')}</TableCell>
                                    <TableCell>
                                        {info.available} {cryptocurrency}
                                    </TableCell>
                                </TableRow>

                                <TableRow>
                                    <TableCell>{t('Fee')}</TableCell>
                                    <TableCell>
                                        {info.fee} {cryptocurrency}
                                    </TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </Grid>
                )}

                <Grid item xs={12}>
                    <Typography>{t('withdrawText1')}</Typography>
                </Grid>
                <Grid item xs={12}>
                    <Typography>{t('withdrawText2')}</Typography>
                </Grid>

                <Grid item xs={12}>
                    <Form onSubmit={handleSubmit(this.onSubmit)}>
                        <Grid container spacing={24}>
                            <Grid item xs={12} sm={6}>
                                <Field
                                    name="addressCrypto"
                                    label="Адрес"
                                    placeholder="Адрес"
                                    component={renderTextField}
                                    fullWidth
                                />
                            </Grid>

                            <Grid item xs={12} sm={6}>
                                <Field
                                    name="amount"
                                    label="Сумма"
                                    placeholder="Сумма"
                                    component={renderTextField}
                                    fullWidth
                                />
                            </Grid>
                        </Grid>
                    </Form>
                </Grid>

                {Boolean(rates) && (
                    <Grid item xs={12}>
                        {_.map(rates, rate => (
                            <Button
                                variant="contained"
                                style={rateButton}
                                onClick={() => this.withdraw(rate.code)}
                                color="primary"
                                key={rate.code}
                            >
                                {rate.code}
                            </Button>
                        ))}
                    </Grid>
                )}

                <Grid item xs={12}>
                    <Paper className={classes.warn}>
                        <Typography variant="subtitle1">
                            <span role="img" aria-label="">
                                ⚠️
                            </span>{' '}
                            Предупреждение
                        </Typography>

                        <p>
                            Любой «агент поддержки» требующий отправить монеты является самозванцем! Единственная служба
                            поддержки доступна по ссылке{' '}
                            <a href="https://changebot.freshdesk.com/support/home">
                                https://changebot.freshdesk.com/support/home
                            </a>
                            &nbsp; или через телеграм-бот <a href="https://t.me/CBHelpBot">https://t.me/CBHelpBot</a>
                        </p>
                    </Paper>
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = (state, props) => {
    return {
        info: _.get(state, ['wallets', props.cryptocurrency, 'info']),
        auth: _.pick(state.auth, 'userId', 'idToken')
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        reset: () => dispatch(reset('withdrawWalletForm')),
        loadWithdrawInfo: () => dispatch(loadWithdrawInfo(props.cryptocurrency))
    };
};

const validate = (values, { t, cryptocurrency }) => {
    const errors = {};

    if (!values.addressCrypto) {
        errors.addressCrypto = t('Required');
    } else {
        if (cryptocurrency === 'BCH') {
            try {
                if (cashaddr.decode(values.addressCrypto).prefix !== 'bitcoincash') {
                    errors.addressCrypto = t('Invalid address');
                }
            } catch (err) {
                errors.addressCrypto = t('Invalid address');
            }
        } else if (!WAValidator.validate(values.addressCrypto, cryptocurrency)) {
            errors.addressCrypto = t('Invalid address');
        }
    }

    const amount = _.toNumber(values.amount);
    if (!_.isFinite(amount) || amount <= 0) {
        errors.amount = t('Invalid value');
    }

    return errors;
};

export default compose(
    withNamespaces('wallets'),
    withStyles(styles),
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    reduxForm({
        form: 'withdrawWalletForm',
        onChange: (values, dispatch, props) => {
            props.submit();
        }
    })
)(Withdraw);

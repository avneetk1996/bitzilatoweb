// -*- rjsx -*-
import React from 'react';
import PropTypes from 'prop-types';
import { Button, Grid, Paper, Typography, withStyles } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import copy from 'copy-to-clipboard';

const styles = () => ({
    table: {
        minWidth: 500
    },
    tableWrapper: {
        overflowX: 'auto'
    }
});

const VoucherInfo = props => {
    const { voucher, classes } = props;
    const paperStyle = { padding_bottom: 40 };
    const secondGridStyle = { padding: 20 };

    return (
        <Paper style={paperStyle}>
            <Grid container style={secondGridStyle}>
                <Typography variant="h6">Voucher links</Typography>

                <Grid item xs={12} sm={12}>
                    <div className={classes.tableWrapper}>
                        <Table>
                            <TableBody>
                                {voucher.links.map(i => {
                                    return (
                                        <TableRow key={i.url}>
                                            <TableCell>{i.type}</TableCell>
                                            <TableCell>
                                                {
                                                    // <a href={`${i.url}`}>{i.url}</a>
                                                }
                                                <Button
                                                    color="primary"
                                                    style={{ float: 'left' }}
                                                    onClick={() => copy(i.url)}
                                                >
                                                    copy
                                                </Button>
                                            </TableCell>
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                        </Table>
                    </div>
                </Grid>
            </Grid>
        </Paper>
    );
};

VoucherInfo.propTypes = {
    voucher: PropTypes.object,
    classes: PropTypes.object
};

export default withStyles(styles)(VoucherInfo);

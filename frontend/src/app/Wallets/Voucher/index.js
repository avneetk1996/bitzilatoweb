// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Field, reduxForm, Form } from 'redux-form';
import Helmet from 'react-helmet';
import { Button, Grid, Paper, Typography } from '@material-ui/core';
import copy from 'copy-to-clipboard';
import { renderSelectField, renderTextField } from '../../common/redux-form-helpers';
import VoucherGrid from './VoucherGrid';
import VoucherInfo from './VoucherInfo';
import { createVoucher, loadVoucherList, cancelVoucher } from '../../../redux';

class Voucher extends Component {
    constructor() {
        super();

        this.state = {
            selectedVoucherId: null,
            selectedVoucher: null
        };
    }

    static propTypes = {
        reset: PropTypes.func,
        handleSubmit: PropTypes.func,

        currency: PropTypes.string,
        cryptocurrency: PropTypes.string,
        voucherList: PropTypes.array,

        loadVoucherList: PropTypes.func,
        cancelVoucher: PropTypes.func,
        createVoucher: PropTypes.func
    };

    componentDidMount() {
        this.props.loadVoucherList();
    }

    cancelVoucher = code => {
        this.props.cancelVoucher(code);
    };

    onSubmit = values => {
        const { reset } = this.props;
        const method = values.currency === this.props.currency ? 'fiat' : 'crypto';
        this.props.createVoucher(values.amount, this.props.cryptocurrency, this.props.currency, method);
        //let f
        //f = document.getElementById("VoucherForm");
        reset();
        //f[1].value = ""
    };

    selectVoucher = voucher => {
        this.setState({
            selectedVoucher: voucher
        });
    };

    render() {
        const { handleSubmit, voucherList } = this.props;
        const voucherListData = voucherList;
        const cheque = { DOGE: 'бла бла бла текст нового чека вуху' };
        const cryptocurrency = 'BTC';
        const { selectedVoucher } = this.state;

        const paperStyle = {
            paddingLeft: 20,
            height: 36,
            marginTop: 40
        };

        const vert = {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center'
        };

        return (
            <div>
                <Helmet>
                    <title>Выписать чек</title>
                </Helmet>

                <Typography variant="h6">Вы можете выписать чек в указанной валюте или криптовалюте</Typography>
                <br />
                <br />

                <Form id="VoucherForm" onSubmit={handleSubmit(this.onSubmit)}>
                    <Grid container spacing={24}>
                        <Grid item xs={12} sm={4} style={vert}>
                            <Field
                                name="amount"
                                style={{ marginTop: 7 }}
                                label="Amount"
                                placeholder="Amount"
                                component={renderTextField}
                                fullWidth
                            />
                        </Grid>

                        <Grid item xs={12} sm={4} style={vert}>
                            <Field
                                select
                                name="currency"
                                label="Currency"
                                placeholder="Currency"
                                component={renderSelectField}
                                options={[
                                    {
                                        value: this.props.currency,
                                        label: this.props.currency
                                    },
                                    {
                                        value: this.props.cryptocurrency,
                                        label: this.props.cryptocurrency
                                    }
                                ]}
                                fullWidth
                            />
                        </Grid>

                        <Grid item xs={12} sm={4} style={vert}>
                            <Button color="primary" variant="contained" fullWidth type="submit">
                                Выписать чек
                            </Button>
                        </Grid>
                    </Grid>
                </Form>

                {cheque &&
                    cheque[cryptocurrency] && (
                        <Paper style={paperStyle}>
                            <span style={{ lineHeight: '36px' }}>{cheque[cryptocurrency]}</span>

                            <Button
                                color="primary"
                                style={{ float: 'right' }}
                                onClick={() => copy(cheque[cryptocurrency])}
                            >
                                copy
                            </Button>
                        </Paper>
                    )}

                <VoucherGrid
                    selectVoucher={this.selectVoucher}
                    data={voucherListData}
                    cancelVoucher={this.cancelVoucher}
                />

                {Boolean(selectedVoucher) && (
                    <div>
                        <br />
                        <br />
                        <VoucherInfo voucher={selectedVoucher} />
                    </div>
                )}
            </div>
        );
    }
}

const mapStateToProps = (state, props) => {
    const currency = _.get(state, 'profile.settings.currency');
    const cryptocurrency = props.match.params.cryptocurrency;
    const voucherList = _.get(state, `wallets.${cryptocurrency}.vouchers.items.data`);
    return {
        currency: currency,
        cryptocurrency: cryptocurrency,
        voucherList: voucherList
    };
};

const mapDispatchToProps = (dispatch, props) => {
    const cryptocurrency = props.cryptocurrency;
    return {
        loadVoucherList: () => dispatch(loadVoucherList(cryptocurrency)),
        cancelVoucher: code => dispatch(cancelVoucher(code)),
        createVoucher: (amount, cryptocurrency, currency, method) =>
            dispatch(createVoucher(amount, cryptocurrency, currency, method))
    };
};

export default compose(
    withNamespaces('profile'),
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    reduxForm({
        form: 'voucherForm',
        validate: (values, { t }) => {
            const errors = {};

            const amount = _.toNumber(values.amount);
            if (!_.isFinite(amount) || amount <= 0) {
                errors.amount = t('Invalid value');
            }

            if (_.isEmpty(values.currency)) {
                errors.currency = t('Required');
            }

            return errors;
        }
    })
)(Voucher);

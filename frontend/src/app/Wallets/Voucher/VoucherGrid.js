// -*- rjsx -*-
import { connect } from 'react-redux';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    withStyles,
    Typography,
    Paper,
    Button,
    Table,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    TableFooter,
    TablePagination
} from '@material-ui/core';
import { withNamespaces } from 'react-i18next';
import { withRouter } from 'react-router-dom';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3
    },
    table: {
        minWidth: 500
    },
    tableWrapper: {
        overflowX: 'auto'
    }
});

class VoucherGrid extends Component {
    constructor(props) {
        super(props);

        this.state = {
            commentBoxOpened: false,
            commentBoxId: null,
            commentBoxMessage: null,
            rowsPerPage: 5,
            page: 0
        };
    }

    static propTypes = {
        t: PropTypes.func,
        data: PropTypes.array,
        classes: PropTypes.object.isRequired,
        cancelVoucher: PropTypes.func,
        selectVoucher: PropTypes.func
    };

    handleChangePage = (event, page) => {
        this.setState({ page });
    };

    handleChangeRowsPerPage = event => {
        const rowsPerPage = event.target.value;
        this.setState({ rowsPerPage });
    };

    handleClickOnRow = id => {
        this.props.selectVoucher(id);
    };

    render() {
        const { page, rowsPerPage } = this.state;

        const { classes, t } = this.props;
        const total = this.props.data && this.props.data.length;
        const emptyRows = rowsPerPage - Math.min(rowsPerPage, total - page * rowsPerPage);

        const rowStyle = {
            cursor: 'pointer'
        };

        return (
            <Paper className={classes.root}>
                <div className={classes.tableWrapper}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell component="th">
                                    <Typography variant="subtitle2">{t('Created')}</Typography>
                                </TableCell>

                                <TableCell component="th">
                                    <Typography variant="subtitle2">{t('Amount')}</Typography>
                                </TableCell>

                                <TableCell component="th">
                                    <Typography variant="subtitle2">{t('Equivalent')}</Typography>
                                </TableCell>

                                <TableCell component="th" className={classes.column} />
                            </TableRow>
                        </TableHead>

                        <TableBody>
                            {Boolean(this.props.data) &&
                                this.props.data
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map(voucher => {
                                        return (
                                            <TableRow
                                                key={voucher.createdAt}
                                                style={rowStyle}
                                                className="advertGrid_tableRow"
                                                onClick={() => this.handleClickOnRow(voucher)}
                                            >
                                                <TableCell component="th" scope="row">
                                                    {t('createdTime', { time: voucher.createdAt })}
                                                </TableCell>

                                                <TableCell>
                                                    {voucher.cryptocurrency.amount} {voucher.cryptocurrency.code}
                                                </TableCell>

                                                <TableCell>
                                                    {voucher.currency.amount} {voucher.currency.code}
                                                </TableCell>

                                                <TableCell>
                                                    <Button
                                                        onClick={event => {
                                                            event.stopPropagation();
                                                            this.props.cancelVoucher(voucher.deepLinkCode);
                                                        }}
                                                        color="secondary"
                                                    >
                                                        close
                                                    </Button>
                                                </TableCell>
                                            </TableRow>
                                        );
                                    })}

                            {emptyRows > 0 && (
                                <TableRow style={{ height: 48 * emptyRows }}>
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>

                        <TableFooter>
                            <TableRow>
                                <TablePagination
                                    count={total || 0}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    rowsPerPageOptions={[5, 10, 25]}
                                    labelRowsPerPage={t('Rows per page')}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                />
                            </TableRow>
                        </TableFooter>
                    </Table>
                </div>
            </Paper>
        );
    }
}

const mapStateToProps = (state, props) => {
    const cryptocurrency = props.match.params.cryptocurrency;

    return {
        cryptocurrency: cryptocurrency,
        data: props.data
    };
};

export default withNamespaces('wallets')(withStyles(styles)(withRouter(connect(mapStateToProps)(VoucherGrid))));

// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import {
    withStyles,
    Paper,
    Table,
    TableHead,
    TableBody,
    TableCell,
    TableFooter,
    TablePagination,
    TableRow,
    Grid
} from '@material-ui/core';
import green from '@material-ui/core/colors/green';

import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';
import InsertComment from '@material-ui/icons/InsertComment';
import Prompt from '../common/Prompt';

import { loadTransactions, commentTransaction } from '../../redux';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3
    },
    table: {
        minWidth: 500
    },
    tableWrapper: {
        overflowX: 'auto'
    },
    receive: {
        color: green[600],
        fontWeight: 'bold'
    },
    withdrawal: {
        color: theme.palette.error.dark,
        fontWeight: 'bold'
    },
    commentIcon: {
        opacity: 0.3,
        cursor: 'pointer',
        '&:hover': {
            opacity: 0.7
        }
    }
});

class TransactionsGrid extends Component {
    constructor() {
        super();

        this.state = {
            commentBoxOpened: false,
            commentBoxId: null,
            commentBoxMessage: null,
            rowsPerPage: 5,
            page: 0
        };
    }

    static propTypes = {
        t: PropTypes.func.isRequired,
        classes: PropTypes.object.isRequired,

        filter: PropTypes.object,
        transactions: PropTypes.array,
        total: PropTypes.number,

        loadTransactions: PropTypes.func,
        commentTransaction: PropTypes.func
    };

    componentDidMount() {
        this.props.loadTransactions(this.state.rowsPerPage * this.state.page, this.state.rowsPerPage);
    }

    handleChangePage = (event, page) => {
        this.setState({ page });
        this.props.loadTransactions(this.state.rowsPerPage * page, this.state.rowsPerPage);
    };

    handleChangeRowsPerPage = event => {
        const rowsPerPage = event.target.value;
        this.setState({ rowsPerPage });
        this.props.loadTransactions(rowsPerPage * this.state.page, rowsPerPage);
    };

    openComment = (id, comment) => {
        this.setState({
            commentBoxOpened: true,
            commentBoxId: id,
            commentBoxMessage: comment
        });
    };

    closeComment = () => {
        this.setState({
            commentBoxOpened: false
        });
    };

    applyCommentChange = async comment => {
        this.props.commentTransaction(this.state.commentBoxId, comment);
        await this.closeComment();
    };

    render() {
        const { page, rowsPerPage, commentBoxOpened, commentBoxMessage } = this.state;

        const { classes, t, transactions, total } = this.props;
        const emptyRows = rowsPerPage - Math.min(rowsPerPage, total - page * rowsPerPage);

        return (
            <Paper className={classes.root}>
                <Helmet>
                    <title>{t('Транзакции')}</title>
                </Helmet>
                <Prompt
                    opened={commentBoxOpened}
                    title={t('Comment')}
                    message={t('Comment')}
                    defaultValue={commentBoxMessage}
                    closeDialog={this.closeComment}
                    handlePrompt={this.applyCommentChange}
                />
                {/* <CommentBox opened={commentBoxOpened} */}
                {/*             message={commentBoxMessage} */}
                {/*             initialValues={{ comment: commentBoxMessage }} */}
                {/*             closeDialog={this.closeComment} */}
                {/*             onSubmit={this.applyCommentChange}/> */}

                <div className={classes.tableWrapper}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell>{_.upperFirst(t('address'))}</TableCell>
                                <TableCell>{_.upperFirst(t('amount'))}</TableCell>
                                <TableCell>{t('Comment')}</TableCell>
                            </TableRow>
                        </TableHead>

                        <TableBody>
                            {transactions &&
                                transactions.map(transaction => {
                                    return (
                                        <TableRow key={transaction.id} className="advertGrid_tableRow">
                                            <TableCell>{transaction.address}</TableCell>

                                            <TableCell
                                                className={
                                                    transaction.type === 'withdrawal'
                                                        ? classes.withdrawal
                                                        : classes.receive
                                                }
                                            >
                                                {transaction.cryptocurrency.amount}
                                            </TableCell>

                                            <TableCell>
                                                <Grid container>
                                                    <Grid item xs={8}>
                                                        {transaction.comment}
                                                    </Grid>
                                                    <Grid item xs={1} />
                                                    <Grid item xs={3}>
                                                        <InsertComment
                                                            onClick={() =>
                                                                this.openComment(transaction.id, transaction.comment)
                                                            }
                                                            className={classes.commentIcon}
                                                        />
                                                    </Grid>
                                                </Grid>
                                            </TableCell>
                                        </TableRow>
                                    );
                                })}

                            {emptyRows > 0 && (
                                <TableRow style={{ height: 48 * emptyRows }}>
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>

                        <TableFooter>
                            <TableRow>
                                {// preventing listing to 0 position on initialization
                                (total &&
                                    total > 0 && (
                                        <TablePagination
                                            count={total}
                                            rowsPerPage={rowsPerPage}
                                            page={page}
                                            rowsPerPageOptions={[5, 10, 25]}
                                            labelRowsPerPage={t('Rows per page')}
                                            onChangePage={this.handleChangePage}
                                            onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                        />
                                    )) ||
                                    null}
                            </TableRow>
                        </TableFooter>
                    </Table>
                </div>
            </Paper>
        );
    }
}

function mapStateToProps(state, props) {
    const cryptocurrency = props.cryptocurrency,
        data = _.get(state.wallets, [cryptocurrency, 'transactions'], {});

    return {
        filter: data.filter,
        transactions: data.items,
        total: data.total
    };
}

function mapDispatchToProps(dispatch, props) {
    const cryptocurrency = props.cryptocurrency;

    return {
        loadTransactions: (skip, limit) => dispatch(loadTransactions(cryptocurrency, skip, limit)),
        commentTransaction: (id, comment) => dispatch(commentTransaction(id, comment))
    };
}

export default withNamespaces('wallets')(
    withStyles(styles)(
        connect(
            mapStateToProps,
            mapDispatchToProps
        )(TransactionsGrid)
    )
);

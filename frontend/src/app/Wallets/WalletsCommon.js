// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import { connect } from 'react-redux';
import { Grid, Typography, withStyles, Table, TableHead, TableBody, TableRow, TableCell } from '@material-ui/core';

import { loadAllWalletsInfo } from '../../redux';

const styles = theme => ({
    root: {
        padding: 40
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default
        }
    },
    cell: {
        [theme.breakpoints.down('sm')]: {
            fontSize: '80%',
            padding: '5px'
        }
    },
    column: {
        color: '#333',
        fontWeight: 'bold',
        [theme.breakpoints.down('sm')]: {
            fontSize: '80%',
            padding: '5px'
        }
    },
    tableWrapper: {
        overflowX: 'auto'
    }
});

class WalletsCommon extends Component {
    static propTypes = {
        t: PropTypes.func,
        classes: PropTypes.object,

        loadAllWalletsInfo: PropTypes.func,
        wallets: PropTypes.object,
        currency: PropTypes.string
    };

    componentDidMount() {
        this.props.loadAllWalletsInfo(_.keys(this.props.wallets), this.props.currency);
    }

    componentDidUpdate() {
        if (_.some(this.props.wallets, wallet => _.isEmpty(wallet))) {
            this.props.loadAllWalletsInfo(_.keys(this.props.wallets), this.props.currency);
        }
    }

    render() {
        const { classes, t, wallets, currency } = this.props;

        return (
            <Grid item xs={12} className={classes.tableWrapper}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell component="th" className={classes.column}>
                                <Typography variant="subtitle2">{t('Balance')}</Typography>
                            </TableCell>

                            <TableCell component="th" className={classes.column}>
                                <Typography variant="subtitle2">{t('Equivalent')}</Typography>
                            </TableCell>

                            <TableCell component="th" className={classes.column}>
                                <Typography variant="subtitle2">{t('Blocked')}</Typography>
                            </TableCell>
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {_.map(wallets, (wallet, cryptocurrency) => (
                            <TableRow className={classes.row} key={cryptocurrency}>
                                <TableCell className={classes.cell}>
                                    {wallet.balance} {cryptocurrency}
                                </TableCell>

                                <TableCell className={classes.cell}>
                                    {_.get(wallet, ['worth', currency, 'value'])} {currency}
                                </TableCell>

                                <TableCell className={classes.cell}>
                                    {wallet.holdBalance} {cryptocurrency}
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Grid>
        );
    }
}

const mapStateToProps = state => {
    return {
        wallets: state.wallets,
        currency: _.get(state, 'profile.settings.currency')
    };
};

const mapDispatchToProps = dispatch => {
    return {
        loadAllWalletsInfo: (cryptocurrencies, currency) => dispatch(loadAllWalletsInfo(cryptocurrencies, currency))
    };
};

export default withNamespaces('wallets')(
    withStyles(styles)(
        connect(
            mapStateToProps,
            mapDispatchToProps
        )(WalletsCommon)
    )
);

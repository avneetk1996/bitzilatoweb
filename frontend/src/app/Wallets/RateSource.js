// -*- rjsx -*-
import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';
import { Select, MenuItem, Typography, Grid } from '@material-ui/core';
import Markdown from 'react-remarkable';

import { setRateSource, loadRateSourcesRef } from '../../redux/actions';

class RateSource extends React.Component {
    static propTypes = {
        t: PropTypes.func,

        currency: PropTypes.string,
        cryptocurrency: PropTypes.string,
        rateSource: PropTypes.object,
        allRateSources: PropTypes.array,

        loadRateSourcesRef: PropTypes.func.isRequired,
        setRateSource: PropTypes.func.isRequired
    };

    componentDidMount() {
        if (this.props.currency && this.props.cryptocurrency) {
            this.props.loadRateSourcesRef();
        }
    }

    componentDidUpdate(oldProps) {
        if (this.props.currency && this.props.cryptocurrency) {
            if (this.props.currency !== oldProps.currency || this.props.cryptocurrency !== oldProps.cryptocurrency) {
                //this.props.loadRateSourcesRef();
            }
        }
    }

    setRateSource = event => {
        this.props.setRateSource(event.target.value);
    };

    render() {
        const { rateSource, allRateSources, currency, cryptocurrency, t } = this.props;

        if (!allRateSources) {
            return null;
        }

        return (
            <Grid container direction="column" spacing={24}>
                <Helmet>
                    <title>{t('rateTitle', { currency, cryptocurrency })}</title>
                </Helmet>

                <Grid item>
                    <Typography variant="h6">
                        <span role="img" aria-label="">
                            📊
                        </span>{' '}
                        {t('rateTitle', { currency, cryptocurrency })}
                    </Typography>
                </Grid>

                <Grid item>
                    <Markdown>{t('rateText', { currency, cryptocurrency })}</Markdown>
                </Grid>

                <Grid item>
                    <Select value={_.get(rateSource, 'url', '')} onChange={this.setRateSource} fullWidth>
                        {_.map(allRateSources, item => (
                            <MenuItem value={item.url} key={item.url}>{`${item.description} (${
                                item.rate
                            } ${currency})`}</MenuItem>
                        ))}
                    </Select>
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = (state, props) => {
    const { currency, cryptocurrency } = props;

    return {
        rateSource: _.get(state, ['wallets', cryptocurrency, 'rateSource', currency]),
        allRateSources: currency && cryptocurrency && _.get(state.refs.rateSources, `${currency}-${cryptocurrency}`)
    };
};

const mapDispatchToProps = (dispatch, props) => {
    const { currency, cryptocurrency } = props;

    return {
        loadRateSourcesRef: () => dispatch(loadRateSourcesRef(currency, cryptocurrency)),
        setRateSource: url => dispatch(setRateSource(url, currency, cryptocurrency))
    };
};

export default withNamespaces('wallets')(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(RateSource)
);

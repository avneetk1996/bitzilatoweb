// -*- rjsx -*-
import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { Table, TableBody, TableCell, TableRow, withStyles, Grid } from '@material-ui/core';
import { withNamespaces } from 'react-i18next';

const style = theme => ({
    tableWrapper: {
        overflowX: 'auto'
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default
        }
    },
    cell: {
        [theme.breakpoints.down('sm')]: {
            fontSize: '80%',
            padding: '5px'
        }
    }
});

const WalletInfo = props => {
    const { wallet, currency, classes, t } = props;

    if (_.isEmpty(wallet)) {
        return null;
    }

    const worth = _.get(wallet.worth, currency);

    return (
        <Grid container>
            {/*
                Boolean(rateSources) &&
                    <div>
                        <Typography variant="subtitle2">
                            Set rate source
                            <br/>
                            <br/>
                            <Select value={''}
                                onChange={setRateSource}>
                                {
                                    rateSources.map((rateSource) => (
                                        <MenuItem value={rateSource.value}>
                                            {rateSource.label}
                                        </MenuItem>
                                    ))
                                }
                            </Select>
                        </Typography>
                    </div>
            */}

            <Grid item xs={12} md={6}>
                <Table>
                    <TableBody>
                        <TableRow className={classes.row}>
                            <TableCell className={classes.cell}>{t('Balance')}</TableCell>
                            <TableCell className={classes.cell} align="right">
                                {wallet.balance} {wallet.cryptocurrency}
                            </TableCell>
                        </TableRow>

                        {worth && (
                            <TableRow className={classes.row}>
                                <TableCell className={classes.cell}>{t('Equivalent')}</TableCell>
                                <TableCell className={classes.cell} align="right">
                                    {worth.value} {worth.currency}
                                </TableCell>
                            </TableRow>
                        )}

                        <TableRow className={classes.row}>
                            <TableCell className={classes.cell}>{t('Blocked')}</TableCell>
                            <TableCell className={classes.cell} align="right">
                                {wallet.holdBalance} {wallet.cryptocurrency}
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Grid>
        </Grid>
    );
};

WalletInfo.propTypes = {
    t: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,

    currency: PropTypes.string,
    wallet: PropTypes.object,
    generateWalletAddress: PropTypes.func
};

export default withNamespaces('wallets')(withStyles(style)(WalletInfo));

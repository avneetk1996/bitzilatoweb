// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Route, Switch, NavLink } from 'react-router-dom';
import { withNamespaces } from 'react-i18next';
import Helmet from 'react-helmet';
import { MenuItem } from '@material-ui/core';
import { withStyles, Paper, Drawer, Divider, Hidden, Typography, Grid } from '@material-ui/core';
import WalletsCommon from './WalletsCommon';
import Wallet from './Wallet';
import { loadWalletList } from '../../redux';
import { WALLETS_URL } from '../../config';
import withInitialWidth from '../hocs/withInitialWidth';

const drawerWidth = 240;

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    appFrame: {
        minHeight: 430,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%'
    },
    drawerPaper: {
        position: 'relative',
        [theme.breakpoints.up('sm')]: {
            width: `${drawerWidth}px`
        }
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        width: `calc(100% - ${drawerWidth}px)`
    },
    menuIcon: {
        //marginRight: 20
    },
    menuText: {
        textDecoration: 'none'
    },
    telegramIcon: {
        width: 24,
        height: 24,
        verticalAlign: 'middle'
    }
});

class WalletsContainer extends Component {
    static propTypes = {
        t: PropTypes.func.isRequired,
        classes: PropTypes.object.isRequired,

        initialWidth: PropTypes.string,
        wallets: PropTypes.array.isRequired,
        cryptocurrencies: PropTypes.array.isRequired,
        loadWalletList: PropTypes.func.isRequired
    };

    componentDidMount() {
        this.props.loadWalletList();
    }

    render() {
        const { classes, t } = this.props;
        const padding = { padding: 40 };

        return (
            <div>
                <Paper className={classes.appFrame}>
                    {this.drawer()}
                    <Paper className={classes.content}>
                        <Switch>
                            <Route exact path={WALLETS_URL}>
                                <Grid container style={padding} spacing={24} className={classes.root}>
                                    <Helmet>
                                        <title>{t('All Balance')}</title>
                                    </Helmet>

                                    <Grid item xs={12}>
                                        <Typography variant="h6">
                                            <span role="img" aria-label="">
                                                💰
                                            </span>{' '}
                                            {t('All Balance')}
                                        </Typography>
                                    </Grid>
                                    <WalletsCommon />
                                </Grid>
                            </Route>

                            <Route path={`${WALLETS_URL}/:cryptocurrency/:page?`} component={Wallet} />
                        </Switch>
                    </Paper>
                </Paper>
            </div>
        );
    }

    drawer() {
        const { wallets, t, classes, initialWidth } = this.props;
        const activeStyle = { backgroundColor: '#efefef' };

        return (
            <Drawer variant="permanent" classes={{ paper: classes.drawerPaper }} anchor="left">
                <NavLink activeStyle={activeStyle} className={classes.menuText} exact to={WALLETS_URL}>
                    <MenuItem>
                        <div>
                            <span role="img" aria-label="">
                                💰
                            </span>{' '}
                            <Hidden smDown initialWidth={initialWidth}>
                                {t('All Balance')}
                            </Hidden>
                        </div>
                    </MenuItem>
                </NavLink>

                <Divider />

                {_.flatten(
                    _.map(wallets, wallet => [
                        <NavLink
                            key={wallet.cryptocurrency}
                            activeStyle={activeStyle}
                            className={classes.menuText}
                            to={`${WALLETS_URL}/${wallet.cryptocurrency}`}
                        >
                            <MenuItem>
                                <div>{wallet.cryptocurrency}</div>
                            </MenuItem>
                        </NavLink>,
                        <Divider key={`D${wallet.cryptocurrency}`} />
                    ])
                )}
            </Drawer>
        );
    }
}

const mapStateToProps = state => {
    return {
        wallets: _.map(_.keys(state.wallets), x => ({ cryptocurrency: x })),
        cryptocurrencies: state.refs.cryptocurrencies
    };
};

export default compose(
    withInitialWidth,
    withStyles(styles),
    withNamespaces('wallets'),
    connect(
        mapStateToProps,
        { loadWalletList }
    )
)(WalletsContainer);

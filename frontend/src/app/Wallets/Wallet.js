// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import { connect } from 'react-redux';
import { withRouter, Route, Switch } from 'react-router-dom';
import { push } from 'connected-react-router';
import Helmet from 'react-helmet';
import { Tabs, Tab, withStyles } from '@material-ui/core';

import { WALLETS_URL } from '../../config';
import { loadWalletInfo, generateWalletAddress, withdrawalRates, withdraw, loadProfile } from '../../redux';
import Info from './Info';
import RateSource from './RateSource';
import Voucher from './Voucher';
import Receive from './Receive';
import Withdraw from './Withdraw';
import Transactions from './Transactions';

const styles = theme => ({
    root: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
        display: 'flex',
        flexDirection: 'column'
    },
    tabs: {
        //width: '100%',
        flex: '0 0 auto'
    },
    tabContent: {
        flexGrow: 1,
        padding: theme.spacing.unit * 5,
        [theme.breakpoints.down('sm')]: {
            padding: '10px 2px'
        }
    }
});

class Wallet extends Component {
    constructor() {
        super();

        this.state = {
            value: 2
        };
    }

    static propTypes = {
        t: PropTypes.func,
        push: PropTypes.func,
        classes: PropTypes.object,

        currency: PropTypes.string,
        cryptocurrency: PropTypes.string,
        page: PropTypes.string,
        wallet: PropTypes.object,
        rates: PropTypes.array,
        voucherList: PropTypes.array,
        transactions: PropTypes.array,

        cancelVoucher: PropTypes.func,
        createVoucher: PropTypes.func,
        loadVoucherList: PropTypes.func,
        loadProfile: PropTypes.func,
        loadWalletInfo: PropTypes.func,
        generateWalletAddress: PropTypes.func,
        withdrawalRates: PropTypes.func,
        withdraw: PropTypes.func,
        commentTransaction: PropTypes.func
    };

    componentDidMount() {
        this.load();
    }

    componentDidUpdate(prevProps) {
        if (this.props.cryptocurrency !== prevProps.cryptocurrency) {
            this.load();
        }
    }

    load() {
        this.props.loadWalletInfo(this.props.cryptocurrency);
    }

    handleChange = (event, value) => {
        this.props.push(`${WALLETS_URL}/${this.props.cryptocurrency}/${value}`);
    };

    generateWalletAddress = () => {
        this.props.generateWalletAddress();
    };

    withdrawalRates = (address, amount) => {
        this.props.withdrawalRates(address, amount);
    };

    withdraw = (address, amount, rate) => {
        this.props.withdraw(address, amount, rate);
    };

    commentTransaction = (id, comment) => {
        this.props.commentTransaction(id, comment);
    };

    loadVoucherList = () => {
        this.props.loadVoucherList();
    };

    cancelVoucher = code => {
        this.props.cancelVoucher(code);
    };

    createVoucher = (amount, cryptocurrency, currency, method) => {
        this.props.createVoucher(amount, cryptocurrency, currency, method);
    };

    render() {
        const { cryptocurrency, currency, page, classes } = this.props;

        return (
            <div className={classes.root}>
                <Helmet>
                    <title>{cryptocurrency}</title>
                </Helmet>

                <Tabs value={page || ''} onChange={this.handleChange} variant="scrollable">
                    <Tab label="Информация" value="" />
                    <Tab label="Курс" value="rate" />
                    <Tab label="Ввод" value="receive" />
                    <Tab label="Вывод" value="withdraw" />
                    <Tab label="Чеки" value="vouchers" />
                    <Tab label="Транзакции" value="txs" />
                </Tabs>

                <div className={classes.tabContent}>
                    <Switch>
                        <Route exact path={`${WALLETS_URL}/:cryptocurrency/`}>
                            <Info
                                wallet={this.props.wallet}
                                generateWalletAddress={this.generateWalletAddress}
                                currency={this.props.currency}
                            />
                        </Route>

                        <Route exact path={`${WALLETS_URL}/:cryptocurrency/rate`}>
                            <RateSource cryptocurrency={cryptocurrency} currency={currency} />
                        </Route>

                        <Route exact path={`${WALLETS_URL}/:cryptocurrency/receive`}>
                            <Receive
                                wallet={this.props.wallet}
                                generateWalletAddress={this.generateWalletAddress}
                                cryptocurrency={cryptocurrency}
                            />
                        </Route>

                        <Route exact path={`${WALLETS_URL}/:cryptocurrency/withdraw`}>
                            <Withdraw
                                wallet={this.props.wallet}
                                withdrawalRates={this.withdrawalRates}
                                withdraw={this.withdraw}
                                rates={this.props.rates}
                                cryptocurrency={cryptocurrency}
                            />
                        </Route>

                        <Route exact path={`${WALLETS_URL}/:cryptocurrency/vouchers`}>
                            <Voucher cryptocurrency={cryptocurrency} />
                        </Route>

                        <Route exact path={`${WALLETS_URL}/:cryptocurrency/txs`}>
                            <Transactions cryptocurrency={this.props.cryptocurrency} />
                        </Route>
                    </Switch>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => {
    const currency = _.get(state, 'profile.settings.currency');
    const cryptocurrency = props.match.params.cryptocurrency;

    return {
        currency: currency,
        cryptocurrency: cryptocurrency,
        page: props.match.params.page,
        wallet: state.wallets[cryptocurrency],
        rates: _.get(state.wallets, [cryptocurrency, 'rates']),
        voucherList: state.wallets[cryptocurrency] && state.wallets[cryptocurrency].voucherList,
        transactions: cryptocurrency && _.get(state.wallets, [cryptocurrency, 'transactions', 'items'])
    };
};

const mapDispatchToProps = (dispatch, props) => {
    const cryptocurrency = props.match.params.cryptocurrency;

    return {
        loadProfile: () => dispatch(loadProfile()),
        loadWalletInfo: () => dispatch(loadWalletInfo(cryptocurrency)),
        generateWalletAddress: () => dispatch(generateWalletAddress(cryptocurrency)),
        withdrawalRates: (address, amount) => dispatch(withdrawalRates(cryptocurrency, address, amount)),
        withdraw: (address, amount, rate) => dispatch(withdraw(cryptocurrency, address, amount, rate)),

        push: path => dispatch(push(path))
    };
};

export default withNamespaces('profile')(
    withStyles(styles)(
        withRouter(
            connect(
                mapStateToProps,
                mapDispatchToProps
            )(Wallet)
        )
    )
);

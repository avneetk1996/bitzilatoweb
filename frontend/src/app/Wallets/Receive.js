// -*- rjsx -*-
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import Helmet from 'react-helmet';
import { Button, Grid, Paper, Typography } from '@material-ui/core';
import copy from 'copy-to-clipboard';

class Receive extends Component {
    static propTypes = {
        t: PropTypes.func.isRequired,

        cryptocurrency: PropTypes.string,

        generateWalletAddress: PropTypes.func,
        wallet: PropTypes.object
    };

    render() {
        const { t, wallet, cryptocurrency } = this.props;

        return (
            <Grid container direction="column" spacing={40}>
                <Helmet>
                    <title>{t('receiveTitle', { cryptocurrency })}</title>
                </Helmet>

                <Grid item xs={12}>
                    <Typography variant="h6">
                        <span role="img" aria-label="">
                            📥
                        </span>{' '}
                        {t('receiveTitle', { cryptocurrency })}
                    </Typography>
                </Grid>

                {wallet && (wallet.address ? this.renderAddress() : this.renderGenerateAddress())}
            </Grid>
        );
    }

    renderAddress() {
        const { wallet } = this.props;

        const paperStyle = {
            paddingLeft: 20,
            height: 36,
            marginTop: 40
        };

        return (
            <Grid container direction="column">
                <Grid item xs>
                    Вы можете пополнить кошелек, внеся средства на указанный ниже адрес
                </Grid>

                <Grid item xs>
                    <Paper style={paperStyle}>
                        <span style={{ lineHeight: '36px' }}>{wallet.address}</span>

                        <Button color="primary" style={{ float: 'right' }} onClick={() => copy(wallet.address)}>
                            copy
                        </Button>
                    </Paper>
                </Grid>
            </Grid>
        );
    }

    renderGenerateAddress() {
        const { generateWalletAddress } = this.props;

        return (
            <Grid container direction="column" spacing={16}>
                <Grid item>Для пополнения кошелька вы должны сначала сгенерировать адрес</Grid>

                <Grid item>
                    <Button color="primary" variant="contained" onClick={generateWalletAddress}>
                        Сгенерировать
                    </Button>
                </Grid>
            </Grid>
        );
    }
}

export default withNamespaces('wallets')(Receive);

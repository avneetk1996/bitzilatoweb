// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { withNamespaces } from 'react-i18next';
import { Typography, Grid } from '@material-ui/core';

import { P2P_URL } from '../../config';
import { loadMixAdverts, saveLastFilter } from '../../redux';

import AdvertsGrid from './AdvertsGrid';
import AdvertsFilter from './AdvertsFilter';
import withInitialWidth from '../hocs/withInitialWidth';
import {
    parseFilterParams,
    pickFilter,
    DEFAULT_ROWS_SKIP,
    DEFAULT_ROWS_PER_PAGE
} from '../common/advert-filter-helpers';
//const AUTOUPDATE_INTERVAL = 60 * 1000;

class SearchAdvert extends Component {
    constructor() {
        super();
    }

    static propTypes = {
        t: PropTypes.func.isRequired,

        searchAdverts: PropTypes.func,
        pushUrl: PropTypes.func,
        saveLastFilter: PropTypes.func,
        initialWidth: PropTypes.string,

        filterParams: PropTypes.object,
        currencies: PropTypes.array,
        cryptocurrencies: PropTypes.array,
        items: PropTypes.array,
        total: PropTypes.number
    };

    componentDidMount() {
        this.refreshPage();
        this.setupAutoload();
    }

    componentDidUpdate(prevProps) {
        if (!_.isEqual(this.props.filterParams, prevProps.filterParams)) {
            this.refreshPage();
            this.setupAutoload();
        }
    }

    componentWillUnmount() {
        this.clearAutoload();
    }

    clearAutoload() {
        if (this.timerID) {
            clearInterval(this.timerID);
            this.timerID = null;
        }
    }

    setupAutoload() {
        // this.clearAutoload();
        // this.timeID = setInterval(() => this.props.searchAdverts(this.props.filterParams), AUTOUPDATE_INTERVAL);
    }

    changePage = (page, force) => {
        const newFilterParams = pickFilter({
            ...this.props.filterParams,
            skip: page * (this.props.filterParams.limit || DEFAULT_ROWS_PER_PAGE)
        });

        if (!_.isEqual(newFilterParams, pickFilter(this.props.filterParams))) {
            this.setFilteredParamsToUrl(newFilterParams);
        } else if (force) {
            this.refreshPage();
        }
    };

    changeRowsPerPage = limit => {
        const newFilteredParams = pickFilter({
            ...this.props.filterParams,
            limit,
            skip:
                limit *
                (this.props.filterParams.skip / (this.props.filterParams.limit || DEFAULT_ROWS_PER_PAGE) ||
                    DEFAULT_ROWS_SKIP)
        });

        this.setFilteredParamsToUrl(newFilteredParams);
    };

    refreshPage = () => {
        const filter = this.props.filterParams;

        if (filter.type && filter.currency && filter.cryptocurrency) {
            this.props.searchAdverts({
                ...filter
            });
        }
    };

    filterAdverts = filterObject => {
        const newFilterParams = pickFilter(filterObject);
        this.setFilteredParamsToUrl(newFilterParams);
    };

    setFilteredParamsToUrl = newFilterParams => {
        if (!_.isEqual(newFilterParams, pickFilter(this.props.filterParams))) {
            const sp = new URLSearchParams();
            Object.entries(newFilterParams).forEach(([key, value]) => {
                if (Array.isArray(value)) {
                    value.forEach(i => sp.append(key, i));
                } else {
                    sp.append(key, value);
                }
            });

            this.props.saveLastFilter(pickFilter(_.omit(newFilterParams, ['skip', 'limit'])));

            this.props.pushUrl(`${P2P_URL}?${sp.toString()}`);
        } else {
            this.refreshPage();
        }
    };

    render() {
        const { t } = this.props;

        return (
            <Grid container spacing={16}>
                <Helmet>
                    <title>{t('Search adverts')}</title>
                </Helmet>

                <Grid item xs={12}>
                    <Typography variant="h6">{t('Search adverts')}</Typography>
                </Grid>

                <Grid item xs={12}>
                    <AdvertsFilter filterParams={this.props.filterParams} filterAdverts={this.filterAdverts} />
                </Grid>

                {!_.isEmpty(this.props.items) && (
                    <Grid item xs={12}>
                        <AdvertsGrid
                            data={this.props.items}
                            page={Number(
                                (this.props.filterParams.skip || DEFAULT_ROWS_SKIP) /
                                    (this.props.filterParams.limit || DEFAULT_ROWS_PER_PAGE)
                            )}
                            rowsPerPage={Number(this.props.filterParams.limit) || DEFAULT_ROWS_PER_PAGE}
                            changePage={this.changePage}
                            changeRowsPerPage={this.changeRowsPerPage}
                            refreshPage={this.refreshPage}
                            filterAdverts={this.filterAdverts}
                            total={this.props.total}
                            pushUrl={this.props.pushUrl}
                            currency={this.props.filterParams.currency}
                            cryptocurrency={this.props.filterParams.cryptocurrency}
                        />
                    </Grid>
                )}
            </Grid>
        );
    }
}

const mapStateToProps = (state, props) => {
    const { filter, items, total } = (state && state.exchange && state.exchange.mix) || {};

    return {
        filterParams: parseFilterParams(_.get(props, 'history.location.search')),
        filter,
        items,
        total,
        currencies: state.refs.currencies,
        cryptocurrencies: state.refs.cryptocurrencies
    };
};

export default compose(
    withInitialWidth,
    withNamespaces('adverts'),
    withRouter,
    connect(
        mapStateToProps,
        { searchAdverts: loadMixAdverts, pushUrl: push, saveLastFilter: saveLastFilter }
    )
)(SearchAdvert);

// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import {
    withStyles,
    Paper,
    Table,
    TableHead,
    TableBody,
    TableCell,
    TableFooter,
    TablePagination,
    TableRow,
    Button,
    Hidden,
    Typography
} from '@material-ui/core';
import { push } from 'connected-react-router';
import { withNamespaces } from 'react-i18next';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import withWidth, { isWidthUp, isWidthDown } from '@material-ui/core/withWidth';

import { P2P_URL } from '../../config';
import Confirm from '../common/Confirm';
import TimeBall, { lastActivityColor } from '../users/TimeBall';
import { BackendClient } from '../../redux/actions/api';
import TablePaginationActions from '../common/TablePaginationActions';
import withInitialWidth from '../hocs/withInitialWidth';

const styles = theme => ({
    root: {
        width: '100%'
    },
    table: {
        minWidth: 100,
        tableLayout: 'auto'
    },
    row: {},
    cell: {
        [theme.breakpoints.down('sm')]: {
            fontSize: '80%',
            padding: '5px 3px 5px 10px'
        }
    },
    column: {
        color: '#333',
        fontWeight: 'bold',
        [theme.breakpoints.down('sm')]: {
            fontSize: '80%',
            padding: '5px 3px 5px 10px'
        }
    },
    tableWrapper: {
        overflowX: 'auto'
    },
    linkToPaymethod: {
        color: '#3f51b5',
        textDecoration: 'underline',
        cursor: 'pointer'
    }
});

class AdvertsGrid extends Component {
    static propTypes = {
        t: PropTypes.func.isRequired,
        classes: PropTypes.object.isRequired,
        pushUrl: PropTypes.func.isRequired,

        changePage: PropTypes.func.isRequired,
        changeRowsPerPage: PropTypes.func.isRequired,
        refreshPage: PropTypes.func.isRequired,
        filterAdverts: PropTypes.func.isRequired,

        total: PropTypes.number,
        data: PropTypes.array,
        page: PropTypes.number,
        rowsPerPage: PropTypes.number,
        initialWidth: PropTypes.string,
        auth: PropTypes.object,
        width: PropTypes.string.isRequired,
        currency: PropTypes.string,
        cryptocurrency: PropTypes.string
    };

    constructor() {
        super();

        this.state = {
            confirm: null
        };
    }

    handleChangePage = (event, page, force) => {
        this.props.changePage(page, force);
    };

    handleChangeRowsPerPage = async event => {
        const rowsPerPage = event.target.value;
        this.props.changeRowsPerPage(rowsPerPage);
    };

    handleRefreshPage = () => {
        this.props.refreshPage();
    };

    openAdvert = async advert => {
        const { t, auth, pushUrl } = this.props;
        const freshAdvert = await new BackendClient(auth.userId, auth.idToken, '').getPublicAdvert(advert.id);

        const url = `${P2P_URL}/exchange/${advert.type === 'selling' ? 'buy' : 'sell'}/${advert.id}`;

        if (advert.rate !== freshAdvert.rate) {
            this.setState({
                confirm: {
                    title: t('confirmAdvertRateTitle'),
                    message: t('confirmAdvertRate', {
                        rate: freshAdvert.rate,
                        currency: freshAdvert.paymethod.currency
                    }),
                    handleOk: () => pushUrl(url)
                }
            });
        } else {
            pushUrl(url);
        }
    };

    render() {
        const { data = [], classes, total = 0, t, page, rowsPerPage, currency, cryptocurrency, width } = this.props;
        const { confirm } = this.state;
        const emptyRows = rowsPerPage - Math.min(rowsPerPage, total - page * rowsPerPage);

        return (
            <Paper className={classes.root}>
                {!_.isEmpty(confirm) && (
                    <Confirm
                        opened={true}
                        title={confirm.title}
                        message={confirm.message}
                        handleOk={() => {
                            confirm.handleOk();
                            this.setState(this.setState({ confirm: null }));
                        }}
                        handleCancel={() => this.setState({ confirm: null })}
                    />
                )}

                <div className={classes.tableWrapper}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell component="th" className={classes.column}>
                                    <Typography variant="subtitle2">{t('Trader')}</Typography>
                                </TableCell>
                                <Hidden smDown initialWidth={width}>
                                    <TableCell component="th" className={classes.column}>
                                        <Typography variant="subtitle2">
                                            {t('Payment method')} ({currency})
                                        </Typography>
                                    </TableCell>
                                </Hidden>
                                <TableCell component="th" className={classes.column}>
                                    <Typography variant="subtitle2">
                                        {t('Rate')} ({currency}/{cryptocurrency})
                                    </Typography>
                                </TableCell>
                                <TableCell component="th" className={classes.column}>
                                    <Typography variant="subtitle2">
                                        {t('Limits')} ({currency})
                                    </Typography>
                                </TableCell>
                                <Hidden smDown initialWidth={width}>
                                    <TableCell component="th" className={classes.column} />
                                </Hidden>
                            </TableRow>
                        </TableHead>

                        <TableBody>
                            {_.map(data, advert => this.advertTableRow(advert))}

                            {emptyRows > 0 && (
                                <TableRow className={classes.cell}>
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>

                        <TableFooter>
                            <TableRow>
                                <Hidden smUp initialWidth={width}>
                                    <TablePagination
                                        colSpan={3}
                                        count={total}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        rowsPerPageOptions={[5, 10, 15]}
                                        labelRowsPerPage=""
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                    />
                                </Hidden>

                                <Hidden xsDown initialWidth={width}>
                                    <TablePagination
                                        colSpan={3}
                                        count={total}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        rowsPerPageOptions={[5, 10, 15]}
                                        labelRowsPerPage={t('Rows per page')}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                        ActionsComponent={TablePaginationActions}
                                    />
                                </Hidden>
                            </TableRow>
                        </TableFooter>
                    </Table>
                </div>
            </Paper>
        );
    }

    advertTableRow(advert) {
        const { classes, width, t } = this.props;
        const openAdvert = () => this.openAdvert(advert);

        return (
            <TableRow
                key={advert.id}
                className={classes.row}
                onClick={(isWidthDown('sm', width) && openAdvert) || undefined}
            >
                {this.advertAuthorCell(advert)}

                <Hidden smDown initialWidth={width}>
                    <TableCell className={classes.cell}>
                        <span
                            className={classes.linkToPaymethod}
                            onClick={() =>
                                this.props.filterAdverts({
                                    currency: advert.currency,
                                    paymethod: advert.paymethod.id
                                })
                            }
                        >
                            {advert.paymethod.name}
                        </span>
                    </TableCell>
                </Hidden>

                <TableCell className={classes.cell}>{advert.rate}</TableCell>

                <TableCell className={classes.cell}>
                    {advert.min} - {advert.max}
                </TableCell>

                <Hidden smDown initialWidth={width}>
                    <TableCell>
                        <Button
                            variant="contained"
                            className={classes.cell}
                            color="secondary"
                            fullWidth
                            onClick={openAdvert}
                        >
                            {advert.type === 'selling' ? t('Buy') : t('Sell')}
                        </Button>
                    </TableCell>
                </Hidden>
            </TableRow>
        );
    }

    advertAuthorCell(advert) {
        const { classes, width } = this.props;

        if (isWidthUp('sm', width)) {
            return (
                <TableCell scope="row" className={classes.cell}>
                    <Link to={`${P2P_URL}/users/${advert.owner}`}>{advert.owner}</Link>

                    <TimeBall time={advert.ownerLastActivity} />
                </TableCell>
            );
        } else {
            return (
                <TableCell scope="row" className={classes.cell}>
                    <strong>
                        {
                            /*
                                <Link
                                onClick={e => e.stopPropagation() }
                                to={`${P2P_URL}/users/${advert.owner}`}
                                style={{ color: lastActivityColor(advert.ownerLastActivity) }}
                                    >

                                    </Link>
                             */
                        }
                        {_.truncate(advert.owner, { length: 9 })}
                    </strong>
                </TableCell>
            );
        }
    }
}

const mapStateToProps = (state, props) => ({
    auth: _.pick(state.auth, 'userId', 'idToken')
});

const mapDispatchToProps = dispatch => ({
    pushUrl: data => dispatch(push(data))
});

export default compose(
    withInitialWidth,
    withWidth(),
    withNamespaces('adverts'),
    withStyles(styles),
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(AdvertsGrid);

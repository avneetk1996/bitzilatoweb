// -*- rjsx -*-
import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm, Form, formValueSelector, submit } from 'redux-form';
import { withNamespaces } from 'react-i18next';
import { Grid, withStyles, Button, Typography } from '@material-ui/core';
import Select from 'react-select';
import { renderSwitch } from '../common/redux-form-helpers';
import { loadAdvertPaymethods } from '../../redux';

const styles = theme => ({
    root: {
        fontFamily: theme.typography.fontFamily
    },
    noMargin: {
        marginTop: 0
    },
    margin: {
        marginTop: 12
    },
    chip: {
        margin: theme.spacing.unit / 4
    },
    forText: {
        paddingTop: 18,
        textAlign: 'center'
    }
});

class AdvertsFilter extends Component {
    static propTypes = {
        currencies: PropTypes.array,
        cryptocurrencies: PropTypes.array,
        paymethods: PropTypes.object,
        filterAdverts: PropTypes.func,
        filteredParams: PropTypes.object,
        widgetMode: PropTypes.bool
    };

    componentDidMount() {
        this.initForm();
        this.loadPaymethods();
    }

    componentDidUpdate(prevProps) {
        if (!_.isEqual(this.props.filterParams, prevProps.filterParams)) {
            this.initForm();
        }

        if (this.paymethodsKey(prevProps) !== this.paymethodsKey()) {
            this.loadPaymethods();
        }
    }

    initForm() {
        this.props.initialize(this.props.filterParams);
    }

    onSubmit = values => {
        const { paymethods } = this.props;

        const key = this.paymethodsKey();
        let isOmitPaymethod = !values.currency || !values.paymethod;

        if (!isOmitPaymethod && key in paymethods) {
            const ids = this.props.paymethods[key].map(i => i.id);
            values.paymethod = values.paymethod.filter(i => ids.includes(i));
            if (values.paymethod.length === 0) isOmitPaymethod = true;
        }

        this.props.filterAdverts(isOmitPaymethod ? _.omit(values, 'paymethod') : values);
    };

    loadPaymethods = _.debounce(() => {
        const { currency, cryptocurrency, type, isOwnerActive, isOwnerVerificated } = this.props;

        if (currency) {
            this.props.loadAdvertPaymethods({ type, cryptocurrency, currency, isOwnerActive, isOwnerVerificated });
        }
    }, 300);

    paymethodsKey(props = this.props) {
        return _.join(
            [props.type, props.cryptocurrency, props.currency, props.isOwnerActive, props.isOwnerVerificated],
            '-'
        );
    }

    render() {
        const { currencies, cryptocurrencies, paymethods, widgetMode, handleSubmit, classes, t } = this.props;

        const pKey = this.paymethodsKey();

        const renderSelect = ({ input, options, meta: { touched, error }, ...rest }) => {
            return (
                <Grid container>
                    <Grid item xs={12}>
                        <Select
                            instanceId={input.name}
                            {...rest}
                            value={_.find(options, item => item.value === input.value)}
                            options={options}
                            onChange={x => input.onChange(_.get(x, 'value', ''))}
                            onBlur={() => input.onBlur()}
                        />
                    </Grid>

                    {touched &&
                        error && (
                            <Grid item xs={12}>
                                <Typography variant="caption" color="error">
                                    {error}
                                </Typography>
                            </Grid>
                        )}
                </Grid>
            );
        };

        const renderMultiSelect = ({ input, options, ...rest }) => {
            const value = options.filter(item => input.value.includes(item.value));
            return (
                <Select
                    instanceId={input.name}
                    {...rest}
                    value={value}
                    options={options}
                    onChange={x => input.onChange(x.map(i => i.value))}
                    onBlur={() => input.onBlur()}
                />
            );
        };

        return (
            <Form onSubmit={handleSubmit(this.onSubmit)} onChange={this.onFormChange} className={classes.root}>
                <Grid container spacing={16}>
                    <Grid item xs={12}>
                        <Grid container spacing={8}>
                            <Grid item xs={12} sm={4} md={2}>
                                <Field
                                    name="type"
                                    label={t('I want')}
                                    placeholder={t('I want')}
                                    options={[
                                        {
                                            value: 'selling',
                                            label: t('to sell')
                                        },
                                        {
                                            value: 'purchase',
                                            label: t('to buy')
                                        }
                                    ]}
                                    component={renderSelect}
                                    isClearable
                                    fullWidth
                                />
                            </Grid>

                            <Grid item xs={12} sm={4} md={2}>
                                <Field
                                    name="cryptocurrency"
                                    label={t('Cryptocurrency')}
                                    placeholder={t('Cryptocurrency')}
                                    options={_.map(cryptocurrencies, cryptocurrency => ({
                                        value: cryptocurrency.code,
                                        label: cryptocurrency.code
                                    }))}
                                    component={renderSelect}
                                    isClearable
                                    fullWidth
                                />
                            </Grid>

                            <Grid item xs={12} sm={4} md={2}>
                                <Field
                                    name="currency"
                                    label={t('Currency')}
                                    placeholder={t('Currency')}
                                    options={_.map(currencies, currency => ({
                                        value: currency.code,
                                        label: currency.code
                                    }))}
                                    component={renderSelect}
                                    isClearable
                                    fullWidth
                                />
                            </Grid>

                            <Grid item xs={12} sm={4} md={2}>
                                <Field
                                    name="paymethod"
                                    label={t('Payment method')}
                                    placeholder={t('Payment method')}
                                    isDisabled={_.isEmpty(_.get(paymethods, pKey))}
                                    options={_.map(_.get(paymethods, pKey), item => ({
                                        value: item.id,
                                        label: item.name
                                    }))}
                                    component={renderMultiSelect}
                                    isClearable
                                    isMulti
                                    isSearchable={false}
                                />
                            </Grid>

                            <Grid item xs={12} sm={4} md={2}>
                                <Field
                                    name="isOwnerActive"
                                    component={renderSwitch}
                                    color="primary"
                                    label={t('Only active')}
                                />
                            </Grid>

                            <Grid item xs={12} sm={4} md={2}>
                                <Field
                                    name="isOwnerVerificated"
                                    component={renderSwitch}
                                    color="primary"
                                    label={t('Only verificated')}
                                />
                            </Grid>
                        </Grid>
                    </Grid>

                    {widgetMode && (
                        <Grid item xs={12}>
                            <Button variant="contained" color="primary" type="submit" fullWidth>
                                {t('Search')}
                            </Button>
                        </Grid>
                    )}
                </Grid>
            </Form>
        );
    }
}

const validate = (values, props) => {
    const { t } = props;
    const errors = {};

    if (!values.type) {
        errors.type = t('Required');
    }

    if (!values.currency) {
        errors.currency = t('Required');
    }

    if (!values.cryptocurrency) {
        errors.cryptocurrency = t('Required');
    }

    return errors;
};

const advertFilterFormSelector = formValueSelector('advertFilterForm');

const mapStateToProps = (state, props) => {
    const { currency, cryptocurrency, type, isOwnerActive, isOwnerVerificated } = advertFilterFormSelector(
        state,
        'currency',
        'cryptocurrency',
        'type',
        'isOwnerActive',
        'isOwnerVerificated'
    );
    return {
        currency,
        cryptocurrency,
        type,
        isOwnerActive,
        isOwnerVerificated,
        paymethods: state.refs.paymethods,
        initialValues: props.filterParams,
        currencies: state.refs.currencies,
        cryptocurrencies: state.refs.cryptocurrencies
    };
};

const mapDispatchToProps = dispatch => ({
    loadAdvertPaymethods: filter => dispatch(loadAdvertPaymethods(filter)),
    applyFilter: () => dispatch(submit('advertFilterForm'))
});

export default compose(
    withNamespaces('adverts'),
    withStyles(styles),
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    reduxForm({
        form: 'advertFilterForm',
        validate,
        onChange: (values, dispatch, props) => {
            if (!props.widgetMode && !_.isEqual(values, props.filterParams)) {
                props.submit();
            }
        }
    })
)(AdvertsFilter);

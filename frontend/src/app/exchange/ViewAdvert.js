// -*- rjsx -*-
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { withNamespaces } from 'react-i18next';
import Helmet from 'react-helmet';
import Emoji from 'react-emoji-render';
import Markdown from 'react-remarkable';
import {
    withStyles,
    Paper,
    Grid,
    Button,
    Typography,
    Table,
    TableBody,
    TableCell,
    TableRow,
    ExpansionPanel,
    ExpansionPanelDetails,
    ExpansionPanelSummary
} from '@material-ui/core';
import { green, grey, red } from '@material-ui/core/colors';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Field, Form, reset, reduxForm, formValueSelector, SubmissionError } from 'redux-form';
import { renderTextField, renderSelectField } from '../common/redux-form-helpers';
import TimeBall from '../users/TimeBall';

import { P2P_URL } from '../../config';
import { BackendClient } from '../../redux/actions/api';
import { loadExchangeAdvert, createTrade, estimateTrade, loadWalletInfo } from '../../redux';

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    paper: {
        padding: 24
        // textAlign: 'center',
        // color: theme.palette.text.secondary,
    },
    reportButton: {
        float: 'right'
    },
    startTradeButton: {
        float: 'left'
    },
    editingControlsBlock: {
        float: 'right'
    },
    tirrrre: {
        textAlign: 'center',
        marginTop: 25
    },
    checkboxHeight: {
        height: 38
    },
    terms: {
        whiteSpace: 'pre-line',
        padding: '24px',
        [theme.breakpoints.down('sm')]: {
            padding: '4px 4px'
        }
    },
    rate: {
        '& *': {
            fontSize: '110%',
            color: green[800]
        }
    },
    rating: {
        fontSize: '90%',
        color: grey[500]
    },
    selectMargin: {
        marginTop: 16
    },
    buyButton: {
        marginTop: 28
    },
    protect: {
        marginTop: '40px',
        padding: '16px',
        [theme.breakpoints.down('sm')]: {
            padding: '4px 4px'
        },
        backgroundColor: grey[100]
    },
    warn: {
        padding: '16px',
        [theme.breakpoints.down('sm')]: {
            padding: '4px 4px'
        },
        '& *': {
            color: red[500]
        }
    }
});

class ViewAdvert extends Component {
    constructor() {
        super();

        this.state = {
            estimate: null
        };
    }

    static propTypes = {
        reset: PropTypes.func.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        t: PropTypes.func.isRequired,
        classes: PropTypes.object.isRequired,

        loadAdvert: PropTypes.func,
        createTrade: PropTypes.func,
        estimateTrade: PropTypes.func,
        loadWalletInfo: PropTypes.func,

        advert: PropTypes.object,
        isLogged: PropTypes.bool.isRequired,
        wallets: PropTypes.object,
        initialValues: PropTypes.object,
        profile: PropTypes.object,
        auth: PropTypes.object,
        input: PropTypes.object
    };

    componentDidMount = async () => {
        this.props.loadAdvert();
    };

    componentDidUpdate = async prevProps => {
        const { auth, advert, match, wallets, input } = this.props;

        if (match.params.advertId !== prevProps.match.params.advertId) {
            this.props.loadAdvert();
        }

        if (
            !_.isEmpty(auth) &&
            !_.isEmpty(advert) &&
            match.params.advertId == advert.id &&
            advert.type === 'selling' &&
            _.isEmpty(wallets)
        ) {
            this.props.loadWalletInfo(advert.cryptocurrency);
        }

        if (!_.isEqual(input, prevProps.input)) {
            this.estimateTrade();
        }
    };

    async estimateTrade() {
        const {
            advert,
            auth,
            input: { amount, amountType }
        } = this.props;

        if (!_.isEmpty(auth) && !_.isEmpty(advert) && amount && amountType) {
            const apiClient = new BackendClient(this.props.auth.userId, this.props.auth.idToken);
            this.setState({
                estimate: await apiClient.estimateTrade(advert.id, amount, amountType, advert.rate)
            });
        } else if (!_.isEmpty(this.state.estimate)) {
            this.setState({
                estimate: null
            });
        }
    }

    createTrade = async ({ amount, amountType, details }) => {
        const { advert, t } = this.props;

        const errors = {};

        if (!amountType) {
            errors.amountType = t('Required');
        }

        if (!amount) {
            errors.amount = t('Required');
        } else {
            const factor = amountType === advert.paymethod.currency ? 1 : advert.rate;

            if (amount * factor < advert.limit.min) {
                errors.amount = t('Minimal amount', {
                    min: advert.limit.min / factor,
                    coin: amountType
                });
            }

            if (amount * factor > advert.limit.max) {
                errors.amount = t('Maximal amount', {
                    max: advert.limit.max / factor,
                    coin: amountType
                });
            }
        }

        if (!_.isEmpty(errors)) {
            throw new SubmissionError(errors);
        }

        return await this.props.createTrade(amount, amountType, advert.rate, details);
    };

    balanceInfo = () => {
        const { advert, t, wallets } = this.props;
        if (!(advert.type === 'selling' && !_.isEmpty(wallets, [advert.cryptocurrency]))) return null;

        return (
            <Grid item xs={12}>
                <Typography>
                    {t('Your balance')}
                    {': '}
                    {wallets[advert.cryptocurrency].balance} {wallets[advert.cryptocurrency].cryptocurrency}
                </Typography>
            </Grid>
        );
    };

    render() {
        const { advert, t } = this.props;

        if (_.isEmpty(advert)) return null;

        const headlineText = t(advert.type === 'purchase' ? 'advertHeaderBuy' : 'advertHeaderSell', {
            cryptocurrency: advert.cryptocurrency,
            paymethod: advert.paymethod.name,
            currency: advert.paymethod.currency
        });

        return (
            <Grid container spacing={24}>
                <Helmet>
                    <title>{headlineText}</title>
                </Helmet>

                {this.warnInfo()}

                <Grid item xs={12}>
                    <Typography variant="h6">{headlineText}</Typography>
                </Grid>

                {this.balanceInfo()}

                <Grid item xs={12}>
                    {this.startTradeForm()}
                </Grid>

                <Grid item xs={12}>
                    {this.advertInfo()}
                </Grid>

                <Grid item xs={12}>
                    {this.protectInfo()}
                </Grid>

                <Grid item xs={12}>
                    <ExpansionPanel>
                        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>Как начать и связаться с трейдером</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <ul>
                                <li>
                                    <Typography>
                                        Прочитайте условия сделки и удостоверьтесь в том, что вы сможете их выполнить.
                                    </Typography>
                                </li>
                                <li>
                                    <Typography>
                                        Укажите сумму в криптовалюте или валюте, которую хотите обменять.
                                    </Typography>
                                </li>
                                <li>
                                    <Typography>
                                        В разделе дополнительная информация укажите необходимую для трейдера информацию.
                                    </Typography>
                                </li>
                                <li>
                                    <Typography>
                                        После отправки запроса на сделку вы можете начать обсуждение с трейдером в
                                        системе обмена сообщения Bitzlato
                                    </Typography>
                                </li>
                                <li>
                                    <Typography>
                                        Открытые запросы на сделку находятся в разделе &quot;Мои сделки&quot;. Там вы
                                        можете безопасно переписываться с трейдером.
                                    </Typography>
                                </li>
                                <li>
                                    <Typography>
                                        Для получения дополнительной информации смотрите краткое руководство
                                        FAQ(https://bitzlato.com/ru/faq)
                                    </Typography>
                                </li>
                            </ul>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>

                    <ExpansionPanel>
                        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>Отмена сделки</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <Typography>
                                Вы можете отменить сделку до осуществления оплаты.
                                <br />
                                Открытые сделки находятся в разделе &quot;Мои сделки&quot;.
                            </Typography>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                </Grid>
            </Grid>
        );
    }

    startTradeForm() {
        const { advert, handleSubmit, classes, t, isLogged } = this.props;

        const currencyOptions = [
            {
                value: advert.paymethod.currency,
                label: advert.paymethod.currency
            },
            {
                value: advert.cryptocurrency,
                label: advert.cryptocurrency
            }
        ];

        const isPurchase = advert.type === 'purchase';
        const notEnought = isLogged && advert.type === 'selling' && !(advert.limit.sellingMax > 0);

        return (
            <Form onSubmit={handleSubmit(this.createTrade)}>
                <Grid container spacing={24}>

                    <Grid item xs={12} sm={6}>
                        <Grid container spacing={24}  direction={!isPurchase && 'column'} >
                            <Grid item xs={12} sm={isPurchase ? 6 : 8}>
                                <Field name="amount" component={renderTextField} label={t('Amount')} fullWidth type="number" />
                            </Grid>

                            <Grid item xs={12} sm={isPurchase ? 6 : 8}>
                                <Field
                                    name="amountType"
                                    select
                                    component={renderSelectField}
                                    label={t('Currency')}
                                    fullWidth
                                    className={classes.selectMargin}
                                    options={currencyOptions}
                                />
                            </Grid>
                        </Grid>
                    </Grid>



                    {!isPurchase && (
                        <Grid item xs={12} sm={isPurchase ? 6 : 6}>
                            <Field
                                name="details"
                                component={renderTextField}
                                label={t('Details')}
                                fullWidth
                                type="text"
                                multiline
                                rows={5}
                                variant={'outlined'}
                            />
                        </Grid>
                    )}

                    {this.state.estimate && (
                        <Grid item xs={12}>
                            <Typography variant="caption">
                                {t(
                                    advert.type === 'purchase' ? 'advertBuyEstimate' : 'advertSellEstimate',
                                    this.state.estimate
                                )}
                            </Typography>
                        </Grid>
                    )}

                    <Grid item xs={12}>
                        {advert.available && (
                            <Button
                                disabled={notEnought}
                                variant="contained"
                                className={isPurchase ? classes.buyButton : ''}
                                type="submit"
                                fullWidth
                                color="primary"
                            >
                                {notEnought ? t('Not enought funds to start the trade') : t('Start trade')}
                            </Button>
                        )}
                    </Grid>
                </Grid>
            </Form>
        );
    }

    advertInfo() {
        const { t, classes, advert, isLogged } = this.props;

        const limitMax = isLogged && advert.type === 'selling' ? advert.limit.sellingMax : advert.limit.max;

        let verification = t('no');
        if (advert.owner.suspicious) {
            verification = `🆘 ${t('Suspicious')}`;
        } else if (advert.owner.verification) {
            verification = `✅ ${t('Identity card')}`;
        }

        return (
            <Grid container spacing={24} direction="row">
                <Grid item xs={12} sm={6}>
                    <Table>
                        <TableBody>
                            <TableRow className={classes.rate}>
                                <TableCell>
                                    <strong>{t('Rate')}</strong>
                                </TableCell>
                                <TableCell>
                                    {advert.rate} {advert.paymethod.currency}
                                </TableCell>
                            </TableRow>

                            <TableRow>
                                <TableCell>
                                    <strong>{t('Trader')}</strong>
                                </TableCell>
                                <TableCell>
                                    <Grid container spacing={8}>
                                        <Grid item xs={12}>
                                            <span role="img" aria-label="">
                                                👤
                                            </span>
                                            <Link to={`${P2P_URL}/users/${advert.owner.name}`}>
                                                <span>{advert.owner.name}</span>
                                                <TimeBall time={advert.owner.lastActivity} />
                                            </Link>
                                        </Grid>

                                        <Grid item xs={12}>
                                            {t('Verification')}: {verification}
                                        </Grid>

                                        <Grid item xs={12}>
                                            <span className={classes.rating}>
                                                {t('rating')} {advert.owner.rating}; ({advert.owner.feedbacks.thumbUp}
                                                )👍 ({advert.owner.feedbacks.thumbDown}
                                                )👎
                                            </span>
                                        </Grid>
                                    </Grid>
                                </TableCell>
                            </TableRow>

                            <TableRow>
                                <TableCell>
                                    <strong>{t('Limits')}</strong>
                                </TableCell>
                                <TableCell>
                                    {advert.limit.min} - {limitMax}
                                </TableCell>
                            </TableRow>

                            {advert.details && (
                                <TableRow>
                                    <TableCell>
                                        <strong>{t('Details')}</strong>
                                    </TableCell>
                                    <TableCell>{advert.details}</TableCell>
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </Grid>

                <Grid item xs={12} sm={6}>
                    <Paper className={classes.terms}>
                        <Grid container direction="column" spacing={8}>
                            <Grid item>
                                <Typography variant="subtitle1">{t('Terms of use')}</Typography>
                            </Grid>

                            <Grid item>
                                <Emoji text={advert.terms || ''} />
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
            </Grid>
        );
    }

    protectInfo() {
        const { classes, t } = this.props;

        return (
            <Paper className={classes.protect}>
                <Markdown>{t('Bitzlato Protect')}</Markdown>
            </Paper>
        );
    }

    warnInfo() {
        const { advert, profile, classes, t } = this.props;

        if (!advert.available) {
            return (
                <Grid item xs={12}>
                    <Paper className={classes.warn}>{t('notAvailable')}</Paper>
                </Grid>
            );
        }

        if (_.isEmpty(profile)) {
            return (
                <Grid item xs={12}>
                    <Paper className={classes.warn}>{t('Need login')}</Paper>
                </Grid>
            );
        } else {
            return null;
        }
    }
}

const mapStateToProps = (state, props) => {
    const advertId = props.match.params.advertId;
    const advert = _.get(state, ['exchange', 'adverts', advertId]);
    const selector = formValueSelector('startTradeForm');
    const isLogged = !_.isEmpty(state.auth);

    return {
        advert,
        wallets: state.wallets,
        initialValues: advert,
        profile: _.get(state, 'profile.settings'),
        auth: _.pick(state.auth, 'userId', 'idToken'),
        input: selector(state, 'amount', 'amountType'),
        isLogged
    };
};

const mapDispatchToProps = (dispatch, props) => {
    const advertId = _.parseInt(props.match.params.advertId);

    return {
        loadAdvert: () => dispatch(loadExchangeAdvert(advertId)),
        reset: () => dispatch(reset('editAdvertForm')),
        createTrade: (amount, amountType, rate, details) =>
            dispatch(createTrade(advertId, amount, amountType, rate, details)),
        estimateTrade: (amount, amountType, rate) => dispatch(estimateTrade(advertId, amount, amountType, rate)),
        loadWalletInfo: cryptocurrency => dispatch(loadWalletInfo(cryptocurrency))
    };
};

export default reduxForm({ form: 'startTradeForm' })(
    withStyles(styles)(
        withNamespaces('adverts')(
            connect(
                mapStateToProps,
                mapDispatchToProps
            )(ViewAdvert)
        )
    )
);

// -*- rjsx -*-*
import React, { Component } from 'react';
import { Grid, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';

export default class NoMatch extends Component {
    static propTypes = {};

    render() {
        const style = {
            textAlign: 'center'
        };

        return (
            <Grid container spacing={16}>
                <Grid item xs={12}>
                    <Typography variant="h4" style={style}>
                        404
                    </Typography>
                </Grid>

                <Grid item xs={12}>
                    <Typography variant="subtitle1" style={style}>
                        <Link to="/">return to home</Link>
                    </Typography>
                </Grid>
            </Grid>
        );
    }
}

// -*- rjsx -*-
import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { hot } from 'react-hot-loader';
import { withRouter, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { Paper, withStyles, Typography } from '@material-ui/core';
import { withNamespaces } from 'react-i18next';
import Markdown from 'react-remarkable';

import Header from './common/Header';
import Footer from './common/Footer';
import Alert from './common/Alert';
import Notifier from './common/Notifier';
import SafeModeWizard from './common/SafeModeWizard';
import AccessToken from './common/AccessToken';
import SearchAdverts from './exchange/SearchAdverts';
import UserPage from './users/UserPage';
import ViewAdvert from './exchange/ViewAdvert';
import Profile from './Profile';
import MyAdverts from './adverts/MyAdverts';
import CreateAdvert from './adverts/CreateAdvert';
import Advert from './adverts/Advert';
import Trade from './trades/Trade';
import TradeList from './trades/TradeList';
import Wallets from './Wallets';
import Tools from './Tools';

import NotFound from './NotFound';
import PrivateRoute from './PrivateRoute';
import TermsBlock from './TermsBlock';

import { PROFILE_URL, WALLETS_URL, P2P_URL } from '../config';
import { updateProfile } from '../redux/actions';

const styles = theme => ({
    '@global': {
        html: {
            height: '100%'
        },
        body: {
            margin: '0',
            padding: '0',
            backgroundColor: 'rgba(0,0,0,0.05)',
            position: 'relative',
            minHeight: '100%'
        }
    },
    app: {
        paddingTop: '80px',
        paddingBottom: '240px',
        paddingLeft: '10px',
        paddingRight: '10px',
        [theme.breakpoints.down('sm')]: {
            paddingTop: 70,
            paddingBottom: 200,
            paddingLeft: 0,
            paddingRight: 0
        }
    },
    container: {
        maxWidth: '1200px',
        margin: '0 auto'
    },
    footer: {
        position: 'absolute',
        width: '100%',
        bottom: '0',
        height: '200px'
    },
    content: {
        padding: 40,
        [theme.breakpoints.down('md')]: {
            padding: 20
        },
        [theme.breakpoints.down('sm')]: {
            padding: 10
        }
    }
});

const page = (Component, extraProps = {}) => {
    return withStyles(styles)(props => (
        <Paper className={props.classes.content}>
            <Component {..._.omit(props, 'classes')} {...extraProps} />
        </Paper>
    ));
};

const navPage = (Component, extraProps = {}) => {
    const NavPage = props => (
        <Paper>
            <Component {...props} {...extraProps} />
        </Paper>
    );
    return NavPage;
};

const PageSearchAdverts = page(SearchAdverts);
const PageUserPage = page(UserPage);
const PageViewAdvert = page(ViewAdvert);
const PageMyAdverts = page(MyAdverts);
const PageCreateAdvert = page(CreateAdvert);
const PageAdvert = page(Advert);
const PageTrade = page(Trade);
const PageTradeList = page(TradeList);
const PageTools = page(Tools);
const NavPageProfile = navPage(Profile);
const NavPageWallets = navPage(Wallets);
const PageNotFound = page(NotFound);

const App = props => {
    const {
        maintenance,
        isLogged,
        licensingAgreementAccepted,
        acceptLicenceAgreement,
        classes,
        t
    } = props;
    let content;

    if (maintenance) {
        content = (
            <Typography variant="body1" component="div">
                <Markdown>{t('errors:maintenance')}</Markdown>
            </Typography>
        );
    } else if (isLogged && !_.isBoolean(licensingAgreementAccepted)) {
        return null;
    } else if (isLogged && !licensingAgreementAccepted) {
        const Terms = page(TermsBlock);

        content = (
            <Terms
                licensingAgreementAccepted={licensingAgreementAccepted}
                acceptLicenceAgreement={acceptLicenceAgreement}
            />
        );
    } else {
        content = (
            <Switch>
                <Route exact path={`${P2P_URL}/`} component={PageSearchAdverts} />
                <Route exact path={`${P2P_URL}/users/:publicName`} component={PageUserPage} />

                <Route exact path={`${P2P_URL}/exchange/buy/:advertId`} type="purchase" component={PageViewAdvert} />
                <Route exact path={`${P2P_URL}/exchange/sell/:advertId`} type="selling" component={PageViewAdvert} />

                <PrivateRoute exact path={`${P2P_URL}/adverts`} component={PageMyAdverts} />
                <PrivateRoute exact path={`${P2P_URL}/adverts/create`} component={PageCreateAdvert} />
                <PrivateRoute exact path={`${P2P_URL}/adverts/:advertId`} component={PageAdvert} />

                <PrivateRoute exact path={`${P2P_URL}/trades/:tradeId`} component={PageTrade} />
                <PrivateRoute exact path={`${P2P_URL}/trades/`} component={PageTradeList} />

                <PrivateRoute path={`${P2P_URL}/tools/`} component={PageTools} />

                <PrivateRoute path={PROFILE_URL} component={NavPageProfile} />
                <PrivateRoute path={WALLETS_URL} component={NavPageWallets} />
                <Route component={PageNotFound} />
            </Switch>
        );
    }

    return (
        <div>
            <Header />

            <div className={classes.app}>
                <div className={classes.container}>{content}</div>
            </div>

            <div className={classes.footer}>
                <Footer />
            </div>

            <Alert />
            <SafeModeWizard />
            <AccessToken />
            <Notifier />
        </div>
    );
};

App.propTypes = {
    maintenance: PropTypes.bool,
    isLogged: PropTypes.bool,
    licensingAgreementAccepted: PropTypes.bool,
    t: PropTypes.func,
    acceptLicenceAgreement: PropTypes.func,
    classes: PropTypes.object,
};

const mapStateToProps = state => ({
    maintenance: state.maintenance,
    isLogged: !_.isEmpty(state.auth),
    licensingAgreementAccepted: _.get(state, 'profile.settings.licensingAgreementAccepted'),
});

const mapDispatchToProps = dispatch => ({
    acceptLicenceAgreement: () => dispatch(updateProfile({ licensingAgreementAccepted: true }))
});

export default hot(module)(
    withNamespaces()(
        withRouter(
            withStyles(styles)(
                connect(
                    mapStateToProps,
                    mapDispatchToProps
                )(App)
            )
        )
    )
);

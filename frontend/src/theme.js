import { createMuiTheme } from '@material-ui/core/styles';

export default () =>
    createMuiTheme({
        palette: {
            primary: {
                main: '#233070'
            },
            secondary: {
                main: '#FDA535'
            }
        },
        typography: {
            fontFamily: 'Montserrat, Arial, sans-serif',
            fontStyle: 'Regular',
            useNextVariants: true
        }
    });

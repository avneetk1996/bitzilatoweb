import { SET_TIMEZONE } from '../constants';

export function setTimezone(value) {
    return {
        type: SET_TIMEZONE,
        value,
    };
}

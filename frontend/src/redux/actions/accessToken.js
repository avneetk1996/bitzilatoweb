import { OPEN_ACCESS_TOKEN, CLOSE_ACCESS_TOKEN, GET_ACCESS_TOKEN } from '../constants';
import { createApiClient } from './api';

export function openAccessToken() {
    return {
        type: OPEN_ACCESS_TOKEN
    };
}

export function closeAccessToken() {
    return {
        type: CLOSE_ACCESS_TOKEN
    };
}

export function getAccessToken(regenerate = false ) {
    return async function(dispatch, getState) {
        const data = await createApiClient(getState).getAccessToken(regenerate);

        dispatch({
            type: GET_ACCESS_TOKEN,
            data
        });
    };
}

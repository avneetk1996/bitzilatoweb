import { OPEN_SAFEMODE, CLOSE_SAFEMODE, NEXT_STEP_SAFEMODE, SET_VALUE_SAFEMODE, ERROR_SAFEMODE } from '../constants';

export function openSafeMode() {
    return {
        type: OPEN_SAFEMODE
    };
}

export function closeSafeMode() {
    return {
        type: CLOSE_SAFEMODE
    };
}

export function nextStepSafeMode() {
    return {
        type: NEXT_STEP_SAFEMODE
    };
}

export function setValueSafeMode(value) {
    return {
        type: SET_VALUE_SAFEMODE,
        value
    };
}

export function errorSafeMode() {
    return {
        type: ERROR_SAFEMODE
    };
}

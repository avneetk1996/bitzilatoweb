import { LOAD_USER_INFO } from '../constants';
import { createApiClient } from './api';

export function loadUserInfoCache(publicName, cryptocurrency) {
    return async function(dispatch, getState) {
        const { userInfo } = getState();
        const key = `${publicName}-${cryptocurrency}`;

        if (key in userInfo) return userInfo[publicName];

        const user = await createApiClient(getState).userinfo(publicName, cryptocurrency);
        dispatch({
            type: LOAD_USER_INFO,
            key,
            user
        });
        return user;
    };
}

export async function getUserInfo(publicName, cryptocurrency, dispatch, getState) {
    const { userInfo } = getState();
    const key = `${publicName}-${cryptocurrency}`;
    if (key in userInfo) return userInfo[key];
    await dispatch(loadUserInfoCache(publicName, cryptocurrency));
    return getState().userInfo[key];
}

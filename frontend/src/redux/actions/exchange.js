import _ from 'lodash';
import { createApiClient } from './api';
import {
    LOAD_EXCHANGE_MIX_ADVERTS,
    LOAD_EXCHANGE_BUY,
    LOAD_EXCHANGE_SELL,
    LOAD_EXCHANGE_ADVERT,
    SAVE_LAST_FILTER
} from '../constants';
import { getUserInfo } from './userInfo';

export function loadBuyAdverts(filter) {
    return async function(dispatch, getState) {
        const { data, total } = await createApiClient(getState).searchAdverts({
            ...filter,
            type: 'purchase'
        });

        dispatch({
            type: LOAD_EXCHANGE_BUY,
            filter: filter,
            items: data,
            total: total
        });
    };
}

export function loadSellAdverts(filter) {
    return async function(dispatch, getState) {
        const { data, total } = await createApiClient(getState).searchAdverts({
            ...filter,
            type: 'selling'
        });

        dispatch({
            type: LOAD_EXCHANGE_SELL,
            filter: filter,
            items: data,
            total: total
        });
    };
}

export function loadMixAdverts(filter) {
    return async function(dispatch, getState) {
        const { data, total } = await createApiClient(getState).searchAdverts(filter);

        dispatch({
            type: LOAD_EXCHANGE_MIX_ADVERTS,
            filter: filter,
            items: data,
            total: total
        });
    };
}

export function loadExchangeAdvert(id) {
    return async function(dispatch, getState) {
        const advert = await createApiClient(getState).getPublicAdvert(id);

        const owner = await getUserInfo(advert.owner, advert.cryptocurrency, dispatch, getState);
        advert.owner = owner;

        dispatch({
            type: LOAD_EXCHANGE_ADVERT,
            id: id,
            advert
        });
    };
}

export function saveLastFilter(filter) {
    return {
        type: SAVE_LAST_FILTER,
        filter: _.omit(filter, ['skip', 'limit'])
    };
}

import { createApiClient } from './api';
import {
    LOAD_CURRENCIES,
    LOAD_CRYPTOCURRENCIES,
    LOAD_PAYMETHODS,
    LOAD_FEEDBACK_RATINGS,
    LOAD_RATE_SOURCES,
    LOAD_REPORTS
} from '../constants';

export function loadMainRefs() {
    return async function(dispatch, getState) {
        const apiClient = createApiClient(getState);
        const [currencies, cryptocurrencies, feedbackRatings] = await Promise.all([
            apiClient.currencies(),
            apiClient.cryptocurrencies(),
            apiClient.feedbackRatings()
        ]);

        dispatch({
            type: LOAD_CURRENCIES,
            currencies
        });

        dispatch({
            type: LOAD_CRYPTOCURRENCIES,
            cryptocurrencies
        });

        dispatch({
            type: LOAD_FEEDBACK_RATINGS,
            feedbackRatings
        });
    };
}

export function loadPaymethodsRef(currency) {
    return async function(dispatch, getState) {
        const paymethods = await createApiClient(getState).paymethods(currency);

        dispatch({
            type: LOAD_PAYMETHODS,
            currency,
            paymethods
        });
    };
}

export function loadRateSourcesRef(currency, cryptocurrency) {
    return async function(dispatch, getState) {
        const rateSources = await createApiClient(getState).rateSources({ currency, cryptocurrency });

        dispatch({
            type: LOAD_RATE_SOURCES,
            currency,
            cryptocurrency,
            rateSources
        });
    };
}

export function loadReportList() {
    return async function(dispatch, getState) {
        const reports = await createApiClient(getState).reports();

        dispatch({
            type: LOAD_REPORTS,
            reports
        });
    };
}

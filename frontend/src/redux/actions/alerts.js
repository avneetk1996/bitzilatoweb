import { ALERT, CLEAR_ALERT } from '../constants';

export function showAlert(type, text) {
    return {
        type: ALERT,
        message: { type, text }
    };
}

export function clearAlerts() {
    return {
        type: CLEAR_ALERT
    };
}

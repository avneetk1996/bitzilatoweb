import { LOAD_LAST_DEAL } from '../constants';
import { createApiClient } from './api';

export function loadLastDeal() {
    return async function(dispatch, getState) {
        try {
            dispatch({
                type: LOAD_LAST_DEAL,
                lastDeal: await createApiClient(getState).lastDeal()
            });
        } catch (err) {
            dispatch({
                type: LOAD_LAST_DEAL,
                lastDeal: null
            });
        }
    };
}

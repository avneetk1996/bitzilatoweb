import { ADD_SNACKBAR, REMOVE_SNACKBAR } from '../constants';

export const addSnackbar = (message, options, snackOptions) => ({
    type: ADD_SNACKBAR,
    notification: {
        key: new Date().getTime() + Math.random(),
        message,
        options,
        snackOptions,
    }
});

export const removeSnackbar = key => ({
    type: REMOVE_SNACKBAR,
    key
});

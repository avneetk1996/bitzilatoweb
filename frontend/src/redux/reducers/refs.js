import {
    LOAD_CURRENCIES,
    LOAD_CRYPTOCURRENCIES,
    LOAD_PAYMETHODS,
    LOAD_FEEDBACK_RATINGS,
    LOAD_RATE_SOURCES,
    LOAD_REPORTS
} from '../constants';

export default function refs(state = {}, action = {}) {
    switch (action.type) {
        case LOAD_CURRENCIES:
            return {
                ...state,
                currencies: action.currencies
            };

        case LOAD_CRYPTOCURRENCIES:
            return {
                ...state,
                cryptocurrencies: action.cryptocurrencies
            };

        case LOAD_FEEDBACK_RATINGS:
            return {
                ...state,
                feedbackRatings: action.feedbackRatings
            };

        case LOAD_PAYMETHODS:
            return {
                ...state,
                paymethods: {
                    ...((state && state.paymethods) || {}),
                    [action.currency]: action.paymethods
                }
            };
        case LOAD_RATE_SOURCES:
            return {
                ...state,
                rateSources: {
                    ...((state && state.rateSources) || {}),
                    [`${action.currency}-${action.cryptocurrency}`]: action.rateSources
                }
            };
        case LOAD_REPORTS:
            return {
                ...state,
                reports: action.reports
            };
        default:
            return state;
    }
}

import { OPEN_ACCESS_TOKEN, CLOSE_ACCESS_TOKEN, GET_ACCESS_TOKEN } from '../constants';

const initial = { open: false, accessToken: null, expiryDate: null };

export default function accessToken(state = initial, action) {
    switch (action.type) {
        case OPEN_ACCESS_TOKEN:
            return { ...state, open: true };
        case CLOSE_ACCESS_TOKEN:
            return initial;
        case GET_ACCESS_TOKEN: {
            const { accessToken, expiryDate } = action.data;
            return { ...state, accessToken, expiryDate };
        }
        default:
            return state;
    }
}

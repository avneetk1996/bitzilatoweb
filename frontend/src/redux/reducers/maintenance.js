import { MAINTENANCE } from '../constants';

export default function maintenance(state = false, action = {}) {
    switch (action.type) {
        case MAINTENANCE:
            return true;
        default:
            return state;
    }
}

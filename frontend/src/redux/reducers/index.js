import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { connectRouter } from 'connected-react-router';

import accessToken from './accessToken';
import adverts from './adverts';
import alerts from './alerts';
import exchange from './exchange';
import lastdeal from './lastdeal';
import maintenance from './maintenance';
import notifications from './notifications';
import profile from './profile';
import refs from './refs';
import safemode from './safemode';
import snackbars from './snackbars';
import timezone from './timezone';
import trades from './trades';
import userInfo from './userInfo';
import users from './users';
import wallets from './wallets';

export const client = history =>
    combineReducers({
        accessToken,
        adverts,
        alerts,
        auth: (state = {}) => state,
        backendUrl: (state = '') => state,
        exchange,
        form: formReducer,
        lastdeal,
        maintenance,
        notifications,
        profile,
        refs,
        router: connectRouter(history),
        safemode,
        snackbars,
        timezone,
        trades,
        userInfo,
        users,
        wallets
    });

export const ssr = () =>
    combineReducers({
        accessToken,
        adverts,
        alerts,
        auth: (state = {}) => state,
        backendUrl: (state = '') => state,
        cookies: (state = []) => state,
        exchange,
        form: formReducer,
        maintenance,
        notifications,
        profile,
        refs,
        safemode,
        snackbars,
        timezone,
        trades,
        userInfo,
        users,
        wallets,
        lastdeal
    });

export const widget = () =>
    combineReducers({
        backendUrl: (state = '') => state,
        refs,
        form: formReducer
    });

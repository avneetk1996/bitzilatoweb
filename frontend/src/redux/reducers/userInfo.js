import { LOAD_USER_INFO } from '../constants';

export default function userInfo(state = {}, action) {
    switch (action.type) {
        case LOAD_USER_INFO:
            return { ...state, [action.key]: action.user };
        default:
            return state;
    }
}

import _ from 'lodash';

import {
    LOAD_TRADE_CHAT_MESSAGES,
    NEW_TRADE_CHAT_MESSAGE,
    LOAD_TRADE_ADMIN_CHAT_MESSAGES,
    NEW_TRADE_ADMIN_CHAT_MESSAGE,
    LOAD_TRADES,
    LOAD_ONE_TRADE,
    PROMPT_TRADE_DETAILS,
    CANCEL_PROMPT_TRADE_DETAILS
} from '../constants';

export default function trades(state = {}, action = {}) {
    switch (action.type) {
        case LOAD_TRADES:
            return {
                ...state,
                active: action.trades
            };
        case LOAD_ONE_TRADE:
            return {
                ...state,
                items: {
                    ...(state.items || {}),
                    [action.id]: action.trade
                }
            };
        case PROMPT_TRADE_DETAILS:
            return {
                ...state,
                items: {
                    ...(state.items || {}),
                    [action.id]: {
                        ..._.get(state, ['items', action.id], {}),
                        promptDetails: true
                    }
                }
            };
        case CANCEL_PROMPT_TRADE_DETAILS:
            return {
                ...state,
                items: {
                    ...(state.items || {}),
                    [action.id]: _.omit(_.get(state, ['items', action.id], {}), 'promptDetails')
                }
            };
        case LOAD_TRADE_CHAT_MESSAGES:
            return {
                ...state,
                chats: {
                    ...(state.chats || {}),
                    [action.id]: action.messages
                }
            };
        case NEW_TRADE_CHAT_MESSAGE:
            return {
                ...state,
                chats: {
                    ...(state.chats || {}),
                    [action.id]: _.sortedUniqBy(
                        _.sortBy([..._.get(state, ['chats', action.id], []), action.message], item => item.id),
                        item => item.id
                    )
                }
            };
        case LOAD_TRADE_ADMIN_CHAT_MESSAGES:
            return {
                ...state,
                adminChats: {
                    ...(state.adminChats || {}),
                    [action.id]: action.messages
                }
            };
        case NEW_TRADE_ADMIN_CHAT_MESSAGE:
            return {
                ...state,
                adminChats: {
                    ...(state.adminChats || {}),
                    [action.id]: _.sortedUniqBy(
                        _.sortBy([..._.get(state, ['adminChats', action.id], []), action.message], item => item.id),
                        item => item.id
                    )
                }
            };

        default:
            return state;
    }
}

import moment from 'moment-timezone';

import { SET_TIMEZONE } from '../constants';

export default function safemode(state = null, action) {
    switch (action.type) {
        case SET_TIMEZONE:
            moment.tz.setDefault(action.value);
            return action.value;
        default:
            return state;
    }
}

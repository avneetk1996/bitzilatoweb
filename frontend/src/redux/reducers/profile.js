import {
    LOAD_PROFILE_SETTINGS,
    LOAD_PROFILE_REFERRAL_LINKS,
    LOAD_PROFILE_RATE_SOURCES,
    CLEAR_PROFILE
} from '../constants';

export default function profile(state = { settings: {} }, action = {}) {
    switch (action.type) {
        case LOAD_PROFILE_SETTINGS:
            return {
                ...state,
                settings: action.profile,
                stats: action.stats,
                features: action.features
            };
        case LOAD_PROFILE_REFERRAL_LINKS:
            return {
                ...state,
                referralLinks: action.links
            };
        case LOAD_PROFILE_RATE_SOURCES:
            return {
                ...state,
                rateSources: action.rateSources
            };
        case CLEAR_PROFILE:
            return {};
        default:
            return state;
    }
}

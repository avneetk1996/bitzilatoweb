import { OPEN_SAFEMODE, CLOSE_SAFEMODE, NEXT_STEP_SAFEMODE, SET_VALUE_SAFEMODE, ERROR_SAFEMODE } from '../constants';

const initSafemode = { opened: false, step: 0, value: null, error: false };

export default function safemode(state = initSafemode, action = {}) {
    switch (action.type) {
        case OPEN_SAFEMODE:
            return { ...initSafemode, opened: true };
        case CLOSE_SAFEMODE:
            return { ...state, opened: false };
        case NEXT_STEP_SAFEMODE:
            return { ...state, step: state.step + 1, value: null, error: false };
        case SET_VALUE_SAFEMODE:
            return { ...state, value: action.value };
        case ERROR_SAFEMODE:
            return { ...initSafemode, opened: true, error: true };
        default:
            return state;
    }
}

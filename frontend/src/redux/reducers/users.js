import _ from 'lodash';

import { LOAD_USER, LOAD_USER_CHAT_MESSAGES, NEW_USER_CHAT_MESSAGE } from '../constants';

export default function users(state = {}, action = {}) {
    if (action.type === LOAD_USER) {
        return {
            ...state,
            [action.user.name]: {
                ...action.user,
                chat: {
                    messages: _.get(state, [action.user.name, 'chat', 'messages'], [])
                }
            }
        };
    } else if (action.type === LOAD_USER_CHAT_MESSAGES) {
        return {
            ...state,
            [action.user]: {
                ...state[action.user],
                chat: {
                    messages: _.sortedUniqBy(
                        _.sortBy(
                            [..._.get(state, [action.user.name, 'chat', 'messages'], []), ...action.messages],
                            item => item.id
                        ),
                        item => item.id
                    )
                }
            }
        };
    } else if (action.type === NEW_USER_CHAT_MESSAGE) {
        return {
            ...state,
            [action.user]: {
                ...state[action.user],
                chat: {
                    messages: _.sortedUniqBy(
                        _.sortBy(
                            [..._.get(state, [action.user, 'chat', 'messages'], []), action.message],
                            item => item.id
                        ),
                        item => item.id
                    )
                }
            }
        };
    } else {
        return state;
    }
}

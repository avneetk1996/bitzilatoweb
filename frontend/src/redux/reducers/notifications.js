import _ from 'lodash';

import {
    LOAD_NOTIFICATIONS,
    NEW_NOTIFICATION,
    MARK_NOTIFICATION_AS_READED,
    SHOW_NOTIFICATIONS,
    HIDE_NOTIFICATIONS
} from '../constants';

export default function notifications(state = { items: [] }, action = {}) {
    switch (action.type) {
        case LOAD_NOTIFICATIONS:
            return {
                ...state,
                items: [...action.items]
            };
        case NEW_NOTIFICATION:
            return {
                ...state,
                items: [
                    ...(state.items || []),
                    {
                        id: action.id,
                        name: action.name,
                        data: action.data
                    }
                ]
            };
        case MARK_NOTIFICATION_AS_READED: {
            const items = _.filter(state.items, item => item.id !== action.id);
            return {
                ...state,
                anchorEl: _.isEmpty(items) ? null : state.anchorEl,
                items: items
            };
        }
        case SHOW_NOTIFICATIONS:
            return {
                ...state,
                anchorEl: action.anchorEl
            };
        case HIDE_NOTIFICATIONS:
            return {
                ...state,
                anchorEl: null
            };
        default:
            return state;
    }
}

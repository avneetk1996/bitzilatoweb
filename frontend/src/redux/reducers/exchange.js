import {
    LOAD_EXCHANGE_MIX_ADVERTS,
    LOAD_EXCHANGE_BUY,
    LOAD_EXCHANGE_SELL,
    LOAD_EXCHANGE_ADVERT,
    SAVE_LAST_FILTER
} from '../constants';

export default function exchange(state = {}, action = {}) {
    switch (action.type) {
        case LOAD_EXCHANGE_MIX_ADVERTS:
            return {
                ...state,
                mix: {
                    filter: action.filter,
                    total: action.total,
                    items: action.items
                }
            };
        case LOAD_EXCHANGE_BUY:
            return {
                ...state,
                buy: {
                    filter: action.filter,
                    total: action.total,
                    items: action.items
                }
            };
        case LOAD_EXCHANGE_SELL:
            return {
                ...state,
                sell: {
                    filter: action.filter,
                    total: action.total,
                    items: action.items
                }
            };
        case LOAD_EXCHANGE_ADVERT:
            return {
                ...state,
                adverts: {
                    ...(state.adverts || {}),
                    [action.id]: action.advert
                }
            };
        case SAVE_LAST_FILTER:
            return {
                ...state,
                lastFilter: action.filter
            };
        default:
            return state;
    }
}

const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');

const { BUILD_MARK, STATIC_URL } = require('./src/config');

function rpath(file) {
    return path.resolve(__dirname, file);
}

module.exports = {
    entry: {
        app: rpath('./src/client.js'),
        sw: rpath('./src/service-worker.js'),
        widget: rpath('./src/widget.js')
    },
    plugins: [
        new CopyWebpackPlugin([rpath('public/manifest.json'), rpath('public/logo.png'), rpath('public/favicon.ico')]),
        new MomentLocalesPlugin({
            localesToKeep: ['ru'] // "en" is built into Moment and can’t be removed
        })
    ],
    output: {
        path: rpath('../build/web'),
        filename: chunkData => {
            switch (chunkData.chunk.name) {
                case 'sw':
                    return 'service-worker.js';
                case 'widget':
                    return 'p2p-search-widget.js';
                default:
                    return `[name].bundle.${BUILD_MARK}.js`;
            }
        },
        publicPath: STATIC_URL
    },
    resolve: {
        alias: {
            'moment-timezone': 'moment-timezone/builds/moment-timezone-with-data-2012-2022.min'
        }
    },
    module: {
        rules: [
            // {
            //     enforce: 'pre',
            //     test: /\.js$/,
            //     exclude: /node_modules/,
            //     loader: 'eslint-loader',
            // },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: ['file-loader']
            }
        ]
    }
    // optimization: {
    //     splitChunks: {
    //         chunks: 'all'
    //     }
    // }
};

const path = require('path');
const merge = require('webpack-merge');
const webpack = require('webpack');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    entry: {
        app: [path.resolve(__dirname, './src/client.js'), 'webpack-hot-middleware/client']
    },
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './build/static',
        hot: true
    },
    output: {
        globalObject: 'this'
    },
    plugins: [new webpack.HotModuleReplacementPlugin()]
});

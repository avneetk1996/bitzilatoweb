module.exports = function(grunt) {
    grunt.initConfig({
        env: {
            dev: {
                NODE_ENV: 'development'
            },
            prod: {
                NODE_ENV: 'production'
            }
        },
        nodemon: {},
        webpack: {
            prod: require('./frontend/webpack.prod.js')
        },
        babel: {
            options: {},
            ssr: {
                files: [
                    {
                        expand: true,
                        cwd: 'frontend/src/',
                        src: ['**/*.js'],
                        dest: 'build/ssr/'
                    }
                ]
            }
        },
        clean: {
            ssr: ['build/ssr'],
            web: ['build/web']
        },
        run: {
            ssr: {
                cmd: 'npx',
                args: ['babel-node', 'frontend/src/index.js', '--development']
            },
            'ssr-prod': {
                cmd: 'node',
                args: ['build/ssr/index.js']
            }
        },
        concurrent: {
            dev: {
                tasks: ['run:ssr', 'open:dev'],
                options: {
                    logConcurrentOutput: true
                }
            },
            serve: {
                tasks: ['run:ssr-prod'],
                options: {
                    logConcurrentOutput: true
                }
            }
        },
        eslint: {
            frontend: ['frontend/src/', 'frontend/ssr/', 'frontend/*.js']
        },
        open: {
            dev: {
                path: 'http://localhost:3000/'
            }
        }
    });

    grunt.registerTask('dev', ['env:dev', 'concurrent:dev']);
    grunt.registerTask('build', ['env:prod', 'clean:web', 'clean:ssr', 'webpack:prod', 'babel:ssr']);
    grunt.registerTask('serve', ['env:prod', 'concurrent:serve']);

    grunt.loadNpmTasks('grunt-nodemon');
    grunt.loadNpmTasks('grunt-webpack');
    grunt.loadNpmTasks('grunt-env');
    grunt.loadNpmTasks('grunt-run');
    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-eslint');
    grunt.loadNpmTasks('grunt-open');
};
